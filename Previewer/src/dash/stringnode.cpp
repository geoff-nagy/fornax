#include "stringnode.h"
using namespace Dash;

#include <string>
#include <sstream>
using namespace std;

StringNode::StringNode(string name)
	: Node(name)
{ }

StringNode::~StringNode() { }

void StringNode::setValue(string value)
{
	this -> value = value;
}

string StringNode::getValue()
{
	return value;
}

string StringNode::toString(int indentLevel)
{
	int i;
	stringstream ss;

	// indent to the appropriate level
	for(i = 0; i < indentLevel; i ++) ss << "\t";

	// add the node name, and value in quotes
	ss << getName() << " \"" << value << "\"" << endl;

	return ss.str();
}
