#pragma once

#include "node.h"

#include <string>
#include <vector>

namespace Dash
{
	class NumbersNode;
	class StringNode;

	class ListNode : public Node
	{
	private:
		std::vector<Node*> nodes;
	public:
		ListNode(std::string name);
		~ListNode();

		void addNode(Node *node);
		Node *getNode(int index);

		int getNumber(std::string path, int index, int defaultValue);
		std::string getString(std::string path, std::string defaultValue);

		Node *getNodeByName(std::string name);
		ListNode *getListNodeByName(std::string name, int nth);
		NumbersNode *getNumbersNodeByName(std::string name, int nth);
		StringNode *getStringNodeByName(std::string name, int nth);

		std::vector<Node*>::const_iterator getBegin() const;
		std::vector<Node*>::const_iterator getEnd() const;

		std::string toString(int indentLevel = 0);
	};
}
