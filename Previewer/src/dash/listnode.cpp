#include "listnode.h"
#include "numbersnode.h"
#include "stringnode.h"
using namespace Dash;

#include <string>
#include <sstream>
#include <iostream>
#include <vector>
using namespace std;

ListNode::ListNode(string name)
	: Node(name)
{ }

ListNode::~ListNode()
{
	vector<Node*>::iterator i = nodes.begin();
	Node *toRemove;

	while(i != nodes.end())
	{
		toRemove = *i;
		delete toRemove;
		i = nodes.erase(nodes.begin());
	}
}

void ListNode::addNode(Node *node)
{
	nodes.push_back(node);
}

Node *ListNode::getNode(int index)
{
	return nodes[index];
}

int ListNode::getNumber(string path, int index, int defaultValue)
{
	int result = defaultValue;						// defaults to default return value
	string childNodeName;							// name of the next child node
	Node *childNode;									// the actual child node
	ListNode *childAsListNode;						// the actual child node as a list node
	string childPath;								// the remaining path after the child
	Node *targetNode;								// the node we're targeting (the string node we want) at the end
	NumbersNode *targetNodeAsNumbers;				// the node we're targeting as a string node
	size_t periodIndex;								// where the period '.' appears

	// if there's a period somewhere, we still have to traverse at least one more list node down
	periodIndex = path.find('.');
	if (periodIndex != string::npos)
	{
		// get the name of the child node, if any
		childNodeName = path.substr(0, periodIndex);

		// now get the child node and figure out what it is
		childNode = getNodeByName(childNodeName);
		childAsListNode = dynamic_cast<ListNode*>(childNode);

		// if it's a list, then continue parsing the path string
		if(childAsListNode != NULL)
		{
			childPath = path.substr(periodIndex + 1, path.length() - (periodIndex + 1));
			result = childAsListNode -> getNumber(childPath, index, defaultValue);
		}
		else
		{
			cerr << "ListNode::getNumber() cannot find a node path to " << path << endl;
		}
	}
	else
	{
		// if there's no period, we should be there!
		targetNode = getNodeByName(path);
		if(targetNode != NULL)
		{
			targetNodeAsNumbers = dynamic_cast<NumbersNode*>(targetNode);
			if (targetNodeAsNumbers != NULL)
			{
				if (index < targetNodeAsNumbers -> getNumberCount())
				{
					result = targetNodeAsNumbers -> at(index);
				}
				else
				{
					cerr << "ListNode::getNumber() found a numbers node at " << path << " but index " << index << " is out of range" << endl;
				}
			}
			else
			{
				cerr << "ListNode::getNumber() found a node called " << path << " but it is not a numbers node" << endl;
			}
		}
		else
		{
			cerr << "ListNode::getNumber() could not find any node named " << path << endl;
		}
	}

	return result;
}

string ListNode::getString(string path, string defaultValue)
{
	string result = defaultValue;					// defaults to default return value
	string childNodeName;							// name of the next child node
	Node *childNode;								// the actual child node
	ListNode *childAsListNode;						// the actual child node as a list node
	string childPath;								// the remaining path after the child
	Node *targetNode;								// the node we're targeting (the string node we want) at the end
	StringNode *targetNodeAsString;					// the node we're targeting as a string node
	size_t periodIndex;								// where the period '.' appears

	// if there's a period somewhere, we still have to traverse at least one more list node down
	periodIndex = path.find('.');
	if (periodIndex != string::npos)
	{
		// get the name of the child node, if any
		childNodeName = path.substr(0, periodIndex);

		// now get the child node and figure out what it is
		childNode = getNodeByName(childNodeName);
		childAsListNode = dynamic_cast<ListNode*>(childNode);

		// if it's a list, then continue parsing the path string
		if (childAsListNode != NULL)
		{
			childPath = path.substr(periodIndex + 1, path.length() - (periodIndex + 1));
			result = childAsListNode -> getString(childPath, defaultValue);
		}
		else
		{
			cerr << "ListNode::getString() cannot find a node path to " << path << endl;
		}
	}
	else
	{
		// if there's no period, we should be there!
		targetNode = getNodeByName(path);
		if(targetNode != NULL)
		{
			targetNodeAsString = dynamic_cast<StringNode*>(targetNode);
			if (targetNodeAsString != NULL)
			{
				result = targetNodeAsString -> getValue();
			}
			else
			{
				cerr << "ListNode::getString() found a node called " << path << " but it is not a string node" << endl;
			}
		}
		else
		{
			cerr << "ListNode::getString() could not find any node named " << path << endl;
		}
	}

	return result;
}

ListNode *ListNode::getListNodeByName(string name, int nth)
{
	vector<Node*>::iterator i = nodes.begin();
	int count = 0;				// which list node index we're currently reading

	Node *curr;					// node we're currently reading
	ListNode *temp;				// attempt to transform current node into a list node
	ListNode *result = NULL;

	while (result == NULL && i != nodes.end())
	{
		curr = *i;

		// if we found a name match, so far so good
		if (curr -> getName().compare(name) == 0)
		{
			// we only consider matches that are actually list nodes, since we want a list node anyways
			temp = dynamic_cast<ListNode*>(curr);
			if (temp != NULL)
			{
				// if we've reached the right count, we're done!
				if (count == nth)
				{
					result = temp;
				}
				count++;
			}
		}
		i++;
	}

	return result;
}

NumbersNode *ListNode::getNumbersNodeByName(string name, int nth)
{
	vector<Node*>::iterator i = nodes.begin();
	int count = 0;				// which list node index we're currently reading

	Node *curr;					// node we're currently reading
	NumbersNode *temp;			// attempt to transform current node into a numbers node
	NumbersNode *result = NULL;

	while (result == NULL && i != nodes.end())
	{
		curr = *i;

		// if we found a name match, so far so good
		if (curr -> getName().compare(name) == 0)
		{
			// we only consider matches that are actually number nodes, since we want a number node anyways
			temp = dynamic_cast<NumbersNode*>(curr);
			if (temp != NULL)
			{
				// if we've reached the right count, we're done!
				if (count == nth)
				{
					result = temp;
				}
				count++;
			}
		}
		i++;
	}

	return result;
}

StringNode *ListNode::getStringNodeByName(string name, int nth)
{
	vector<Node*>::iterator i = nodes.begin();
	int count = 0;				// which list node index we're currently reading

	Node *curr;					// node we're currently reading
	StringNode *temp;			// attempt to transform current node into a numbers node
	StringNode *result = NULL;

	while (result == NULL && i != nodes.end())
	{
		curr = *i;

		// if we found a name match, so far so good
		if (curr -> getName().compare(name) == 0)
		{
			// we only consider matches that are actually string nodes, since we want a string node anyways
			temp = dynamic_cast<StringNode*>(curr);
			if (temp != NULL)
			{
				// if we've reached the right count, we're done!
				if (count == nth)
				{
					result = temp;
				}
				count++;
			}
		}
		i++;
	}

	return result;
}

Node *ListNode::getNodeByName(string name)
{
	vector<Node*>::iterator i = nodes.begin();
	Node *curr;
	Node *result = NULL;

	while(result == NULL && i != nodes.end())
	{
		curr = *i++;
		if(curr -> getName().compare(name) == 0)
		{
			result = curr;
		}
	}

	return result;
}

vector<Node*>::const_iterator ListNode::getBegin() const
{
	return nodes.begin();
}

vector<Node*>::const_iterator ListNode::getEnd() const
{
	return nodes.end();
}

string ListNode::toString(int indentLevel)
{
	int i;
	vector<Node*>::iterator j;
	stringstream ss;

	// indent to the appropriate level
	for(i = 0; i < indentLevel; i ++) ss << "\t";

	// add the node name
	ss << getName() << endl;

	// now print out the nodes we own
	for(j = nodes.begin(); j != nodes.end(); j ++)
	{
		ss << (*j) -> toString(indentLevel + 1);
	}

	// output a new line for the next node
	//ss << endl;

	return ss.str();
}
