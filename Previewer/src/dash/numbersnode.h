#pragma once

#include "node.h"

#include <string>
#include <vector>

namespace Dash
{
	class Number;

	class NumbersNode : public Node
	{
	private:
		std::vector<Number*> numbers;
	public:
		NumbersNode(std::string name);
		~NumbersNode();

		void addNumber(Number *number);
		int at(int index);

		int getNumberCount();

		std::vector<Number*>::const_iterator getBegin() const;
		std::vector<Number*>::const_iterator getEnd() const;

		std::string toString(int indentLevel = 0);
	};
}
