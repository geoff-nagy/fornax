#pragma once

#include <string>

namespace Dash
{
	class Node
	{
	private:
		std::string name;

	public:
		Node(std::string name);									// node cannot be created without a name
		virtual ~Node() = 0;									// abstract class

        std::string getName();									// name of node, but not its value

        virtual std::string toString(int indentLevel) = 0;		// must be implemented
	};
}
