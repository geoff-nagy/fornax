#include "numbersnode.h"
#include "number.h"
using namespace Dash;

#include <string>
#include <sstream>
#include <vector>
using namespace std;

NumbersNode::NumbersNode(string name)
	: Node(name)
{ }

NumbersNode::~NumbersNode()
{
	vector<Number*>::iterator i = numbers.begin();
	Number *toRemove;

	while(i != numbers.end())
	{
		toRemove = *i;
		delete toRemove;
		i = numbers.erase(numbers.begin());
	}
}

void NumbersNode::addNumber(Number *number)
{
	numbers.push_back(number);
}

int NumbersNode::at(int index)
{
	return numbers[index] -> getValue();
}

int NumbersNode::getNumberCount()
{
	return (int)numbers.size();
}

vector<Number*>::const_iterator NumbersNode::getBegin() const
{
	return numbers.begin();
}

vector<Number*>::const_iterator NumbersNode::getEnd() const
{
	return numbers.end();
}

// this is mostly just a debugging function used to make sure that Dash is parsing things correctly
string NumbersNode::toString(int indentLevel)
{
	int i;
	vector<Number*>::iterator j;
	stringstream ss;

	// indent to the appropriate level
	for(i = 0; i < indentLevel; i ++) ss << "\t";

	// add the node name
	ss << getName() << " ";

	// now print out the numbers we have
	for(j = numbers.begin(); j != numbers.end(); j ++)
	{
		ss << (*j) -> toString() << " ";
	}

	// output a new line for the next node
	ss << endl;

	return ss.str();
}
