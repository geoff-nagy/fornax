#pragma once

#include "node.h"

#include <string>

namespace Dash
{
	class StringNode : public Node
	{
	private:
		std::string value;
	public:
		StringNode(std::string name);
		~StringNode();

		std::string getValue();
		void setValue(std::string value);

		std::string toString(int indentLevel = 0);
	};
};
