#pragma once

#include <string>
#include <fstream>
#include <map>

namespace Dash
{
	// forward declare these objects
	class Node;
	class NumbersNode;
	class StringNode;
	class ListNode;

	class Tree
	{
	private:
		Node *root;																			// a dash tree root node can be any type of node
		static std::map<std::string, std::string> constants;								// any user-defined constants

		Tree(Node *root);																	// instantiate the tree with the root node

		static bool isWhitespace(char c);													// is the character non-visible?
		static bool isTokenEnd(char c);														// whitespace, parentheses, and commas denote the end of a token
		static bool isQuote(char c);														// is the character a double-quotation mark?
		static bool isNumeric(char c);														// is the character a digit?
		static bool isNumeric(std::string str, double *number);								// is the string in integer or double format?
		static bool isValidNodeStart(char c);												// is the character in the range a - z (lowercase only)?
		static bool isConstant(char c);														// is the character in the range A - Z (uppercase only)?
		static void consumeWhitespace(std::ifstream &file);									// skip past any whitespace or comments
		static void consumeUpToNodeValue(std::ifstream &file);								// skip far enough to read the value of the node we're in, skipping leading and trailing whitespace
		static std::string consumeToken(std::ifstream &file);								// return a token terminated by a token-end character (see isTokenEnd())
		static std::string consumeConstantToken(std::ifstream &file);						// assumes that the token is a constant; called by consumeToken()
		static std::string consumeStringToken(std::ifstream &file);							// assumes that the token is a string literal; called by consumeToken()
		static std::string consumeWordToken(std::ifstream &file);							// assumes that the token is a single word (including a number); called by consumeToken()

		static Node *parseNode(std::ifstream &file);										// parse the node we see depending on its perceived type
		static NumbersNode *parseNumberList(std::ifstream &file, std::string nodeName);		// parse a node as a list of numbers
		static StringNode *parseStringNode(std::ifstream &file, std::string nodeName);		// parse a node as a string value
		static ListNode *parseListNode(std::ifstream &file, std::string nodeName);			// parse a node recursively as a list of nodes

		static void parsePrefaceDirectives(std::ifstream &file);							// parse a command beginning with '$' at the top of the file
		static void parseConstant(std::ifstream &file);										// parse a constant variable definition at the top of the file

	public:
		~Tree();

		static Tree *createFromFile(std::string filename);									// create a dash tree by parsing the given file

		std::string toString();																// get a string representing the dash tree

		Node *getRoot();																	// return pointer to the root node in the tree
	};
}
