#pragma once

#include <string>

// a number in dash takes the form of a number
// followed (optionally) by either the + or -
// sign and a variable name
// e.g., 54
//		 54 - someVal
//		 54 + someOtherVal

namespace Dash
{
	class Number
	{
	public:
		Number();
		Number(double value);

		enum Operator
		{
			OPERATOR_NONE,
			OPERATOR_PLUS,
			OPERATOR_MINUS
		};

		double getValue();
		void setValue(double);

		Operator getOperator();
		void setOperator(Operator op);

		std::string getVariable();
		void setVariable(std::string variable);

		std::string toString();

	private:
		double value;
		std::string variable;
		Operator op;
	};
}
