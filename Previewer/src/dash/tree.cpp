#include "tree.h"
#include "node.h"
#include "numbersnode.h"
#include "number.h"
#include "stringnode.h"
#include "listnode.h"
using namespace Dash;

#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <map>
using namespace std;

map<string, string> Tree::constants;					// contains any constants used when parsing the tree

Tree::Tree(Node *root)
{
	this -> root = root;
}

Tree::~Tree()
{
	delete root;
}

Tree *Tree::createFromFile(string filename)
{
	Tree *result = NULL;
	Node *root = NULL;

	// open the file and make sure it exists
	ifstream file(filename.c_str(), ios::binary);			// we use ios::binary to avoid line encoding translations that screw us up
	if(file.is_open())
	{
		// see if there are any defined constants (or any other types of preface directives we might implement in the future)
		parsePrefaceDirectives(file);

		// dash doesn't care what kind of node we have at the root
		root = parseNode(file);

		// if the parse was successful, allocate the tree and set its root to what we just parsed
		if(root != NULL)
		{
			result = new Tree(root);
		}
		else
		{
			cerr << "Tree::createFromFile() failed to create a non-null root" << endl;
			exit(1);
		}
	}
	else
	{
		cerr << "Tree::createFromFile() could not open " << filename << endl;
	}

	return result;
}

string Tree::toString()
{
	return root -> toString(0);
}

Node *Tree::getRoot()
{
	return root;
}

bool Tree::isWhitespace(char c)
{
	return c <= 32 && c >= 9;
}

bool Tree::isTokenEnd(char c)
{
	return isWhitespace(c) || c == '(' || c == ')' || c == ',';
}

bool Tree::isQuote(char c)
{
	return c == '\"';
}

bool Tree::isNumeric(char c)
{
	return c == '-' || (c >= '0' && c <= '9');
}

bool Tree::isNumeric(string str, double *number)
{
	char *p = NULL;

	// this sets p to point to the last character in the attempted
	// conversion; thus, if it was successful, p will point to the
	// string null-terminating character (i.e., the end of the
	// string), or anything else if not successfuly
    *number = strtod(str.c_str(), &p);
    return *p == 0;
}

bool Tree::isValidNodeStart(char c)
{
	return c >= 'a' && c <= 'z';
}

bool Tree::isConstant(char c)
{
	return c >= 'A' && c <= 'Z';
}

void Tree::consumeWhitespace(ifstream &file)
{
	char c;

	// skip over the general repeating pattern of [whitespace][comment] until we run out
	do
	{
		// skip over any characters that are whitespace (tabs, spaces, new lines, etc.)
		do
		{
			file.get(c);
		}
		while(isWhitespace(c));

		// skip over any comments
		if(c == '#')
		{
			// skip over characters until we encounter a new line
			do
			{
				file.get(c);
			}
			while(!(c == '\r' || c == '\n'));
		}
	}
	while(isWhitespace(c));

	// we've consumed a visible char that's not a comment, so we need to spit it back out
	file.unget();
}

void Tree::consumeUpToNodeValue(ifstream &file)
{
	char c;

	// assumes that the node name has been read by now; so, eat any whitespace
	consumeWhitespace(file);

	// the next char should be a semicolon
	file.get(c);
	if(c != '(')
	{
		cerr << "Tree::consumeUpToNodeValue() expected '(' but got " << c << endl;
		exit(1);
	}

	// now read any remaining whitespace
	consumeWhitespace(file);
}

string Tree::consumeToken(ifstream &file)
{
	string result = "";
	char c;

	// get rid of any whitespace
	consumeWhitespace(file);

	// see what kind of data we're dealing with
	file.get(c);
	file.unget();

	if(isConstant(c))
	{
		result = consumeConstantToken(file);
	}
	else if(isQuote(c))
	{
		result = consumeStringToken(file);
	}
	else
	{
		result = consumeWordToken(file);
	}

	return result;
}

string Tree::consumeConstantToken(ifstream &file)
{
	// get the name of the constant
	string name = consumeWordToken(file);
	string result;

	// make sure the constant exists!
	if(constants.find(name) != constants.end())
	{
		result = constants[name];
	}
	else
	{
		cerr << "Tree::consumeConstantToken cannot find a constant named " << name << endl;
		exit(1);
	}

	return result;
}

string Tree::consumeStringToken(ifstream &file)
{
	char c;
	bool done = false;
	string result = "";

	// make sure we begin by reading a double quote
	file.get(c);
	if(c != '\"')
	{
		cerr << "Tree::consumeStringToken() expected opening double-quote '\"', but got " << c << endl;
		exit(1);
	}

	// consume anything up to, but not including, the closing quote
	do
	{
		file.get(c);
		done = c == '\"';

		if(!done)
		{
			result += c;
		}
	}
	while(!done);

	return "\"" + result + "\"";
}

string Tree::consumeWordToken(ifstream &file)
{
	string result = "";
	char c;
	bool done = false;

	// build the node name char by char until we hit a '(' or some whitespace
	do
	{
		file.get(c);
		done = isTokenEnd(c);

		if(!done)
		{
			result += c;
		}
	}
	while(!done);

	file.unget();

	return result;
}

void Tree::parsePrefaceDirectives(ifstream &file)
{
	string directive;
	bool done = false;
	char c;

	// clear any defined constants
	constants.clear();

	// keep going while we have more directives to check out
	while(!done)
	{
		// first go over any whitespace or comments
		consumeWhitespace(file);

		// get the first char and see if it is a preface directive
		file.get(c);
		if(c == '$')
		{
			// read the directive and decide what to do with it
			directive = consumeWordToken(file);
			if(directive.compare("def") == 0)
			{
				parseConstant(file);
			}
			else
			{
				cerr << "Tree::parsePrefaceDirectives() does not know preface directive " << directive << endl;
				exit(1);
			}
		}
		else
		{
			file.unget();
			done = true;
		}
	}
}

void Tree::parseConstant(ifstream &file)
{
	string name;
	string value;
	char c;

	// consume some more whitespace
	consumeWhitespace(file);

	// get the name of the constant and see if it already exists
	name = consumeWordToken(file);
	if(constants.find(name) == constants.end())
	{
		// get the first character of the constant value
		consumeWhitespace(file);
		file.get(c);
		file.unget();

		// ...decide what to based on the type of data we encounter
		if(isQuote(c))
		{
			//value = "\"" + consumeStringToken(file) + "\"";
			value = consumeStringToken(file);
			constants[name] = value;
		}
		else if(isNumeric(c))
		{
			value = consumeWordToken(file);
			constants[name] = value;
		}
		else
		{
			cerr << "Tree::parseConstant() only allows constants to be string literals or numeric literals " << endl;
			exit(1);
		}
	}
	else
	{
		cerr << "Tree::parseConstant() found a duplicate constant: " << name << endl;
		exit(1);
	}
}

Node *Tree::parseNode(ifstream &file)
{
	Node *result = NULL;
	string constantValue;
	size_t pos;
	char c;

	// read the name of the node
	string nodeName = consumeWordToken(file);

	// skip up to the node value (i.e., past the opening semicolon and to the first
	// char of whatever the node value is)
	consumeUpToNodeValue(file);

	// read the first char...
	file.get(c);
	file.unget();

	// determine if this is a constant
	if(isConstant(c))
	{
		// if it is, read the whole name (but keep track of where we started, because
		// we have to re-read it again later)
		pos = file.tellg();
		constantValue = consumeConstantToken(file);
		c = constantValue[0];

		// and back up, too, because we have to read it again
		file.seekg(pos);
	}

	// ...and decide what to do with the node value based on that first char
	if(isNumeric(c))
	{
		result = parseNumberList(file, nodeName);
	}
	else if(isQuote(c))
	{
		result = parseStringNode(file, nodeName);
	}
	else if(isValidNodeStart(c))
	{
		result = parseListNode(file, nodeName);
	}
	else
	{
		cerr << "Tree::parseNode() expected numeric literal, string literal, constant, or node, but got character " << c << endl;
		exit(1);
	}

	return result;
}

NumbersNode *Tree::parseNumberList(ifstream &file, string nodeName)
{
	NumbersNode *result = new NumbersNode(nodeName);			// the new node we insert into the tree
	Number *numberObj;											// the number object with an optional simple variable added or subtracted
	string numberStr;											// the number value interpreted as a string
	double numberValue;											// the number value converted to a double
	string variableName;										// the optional variable name added to or subtracted from the numeric value

	char c;
	bool done = false;

	// we don't know how many numbers we'll get, so start looping
	while(!done)
	{
		// skip any leading whitespace
		consumeWhitespace(file);

		// read the next number in the sequence
		numberStr = consumeToken(file);

		// make sure it's actually a number
		if(!isNumeric(numberStr, &numberValue))
		{
			cerr << "Tree::parseNumberList() does not allow non-numeric data like " << numberStr << endl;
			exit(1);
		}

		// by now, we know a conversion will be successful; add the number to the list in this node
		numberObj = new Number(numberValue);
		result -> addNumber(numberObj);

		// skip trailing whitespace
		consumeWhitespace(file);

		// the next char should be a comma, a plus or minus, or a closing parentheses
		file.get(c);
		if(c == ')')
		{
			done = true;
		}
		// we may have an operator to read, so check for it
		else if(c == '+' || c == '-')
		{
			// read in the specific operator we see
			numberObj -> setOperator(c == '+' ? Number::OPERATOR_PLUS : Number::OPERATOR_MINUS);

			// now skip leading whitespace
			consumeWhitespace(file);

			// read the variable name
			variableName = consumeToken(file);
			numberObj -> setVariable(variableName);

			// skip any trailing whitespace
			consumeWhitespace(file);

			// the next char must be a comma or closing parenthesis
			file.get(c);
			if(c == ')')
			{
				done = true;
			}
			else if( c != ',')
			{
				cerr << "Tree::parseNumberList() expected comma or terminating ')' but got " << c << endl;
			}
		}
		else if(c != ',')
		{
			cerr << "Tree::parseNumberList() expected comma, operator (+ or -), or terminating ')' but got " << c << endl;
			exit(1);
		}
	}

	return result;
}

StringNode *Tree::parseStringNode(ifstream &file, string nodeName)
{
	StringNode *result = new StringNode(nodeName);
	string str;
	char c;

	// read over any whitespace
	consumeWhitespace(file);

	// read the string data
	str = consumeToken(file);

	// remove the quotes and put the value into the string node
	str = str.substr(1, str.length() - 2);
	result -> setValue(str);

	// read any trailing whitespace
	consumeWhitespace(file);

	// ensure that there is a terminating ')' char
	file.get(c);
	if(c != ')')
	{
		cerr << "Tree::parseStringNode() expected terminating ')' but got " << c << endl;
		exit(1);
	}

	return result;
}

ListNode *Tree::parseListNode(ifstream &file, string nodeName)
{
	ListNode *result = new ListNode(nodeName);
	Node *item;
	char c;
	bool done = false;

	while(!done)
	{
		// parse the node recursively and add it to the node list
		item = parseNode(file);
		result -> addNode(item);

		// skip past any whitespace
		consumeWhitespace(file);

		// if we reach a semicolon, we're done
		file.get(c);
		if(c == ')')
		{
			done = true;
		}
		else
		{
			// if this isn't a semicolon, it's something we need, so put it back
			file.unget();
		}
	}

	return result;
}
