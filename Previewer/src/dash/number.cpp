#include "number.h"
using namespace Dash;

#include <string>
#include <sstream>
using namespace std;

Number::Number()
{
	value = 0.0;
	variable = "";
	op = OPERATOR_NONE;
}

Number::Number(double value)
{
	this -> value = value;
	variable = "";
	op = OPERATOR_NONE;
}

double Number::getValue()
{
	return value;
}

void Number::setValue(double value)
{
	this -> value = value;
}

Number::Operator Number::getOperator()
{
	return op;
}

void Number::setOperator(Number::Operator op)
{
	this -> op = op;
}

string Number::getVariable()
{
	return variable;
}

void Number::setVariable(string variable)
{
	this -> variable = variable;
}

string Number::toString()
{
	stringstream ss;

	// variable is printed if it exists, with the appropriate operator
	if(op == OPERATOR_PLUS)
	{
		ss << "(" << value << " + " << variable << ")";
	}
	else if(op == OPERATOR_MINUS)
	{
		ss << "(" << value << " - " << variable << ")";
	}
	else
	{
		ss << value;
	}

	return ss.str();
}
