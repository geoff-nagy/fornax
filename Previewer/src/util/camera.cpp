#include "util/camera.h"

#include "glm/glm.hpp"
#include <glm/gtc/matrix_transform.hpp>

using namespace glm;

dmat4 Camera::modelview[2];
dmat4 Camera::projection[2];
dmat4 Camera::mvp[2];

Camera::Plane Camera::frustumPlanes[2][6];

dvec3 Camera::pos[2];
dvec3 Camera::forward[2];
dvec3 Camera::side[2];
dvec3 Camera::up[2];

void Camera::configurePerspectiveMode(double fov, double aspect, double near, double far)
{
	projection[PERSPECTIVE] = glm::perspective(glm::radians(fov), aspect, near, far);
}

void Camera::configureOrthoMode(double left, double right, double bottom, double top)
{
	projection[ORTHO] = glm::ortho(left, right, bottom, top);
}

void Camera::lookAt(ProjectionMode mode, dvec3 eye, dvec3 centerVec, dvec3 upVec)
{
	modelview[mode] = glm::lookAt(eye, centerVec, upVec);
	mvp[mode] = projection[mode] * modelview[mode];

	pos[mode] = eye;
	forward[mode] = normalize(centerVec - eye);
	side[mode] = normalize(cross(forward[mode], upVec));
	up[mode] = normalize(upVec);

	extractFrustumPlanes(mode);
}

Camera::Plane Camera::extractPlane(double a, double b, double c, double d)
{
	Plane result;
	double len = sqrt( a * a + b * b + c * c );

	result.a = a / len;
	result.b = b / len;
	result.c = c / len;
	result.d = d / len;

	return result;
}

void Camera::extractFrustumPlanes(ProjectionMode mode)
{
    frustumPlanes[mode][PLANE_LEFT] = extractPlane(mvp[mode][0][3] + mvp[mode][0][0],
												   mvp[mode][1][3] + mvp[mode][1][0],
												   mvp[mode][2][3] + mvp[mode][2][0],
												   mvp[mode][3][3] + mvp[mode][3][0]);

    frustumPlanes[mode][PLANE_RIGHT] = extractPlane(mvp[mode][0][3] - mvp[mode][0][0],
													mvp[mode][1][3] - mvp[mode][1][0],
													mvp[mode][2][3] - mvp[mode][2][0],
													mvp[mode][3][3] - mvp[mode][3][0]);

    frustumPlanes[mode][PLANE_TOP] = extractPlane(mvp[mode][0][3] - mvp[mode][0][1],
												  mvp[mode][1][3] - mvp[mode][1][1],
												  mvp[mode][2][3] - mvp[mode][2][1],
												  mvp[mode][3][3] - mvp[mode][3][1]);

    frustumPlanes[mode][PLANE_BOTTOM] = extractPlane(mvp[mode][0][3] + mvp[mode][0][1],
													 mvp[mode][1][3] + mvp[mode][1][1],
													 mvp[mode][2][3] + mvp[mode][2][1],
													 mvp[mode][3][3] + mvp[mode][3][1]);

    frustumPlanes[mode][PLANE_FAR] = extractPlane(mvp[mode][0][3] + mvp[mode][0][2],
												  mvp[mode][1][3] + mvp[mode][1][2],
												  mvp[mode][2][3] + mvp[mode][2][2],
												  mvp[mode][3][3] + mvp[mode][3][2]);

    frustumPlanes[mode][PLANE_NEAR] = extractPlane(mvp[mode][0][3] - mvp[mode][0][2],
												   mvp[mode][1][3] - mvp[mode][1][2],
												   mvp[mode][2][3] - mvp[mode][2][2],
												   mvp[mode][3][3] - mvp[mode][3][2]);
}

dmat4 Camera::getModelview(ProjectionMode mode)
{
	return modelview[mode];
}

dmat4 Camera::getProjection(ProjectionMode mode)
{
	return projection[mode];
}

dmat4 Camera::getModelviewProjection(ProjectionMode mode)
{
	return mvp[mode];
}

dvec3 Camera::getPos(ProjectionMode mode)
{
	return pos[mode];
}

dvec3 Camera::getForward(ProjectionMode mode)
{
	return forward[mode];
}

dvec3 Camera::getSide(ProjectionMode mode)
{
	return side[mode];
}

dvec3 Camera::getUp(ProjectionMode mode)
{
	return up[mode];
}

bool Camera::isFacing(ProjectionMode mode, dvec3 point)
{
	dvec3 rel = normalize(point - pos[mode]);
	return dot(rel, forward[mode]) > 0.0;
}

bool Camera::isSphereInFrustum(ProjectionMode mode, dvec3 center, double radius)
{
	bool result = true;
	int i = 0;

	while(result && i < 6)
	{
		if (frustumPlanes[mode][i].a * center.x +
            frustumPlanes[mode][i].b * center.y +
            frustumPlanes[mode][i].c * center.z +
            frustumPlanes[mode][i].d <= -radius)
        {
			result = false;
        }
        i ++;
    }

	return result;
}
