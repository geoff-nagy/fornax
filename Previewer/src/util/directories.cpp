// thanks, StackOverflow!
// https://stackoverflow.com/a/29828907

#include "util/directories.h"

#include <sys/stat.h> // stat
#include <errno.h>    // errno, ENOENT, EEXIST
#if defined(USING_WINDOWS)
	#include <direct.h>   // _mkdir
#endif

#include <cstdlib>
#include <string>
#include <iostream>
using namespace std;

bool dirDoesDirectoryExist(const string& path)
{
#if defined(USING_WINDOWS)
    struct _stat info;
    if (_stat(path.c_str(), &info) != 0)
    {
        return false;
    }
    return (info.st_mode & _S_IFDIR) != 0;
#elif defined(USING_LINUX)
    struct stat info;
    if (stat(path.c_str(), &info) != 0)
    {
        return false;
    }
    return (info.st_mode & S_IFDIR) != 0;
#else
	#error dirDoesDirectoryExist(): neither USING_WINDOWS nor USING_LINUX were defined
#endif
}

bool dirMakeDirectory(const string& path)
{
	// attempt to make the directory
#if defined(USING_WINDOWS)
	int ret = _mkdir(path.c_str());
#elif defined(USING_LINUX)
	mode_t mode = 0755;
	int ret = mkdir(path.c_str(), mode);
#else
	#error dirMakeDirectory(): neither USING_WINDOWS nor USING_LINUX were defined
#endif

	// zero as an error code means success; return true in this case
    if (ret == 0)
        return true;

	if(errno == ENOENT)		// parent didn't exist, try to create it
	{
		int pos = path.find_last_of(dirGetDirSep());
		if (pos == (int)string::npos)
#if defined(USING_WINDOWS)
			pos = path.find_last_of(dirGetDirSep());
		if (pos == (int)string::npos)
#elif defined(USING_LINUX)
			return false;
#else
	#error dirMakeDirectory(): neither USING_WINDOWS nor USING_LINUX were defined
#endif

		// recursively try to create the parent directory
		if (!dirMakeDirectory(path.substr(0, pos)))
			return false;

		// now, try to create again
#if defined(USING_WINDOWS)
		return 0 == _mkdir(path.c_str());
#elif defined(USING_LINUX)
		return 0 == mkdir(path.c_str(), mode);
#else
	#error dirMakeDirectory(): neither USING_WINDOWS nor USING_LINUX were defined
		return 0;
#endif
	}
	else if(errno == EEXIST)
	{
		// done!
		return dirDoesDirectoryExist(path);
	}
	else
	{
		return false;
    }
}

string dirGetHomeDirectory()
{
	string result = "";
	char *path;
#if defined(USING_WINDOWS)
	path = getenv("USERPROFILE");
	if(path)
	{
		result += path;
	}
	else
	{
		path = getenv("HOMEDRIVE");
		if(path)
		{
			result += path;
			path = getenv("HOMEPATH");
			if(path)
			{
				result += path;
			}
			else
			{
				result = "";
			}
		}
	}
#elif defined(USING_LINUX)

	path = getenv("HOME");
	if(path)
	{
		result += string(path) + dirGetDirSep() + ".local" + dirGetDirSep() + "share";
	};
#else
	#error dirGetHomeDirectory(): neither USING_WINDOWS nor USING_LINUX were defined
#endif

	return result;
}

char dirGetDirSep()
{
    char result = '/';
#if defined(USING_WINDOWS)
    result = '\\';
#elif defined(USING_LINUX)
    result = '/';
#else
    #error dirGetHomeDirectory(): neither USING_WINDOWS nor USING_LINUX were defined
#endif
    return result;
}
