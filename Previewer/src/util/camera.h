#pragma once

#include "glm/glm.hpp"

class Camera
{
public:
	typedef enum PROJECTIONMODE {ORTHO, PERSPECTIVE} ProjectionMode;

	typedef struct PLANE {double a, b, c, d;} Plane;

	enum
	{
	    PLANE_LEFT = 0,
	    PLANE_RIGHT,
	    PLANE_TOP,
	    PLANE_BOTTOM,
	    PLANE_FAR,
	    PLANE_NEAR
    };

private:
	static glm::dmat4 modelview[2];
	static glm::dmat4 projection[2];
	static glm::dmat4 mvp[2];

	static Plane frustumPlanes[2][6];

    static glm::dvec3 pos[2];
    static glm::dvec3 forward[2];
    static glm::dvec3 side[2];
    static glm::dvec3 up[2];

	static Plane extractPlane(double a, double b, double c, double d);
    static void extractFrustumPlanes(ProjectionMode mode);

public:
	static void configurePerspectiveMode(double fov, double aspect, double near, double far);
	static void configureOrthoMode(double left, double right, double bottom, double top);

	static void lookAt(ProjectionMode mode, glm::dvec3 eye, glm::dvec3 center, glm::dvec3 up);

	static glm::dmat4 getModelview(ProjectionMode mode);
	static glm::dmat4 getProjection(ProjectionMode mode);
	static glm::dmat4 getModelviewProjection(ProjectionMode mode);

	static bool isFacing(ProjectionMode mode, glm::dvec3 point);
	static bool isSphereInFrustum(ProjectionMode mode, glm::dvec3 center, double radius);

	static glm::dvec3 getPos(ProjectionMode mode);
	static glm::dvec3 getForward(ProjectionMode mode);
	static glm::dvec3 getSide(ProjectionMode mode);
	static glm::dvec3 getUp(ProjectionMode mode);
};
