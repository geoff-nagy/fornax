#include "particles/particleconfig.h"
#include "particles/particledescriptor.h"
#include "particles/emitterdescriptor.h"

#include "glm/glm.hpp"
using namespace glm;

ParticleConfig::ParticleConfig()
: descriptor(NULL), additiveBlending(false), sizeFactor(1.0), velocityFactor(1.0), lifeFactor(1.0), scale(1.0)
{ }

ParticleConfig::ParticleConfig(ParticleDescriptor *n_descriptor, dvec3 n_pos)
: descriptor(n_descriptor), pos(n_pos), additiveBlending(false), sizeFactor(1.0), velocityFactor(1.0), lifeFactor(1.0), scale(1.0)
{ }

ParticleConfig::ParticleConfig(ParticleDescriptor *n_descriptor, dvec3 n_pos, dvec3 n_velocity)
: descriptor(n_descriptor), pos(n_pos), velocity(n_velocity), additiveBlending(false), sizeFactor(1.0), velocityFactor(1.0), lifeFactor(1.0), scale(1.0)
{ }
