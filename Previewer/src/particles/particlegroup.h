#pragma once

#include <vector>

class ParticleDescriptor;
class EmitterDescriptor;

class ParticleGroup
{
public:
	ParticleGroup(ParticleDescriptor *particle);

	ParticleDescriptor *particle;						// describes the behaviour and appearance of the particle
	int emissionCount;									// how many particles of this type to emit
	double initialDelay;								// delay before particles are actually spawned
	bool reEmit;										// do we emit this particle group repeatedly?
	double reEmitInterval;								// how often to re-emit, in ms, if we are repeating
	bool enabled;										// will the particle be processed by the particle engine?
	bool solo;											// enables only this and other solo particles
	bool spawnCenter;									// ignore emitter center and spawn directly in its center
	bool additiveBlending;								// does this particle use additive blending?

	bool emitterAffectsSize;							// does size scale down as the emitter's life expires?
	bool emitterAffectsVelocity;						// does velocity scale down as the emitter's life expires?
	bool emitterAffectsLifespan;						// does lifetime scale down as the emitter's life expires?

	std::vector<EmitterDescriptor*> emitters;			// any emitters under the control of this particle
};
