#pragma once

#include <string>
#include <vector>
#include <map>

#include "glm/glm.hpp"

class ParticleGroup;
class EmitterDescriptor;

class Emitter
{
private:
	std::vector<ParticleGroup*> *particles;
	std::map<ParticleGroup*, double> initialTimers;
	std::map<ParticleGroup*, double> repeatTimers;
	std::map<ParticleGroup*, bool> burstEmitted;

	glm::dvec3 pos;
	glm::dvec3 velocity;
	double life;
	double maxLife;
	double radius;
	double scale;

public:
	Emitter();

	void setLife(double life);
	void setRadius(double radius);
	void setParticleGroups(std::vector<ParticleGroup*> *particles);
	void setPos(const glm::dvec3 &pos);
	void setVelocity(const glm::dvec3 &velocity);
	void setScale(double scale);

	void update(double dt);

	bool isDead();
};
