#include "particles/particle.h"
#include "particles/particleconfig.h"
#include "particles/particledescriptor.h"
#include "particles/particleengine.h"
#include "particles/emitter.h"
#include "particles/emitterdescriptor.h"

#include "glm/glm.hpp"
#include "glm/gtc/random.hpp"
using namespace glm;

#include <map>
#include <iostream>
using namespace std;

Particle::Particle()
{
	pos = dvec3(0.0);
    angle = 0.0;
    dead = false;

    size = 0.0;
    sizeFactor = 0.0;

    scale = 1.0;

    life = 0.0;

    texture = 0;
}

Particle::~Particle()
{
	vector<Emitter*>::iterator i = childEmitters.begin();
	Emitter *toRemove;

	while(i != childEmitters.end())
	{
		toRemove = *i;
		i = childEmitters.erase(i);

		delete toRemove;
	}
}

void Particle::configure(ParticleConfig config)
{
	ParticleDescriptor *descriptor = config.descriptor;
	double velocityFactor = config.velocityFactor;

	// set basic properties
	life = glm::linearRand(descriptor -> minLife, descriptor -> maxLife) * config.lifeFactor;
	maxLife = life;
	texture = descriptor -> texture;

	pos = config.pos;
	baseVelocity = config.velocity;

	angle = glm::linearRand(0.0, 360.0);

	// set keyframes
	colorKeyframes = &descriptor -> colorKeyframes;
	alphaKeyframes = &descriptor -> alphaKeyframes;
	velocityKeyframes = &descriptor -> velocityKeyframes;
	sizeKeyframes = &descriptor -> sizeKeyframes;
	spinKeyframes = &descriptor -> spinKeyframes;

	// assign the starting colour keyframes
	prevColor = colorKeyframes -> end();
	nextColor = colorKeyframes -> begin();

	// assign the starting alpha keyframes
	prevAlpha = alphaKeyframes -> end();
	nextAlpha = alphaKeyframes -> begin();

	// assign the starting velocity keyframes
	prevVelocity = velocityKeyframes -> end();
	nextVelocity = velocityKeyframes -> begin();

	// assign the starting size keyframes
	prevSize = sizeKeyframes -> end();
	nextSize = sizeKeyframes -> begin();

	// assign the starting spin keyframes
	prevSpin = spinKeyframes -> end();
	nextSpin = spinKeyframes -> begin();

	// set velocity properties
	velocityDirection = dvec3(linearRand(-1.0, 1.0), linearRand(-1.0, 1.0), linearRand(-1.0, 1.0));
	velocityDirection = normalize(velocityDirection) * linearRand(1.0 - descriptor -> velocityVariance, 1.0 + descriptor -> velocityVariance) * velocityFactor;

	// set spin properties
	spinDirection = glm::linearRand(0, 1) ? -1.0 : 1.0;

	// set size modifier
	sizeFactor = config.sizeFactor * (1.0 + glm::linearRand(-descriptor -> sizeVariance, descriptor -> sizeVariance));

	scale = config.scale;

	childEmitters.clear();

	vector<EmitterDescriptor*>::iterator i;
	for(i = config.emitters.begin(); i != config.emitters.end(); i ++)
	{
		addChildEmitter((*i) -> create(config.pos, dvec3(0.0), scale));
	}
}

void Particle::update(double dt)
{
	vector<Emitter*>::iterator i;
	Emitter *curr;
	double lifeFactor;

	// compute life value
	life -= dt;
	lifeFactor = glm::clamp(1.0 - (life / maxLife), 0.0, 1.0);

	// linearly interpolate color
	color = dvec4(getCurrentColor(lifeFactor), getCurrentAlpha(lifeFactor));

	// linearly interpolate velocity
	pos = pos + (getCurrentVelocity(lifeFactor) * velocityDirection * scale * dt) + (baseVelocity * dt);

	// linearly interpolate size
	size = getCurrentSize(lifeFactor) * sizeFactor * scale;

	// linearly interpolate spin
	angle += getCurrentSpin(lifeFactor) * spinDirection * dt;

	// control child emitters
	for(i = childEmitters.begin(); i != childEmitters.end(); i ++)
	{
		curr = *i;
		curr -> setPos(pos);
		//curr -> setSizeScaling(1.0 - lifeFactor);
		curr -> update(dt);
	}

	// kill the particle if it's out of life
	if(life <= 0.0)
	{
		dead = true;
	}
}

void Particle::addChildEmitter(Emitter *childEmitter)
{
	childEmitter -> setLife(maxLife);
	childEmitters.push_back(childEmitter);
}

dvec3 Particle::getCurrentColor(double lifeFactor)
{
	double interp;
	dvec3 result;

	// advance to next keyframe if necessary
	if(nextColor != colorKeyframes -> end() && lifeFactor > nextColor -> first)
	{
		prevColor = nextColor;
		nextColor ++;
	}

	// easy case: we're before the first keyframe
	if(prevColor == colorKeyframes -> end())
	{
		result = nextColor -> second;
	}
	// easy case: we're after the last keyframe
	else if(nextColor == colorKeyframes -> end())
	{
		result = prevColor -> second;
	}
	// slightly harder case: we're in between two valid keyframes
	else
	{
		interp = (lifeFactor - prevColor -> first) / (nextColor -> first - prevColor -> first);
		result = prevColor -> second + (nextColor -> second - prevColor -> second) * interp;
	}

	return result;
}

double Particle::getCurrentAlpha(double lifeFactor)
{
	double interp;
	double result;

	// advance to next keyframe if necessary
	if(nextAlpha != alphaKeyframes -> end() && lifeFactor > nextAlpha -> first)
	{
		prevAlpha = nextAlpha;
		nextAlpha ++;
	}

	// easy case: we're before the first keyframe
	if(prevAlpha == alphaKeyframes -> end())
	{
		result = nextAlpha -> second;
	}
	// easy case: we're after the last keyframe
	else if(nextAlpha == alphaKeyframes -> end())
	{
		result = prevAlpha -> second;
	}
	// slightly harder case: we're in between two valid keyframes
	else
	{
		interp = (lifeFactor - prevAlpha -> first) / (nextAlpha -> first - prevAlpha -> first);
		result = prevAlpha -> second + (nextAlpha -> second - prevAlpha -> second) * interp;
	}

	return result;
}

double Particle::getCurrentVelocity(double lifeFactor)
{
	double interp;
	double result;

	// advance to next keyframe if necessary
	if(nextVelocity != velocityKeyframes -> end() && lifeFactor > nextVelocity -> first)
	{
		prevVelocity = nextVelocity;
		nextVelocity ++;
	}

	// easy case: we're before the first keyframe
	if(prevVelocity == velocityKeyframes -> end())
	{
		result = nextVelocity -> second;
	}
	// easy case: we're after the last keyframe
	else if(nextVelocity == velocityKeyframes -> end())
	{
		result = prevVelocity -> second;
	}
	// slightly harder case: we're in between two valid keyframes
	else
	{
		interp = (lifeFactor - prevVelocity -> first) / (nextVelocity -> first - prevVelocity -> first);
		result = prevVelocity -> second + (nextVelocity -> second - prevVelocity -> second) * interp;
	}

	return result;
}

double Particle::getCurrentSize(double lifeFactor)
{
	double interp;
	double result;

	// advance to next keyframe if necessary
	if(nextSize != sizeKeyframes -> end() && lifeFactor > nextSize -> first)
	{
		prevSize = nextSize;
		nextSize ++;
	}

	// easy case: we're before the first keyframe
	if(prevSize == sizeKeyframes -> end())
	{
		result = nextSize -> second;
	}
	// easy case: we're after the last keyframe
	else if(nextSize == sizeKeyframes -> end())
	{
		result = prevSize -> second;
	}
	// slightly harder case: we're in between two valid keyframes
	else
	{
		interp = (lifeFactor - prevSize -> first) / (nextSize -> first - prevSize -> first);
		result = prevSize -> second + (nextSize -> second - prevSize -> second) * interp;
	}

	return result;
}

double Particle::getCurrentSpin(double lifeFactor)
{
	double interp;
	double result;

	// advance to next keyframe if necessary
	if(nextSpin != spinKeyframes -> end() && lifeFactor > nextSpin -> first)
	{
		prevSpin = nextSpin;
		nextSpin ++;
	}

	// easy case: we're before the first keyframe
	if(prevSpin == spinKeyframes -> end())
	{
		result = nextSpin -> second;
	}
	// easy case: we're after the last keyframe
	else if(nextSpin == spinKeyframes -> end())
	{
		result = prevSpin -> second;
	}
	// slightly harder case: we're in between two valid keyframes
	else
	{
		interp = (lifeFactor - prevSpin -> first) / (nextSpin -> first - prevSpin -> first);
		result = prevSpin -> second + (nextSpin -> second - prevSpin -> second) * interp;
	}

	return result;
}

GLuint Particle::getTexture()
{
    return texture;
}

void Particle::getPos(float *posArr)
{
    posArr[0] = pos.x;
    posArr[1] = pos.y;
    posArr[2] = pos.z;
}

void Particle::getColor(float *colorArr)
{
    colorArr[0] = color.x;
    colorArr[1] = color.y;
    colorArr[2] = color.z;
    colorArr[3] = color.w;
}

float Particle::getAngle()
{
    return angle;
}

float Particle::getSize()
{
    return size;
}

bool Particle::getDead()
{
    return life <= 0.0;
}
