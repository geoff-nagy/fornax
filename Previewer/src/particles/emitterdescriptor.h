#pragma once

#include "dash/listnode.h"

#include "glm/glm.hpp"

#include <vector>
#include <string>

class ParticleGroup;
class Emitter;

class EmitterDescriptor
{
public:
	// the only way to create an emitter descriptor
	static EmitterDescriptor *fromFile(const std::string &filename);
	static EmitterDescriptor *fromListNode(Dash::ListNode *root, const std::string &name);

	// destructor
	~EmitterDescriptor();

	// adding a particle group to an emitter description
	void addParticleGroup(ParticleGroup *group);

	// create an emitter based on this descriptor
	Emitter *create(glm::dvec3 pos, glm::dvec3 velocity = glm::dvec3(0.0), double scale = 1.0);

private:
	// particles that should be emitted by the emitter
	std::vector<ParticleGroup*> particles;

	// life and radius properties
	double life;
	double radius;

	// private; only for default values
	EmitterDescriptor();
};
