#include "particles/particlegroup.h"
#include "particles/particledescriptor.h"

ParticleGroup::ParticleGroup(ParticleDescriptor *particle)
{
    this -> particle = particle;

    emissionCount = 1;
    initialDelay = 0;
    reEmit = false;
    reEmitInterval = 0;
    enabled = true;
    solo = false;
	spawnCenter = false;
	additiveBlending = false;

    emitterAffectsSize = false;
    emitterAffectsVelocity = false;
    emitterAffectsLifespan = false;
}
