#pragma once

#include "glm/glm.hpp"

#include <vector>

class ParticleDescriptor;
class EmitterDescriptor;

class ParticleConfig
{
public:
	ParticleConfig();
	ParticleConfig(ParticleDescriptor *descriptor, glm::dvec3 pos);
	ParticleConfig(ParticleDescriptor *descriptor, glm::dvec3 pos, glm::dvec3 velocity);

	// what kind of particle we should spawn
	ParticleDescriptor *descriptor;

	// where the particle should appear
	glm::dvec3 pos;

	// how fast the particle moves, if at all
	glm::dvec3 velocity;

	// use additive blending?
	bool additiveBlending;

	// any inherited or effected properties managed by the emitter
	double sizeFactor;
	double velocityFactor;
	double lifeFactor;

	// scaling from parent emitter/particle
	double scale;

	// any emitters belonging to this particle
	std::vector<EmitterDescriptor*> emitters;
};
