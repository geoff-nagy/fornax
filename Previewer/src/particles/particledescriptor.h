#pragma once

#include "dash/listnode.h"

#include "GL/glew.h"

#include "glm/glm.hpp"

#include <map>
#include <string>

class Particle;

class ParticleDescriptor
{
private:
	// private constructor means we must load using fromFile()
	ParticleDescriptor();

public:
	// the only way to create a particle descriptor
	static ParticleDescriptor *fromFile(std::string filename);
	static ParticleDescriptor *fromListNode(Dash::ListNode *root);

	// texture we're using
	GLuint texture;

	// min and max life
	double minLife, maxLife;

	// keyframes for different properties
	std::map<double, glm::dvec3> colorKeyframes;
	std::map<double, double> alphaKeyframes;
	std::map<double, double> velocityKeyframes;
	std::map<double, double> sizeKeyframes;
	std::map<double, double> spinKeyframes;

	// variance values for particle properties
	double velocityVariance;
	double sizeVariance;
	double spinVariance;

	// inserting keyframes
	void addColorKeyframe(double time, glm::dvec3 color);
	void addAlphaKeyframe(double time, double alpha);
	void addVelocityKeyframe(double time, int velocity);
	void addSizeKeyframe(double time, int size);
	void addSpinKeyframe(double time, int spin);
};
