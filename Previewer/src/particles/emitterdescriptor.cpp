#include "particles/emitterdescriptor.h"
#include "particles/emitter.h"
#include "particles/particlegroup.h"
#include "particles/particledescriptor.h"

#include "assets/assetmanager.h"

#include "dash/tree.h"
#include "dash/node.h"
#include "dash/listnode.h"
#include "dash/stringnode.h"
using namespace Dash;

#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
using namespace glm;

#include <iostream>
#include <string>
#include <vector>
using namespace std;

EmitterDescriptor::EmitterDescriptor()
{
	// set some default values that should be overridden
	life = 0.0;
	radius = 0.0;
}

EmitterDescriptor::~EmitterDescriptor() { }

EmitterDescriptor *EmitterDescriptor::fromFile(const string &filename)
{
	Tree *tree = Tree::createFromFile(filename);					// tree container of dash structure
	ListNode *root = dynamic_cast<ListNode*>(tree -> getRoot());	// root of dash structure (should be a list node)
	EmitterDescriptor *result = fromListNode(root, filename);

	delete tree;
	return result;
}

EmitterDescriptor *EmitterDescriptor::fromListNode(ListNode *root, const string &name)
{
	EmitterDescriptor *result = new EmitterDescriptor();				// emitter to return
	ParticleGroup *group;												// a new group to load

	ListNode *part;														// a list node (probably one of several) describing one particle in this emitter
	int particleIndex;													// denotes the (particleIndex)th list node we're loading (starting at 0)

	string directoryPath;
	string particleShortName;											// the short name of the particle inside the current particle group
	string particleFileName;											// the complete path to the particle inside the current particle group
	ParticleDescriptor *particleDescriptor;								// a descriptor for the current particle group's particle

	int childEmitterIndex;												// index of the current particle group's child emitter object
	StringNode *child;													// handle to the string node containing childe emitter name

	bool foundSoloParticle = false;										// is there at least one particle marked 'solo', meaning only those particle should be active?

	// record path information
	directoryPath = name.substr(0, name.rfind('\\') + 1);
	//cout << "LOADING: " << name << endl;
	//cout << "PATH IS: " << directoryPath << endl;

	// read basic emitter properties; we divide by 1000 here because we want floating-point seconds
	result -> life = (double)root -> getNumber("life", 0, 0) / 1000.0;
	result -> radius = (double)root -> getNumber("radius", 0, 0) / 2.0;			// divide by 2 here saves some calculations later on

	// determine if there are any solo particle groups in this emitter; if there are, only those should be used
	particleIndex = 0;
	part = root -> getListNodeByName("particle", particleIndex ++);
	while(!foundSoloParticle && part != NULL)
	{
		// examine the current particle group and see if it is declared solo
		foundSoloParticle = part -> getNumber("solo", 0, 0) == 1;
		part = root -> getListNodeByName("particle", particleIndex++);
	}

	// now read as many particle groups as we can
	particleIndex = 0;
	part = root -> getListNodeByName("particle", particleIndex++);
	while (part != NULL)
	{
		// make sure that the particle is enabled, or marked solo if there is at least one solo particle
		if(part -> getNumber("enabled", 0, 0) && (!foundSoloParticle || part -> getNumber("solo", 0, 0)))
		{
			// read particle descriptor
			particleShortName = part -> getString("name", "");
			particleDescriptor = AssetManager::getParticle(directoryPath + "pds\\" + particleShortName + ".pds");

			// read basic properties from dash file (look how easy that is!!)
			group = new ParticleGroup(particleDescriptor);
			group -> emissionCount = part -> getNumber("emission-count", 0, 0);
			group -> initialDelay = (double)part -> getNumber("initial-delay", 0, 0) / 1000.0;
			group -> reEmit = part -> getNumber("re-emit", 0, 0) == 1;
			group -> reEmitInterval = (double)part -> getNumber("re-emit-interval", 0, 0) / 1000.0;
			group -> enabled = part -> getNumber("enabled", 0, 0) == 1;
			group -> solo = part -> getNumber("solo", 0, 0) == 1;
			group -> spawnCenter = part -> getNumber("spawn-center", 0, 0) == 1;
			group -> additiveBlending = part -> getNumber("additive-blending", 0, 0) == 1;

			// life inheritance properties
			group -> emitterAffectsSize = (part -> getNumber("scale-size", 0, 0)) == 1;
			group -> emitterAffectsVelocity = (part -> getNumber("scale-velocity", 0, 0)) == 1;
			group -> emitterAffectsLifespan = (part -> getNumber("scale-life", 0, 0)) == 1;

			// read in trail properties, if any
            childEmitterIndex = 0;
            child = part -> getStringNodeByName("child-emitter", childEmitterIndex++);
            while(child)
            {
				group -> emitters.push_back(AssetManager::getEmitter(directoryPath + child -> getValue() + ".emt"));
				child = part -> getStringNodeByName("child-emitter", childEmitterIndex++);
            }

			// add the particle group to the emitter
			result -> addParticleGroup(group);
		}

		// and now load the next one
		part = root -> getListNodeByName("particle", particleIndex++);
	}

	return result;
}

void EmitterDescriptor::addParticleGroup(ParticleGroup *group)
{
	particles.push_back(group);
}

Emitter *EmitterDescriptor::create(dvec3 pos, dvec3 velocity, double scale)
{
	Emitter *result = new Emitter();

	result -> setLife(life);
	result -> setRadius(radius);
	result -> setParticleGroups(&particles);
	result -> setPos(pos);
	result -> setVelocity(velocity);
	result -> setScale(scale);

	return result;
}
