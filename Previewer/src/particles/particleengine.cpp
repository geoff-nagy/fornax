#include "particles/particleengine.h"
#include "particles/particlemanager.h"
#include "particles/particleconfig.h"
#include "particles/emitter.h"
#include "particles/emitterdescriptor.h"

#include "assets/assetmanager.h"
#include "assets/shader.h"

#include "util/camera.h"

#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
using namespace glm;

#include <cmath>
#include <iostream>
#include <vector>
using namespace std;

const int ParticleEngine::GARBAGE_COLLECTION_INTERVAL = 2.0;

ParticleEngine *ParticleEngine::instance = NULL;
Shader *ParticleEngine::shader = NULL;
bool ParticleEngine::initialized = false;

ParticleEngine::ParticleEngine()
{
	const int MAX_ADDITIVE_PARTICLES = 500;
	const int MAX_STANDARD_PARTICLES = 7500;//8500;

	init();

	garbageCollectionTimer = GARBAGE_COLLECTION_INTERVAL;

	additiveParticles = new ParticleManager(MAX_ADDITIVE_PARTICLES);
	standardParticles = new ParticleManager(MAX_STANDARD_PARTICLES);
}

ParticleEngine::~ParticleEngine()
{
	vector<Emitter*>::iterator i;

	// delete particle managers
	delete additiveParticles;
	delete standardParticles;

	// delete all buffered emitters
	for(i = buffered.begin(); i != buffered.end(); ++ i)
	{
		delete *i;
	}

	// delete all active emitters
	for(i = emitters.begin(); i != emitters.end(); ++ i)
	{
		delete *i;
	}

	// nullify instance
	instance = NULL;
}

ParticleEngine *ParticleEngine::getInstance()
{
	if(instance == NULL)
	{
		instance = new ParticleEngine();
	}

	return instance;
}

void ParticleEngine::add(ParticleConfig *particle, bool additiveBlending)
{
	if(additiveBlending)
	{
		additiveParticles -> add(particle);
	}
	else
	{
		standardParticles -> add(particle);
	}
}

void ParticleEngine::add(EmitterDescriptor *emitter, dvec3 pos, dvec3 velocity, double scale)
{
	buffered.push_back(emitter -> create(pos, velocity, scale));
}

void ParticleEngine::update(double dt)
{
	vector<Emitter*>::iterator i;

	// control our garbage collection; it only fires periodically to avoid wasting CPU cycles
	garbageCollectionTimer -= dt;
	if(garbageCollectionTimer < 0.0)
	{
		garbageCollectionTimer = GARBAGE_COLLECTION_INTERVAL;
		garbageCollect();
	}

	// add buffered emitters
	for(i = buffered.begin(); i != buffered.end(); ++ i)
	{
		emitters.push_back(*i);
	}
	buffered.clear();

	// update our emitter objects
	for(i = emitters.begin(); i != emitters.end(); ++ i)
	{
		(*i) -> update(dt);
	}

	// remove any dead particles
	additiveParticles -> recycle();
	standardParticles -> recycle();

	// add any new particles to the system
	additiveParticles -> insertBuffered();
	standardParticles -> insertBuffered();

	// update the particle animations
	additiveParticles -> update(dt);
	standardParticles -> update(dt);
}

void ParticleEngine::render()
{
    mat4 view = mat4(Camera::getModelview(Camera::PERSPECTIVE));
    mat4 projection = mat4(Camera::getProjection(Camera::PERSPECTIVE));
    dvec3 cameraRight = Camera::getSide(Camera::PERSPECTIVE);
    dvec3 cameraUp = Camera::getUp(Camera::PERSPECTIVE);

	// turn on the appropriate blending mode
    glEnable(GL_BLEND);

	// make sure the first texture unit is the one we bind to
    glDisable(GL_CULL_FACE);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// set up appropriate rendering options for point sprites
	glDepthMask(GL_FALSE);
	glDisable(GL_DEPTH_TEST);

	// enable our shader
    shader -> bind();
    shader -> uniformMatrix4fv("u_ViewMatrix", 1, value_ptr(view));
    shader -> uniformMatrix4fv("u_ProjectionMatrix", 1, value_ptr(projection));
	shader -> uniformVec3("u_CameraRight", cameraRight);
	shader -> uniformVec3("u_CameraUp", cameraUp);

	// render our standard particles on the bottom
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	standardParticles -> render();

	// render our additive particles on top
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	additiveParticles -> render();

	// disable our shader
	shader -> unbind();

	// turn off point sprite rendering options
	glDepthMask(GL_TRUE);
}

void ParticleEngine::garbageCollect()
{
	vector<Emitter*>::iterator i = emitters.begin();
	Emitter *curr;
	Emitter *toDelete;

	// trash any emitters that have expired and aren't doing anything anymore
	while(i != emitters.end())
	{
		curr = *i;
		if(curr -> isDead())
		{
			// record the emitter for deletion and erase its entry in the list
			toDelete = curr;
			i = emitters.erase(i);

			// finally, free its memory
			delete toDelete;				// segfault occurs here!!
		}
		else
		{
			i ++;
		}
	}
}

int ParticleEngine::getNumActiveParticles()
{
	return standardParticles -> getNumActiveParticles() + additiveParticles -> getNumActiveParticles();
}

void ParticleEngine::init()
{
	if(!initialized)
	{
		initialized = true;

		// prepare our shader object
		shader = AssetManager::getShader("shaders/particle-preview");
		shader -> bindAttrib("a_Position", 0);
		shader -> bindAttrib("a_Color", 1);
		shader -> bindAttrib("a_Size", 2);
		shader -> bindAttrib("a_Angle", 3);
		shader -> link();
		shader -> bind();
		shader -> uniform1i("u_Texture", 0);
		shader -> unbind();
	}
}
