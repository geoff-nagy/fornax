#pragma once

#include "glm/glm.hpp"
#include "GL/glew.h"
#include <vector>

class ParticleConfig;
class Particle;

class ParticleManager
{
private:
	static bool initialized;

	static void init();

	int maxParticles;										// number of active particles we allow at one time
	int freeParticleIndex;									// decent guess as to where the next unused particle is located in our pool

	int numInActivePool;									// how many particles are active in our active particle list
	int numToRecycle;										// used to prevent unnecessary iteration when removing dead particles from active pool

	GLuint vao;												// vertex array that encapsulates vertex buffer states
	GLuint vbo;												// vertex buffer object

	Particle *particlePool;									// we allocate a large pool of particles and draw from it/replace as necessary
	Particle **activeParticles;								// only contains the particles that are currently active
	std::vector<int> textureIndices;						// where each texture index begins inside the sorted (by texture) list of active particles
	std::vector<ParticleConfig> buffered;					// particles waiting to be inserted into the active particle list

	float *vertexAttribData;								// where we store the vertex attributes for each particle texture group

	void insertIntoActivePool(Particle *particle);
	Particle *getFreeParticle();

public:
	ParticleManager(int maxParticles);
	~ParticleManager();

	void add(ParticleConfig *particle);

	void insertBuffered();
	void update(double dt);
	void recycle();
	void render();

	int getNumActiveParticles();
};
