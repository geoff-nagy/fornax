#include "particles/particledescriptor.h"
#include "particles/particle.h"

#include "assets/assetmanager.h"

#include "dash/tree.h"
#include "dash/listnode.h"
#include "dash/node.h"
#include "dash/numbersnode.h"
#include "dash/stringnode.h"
using namespace Dash;

#include "GL/glew.h"
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
using namespace glm;

#include <map>
#include <string>
#include <iostream>
using namespace std;

ParticleDescriptor::ParticleDescriptor()
{
	// set some default values that should be overridden
	texture = 0;
	velocityVariance = 0.0;
	sizeVariance = 0.0;
	spinVariance = 0.0;
}

ParticleDescriptor *ParticleDescriptor::fromFile(string filename)
{
	Tree *tree = Tree::createFromFile(filename);					// tree container of dash structure
	ListNode *root = dynamic_cast<ListNode*>(tree -> getRoot());	// root of dash structure (should be a list node)
	ParticleDescriptor *result = fromListNode(root);

	delete tree;
	return result;
}

ParticleDescriptor *ParticleDescriptor::fromListNode(ListNode *root)
{
	ParticleDescriptor *result = new ParticleDescriptor();			// particle to return

	NumbersNode *keyframe;											// a number node (probably one of several) describing one keyframe in this particle
	int keyframeIndex;												// denotes the (keyframeIndex)th list node we're loading (starting at 0)

	double life;
	double lifeVariance;

	// read texture
	result -> texture = AssetManager::getTexture(root -> getString("texture", ""));

	// compute allowed range of particle life
	life = (double)root -> getNumber("life", 0, 1.0) / 1000.0;
	lifeVariance = (double)root -> getNumber("life-variance", 0, 0) / 100.0;
	result -> minLife = life - (life * lifeVariance);
	result -> maxLife = life + (life * lifeVariance);

	// read property variances
	result -> velocityVariance = (double)root -> getNumber("velocity-variance", 0, 0) / 200.0;
	result -> sizeVariance = (double)root -> getNumber("size-variance", 0, 0) / 200.0;
	result -> spinVariance = (double)root -> getNumber("spin-variance", 0, 0) / 200.0;

	// read color keyframes
	keyframeIndex = 0;
	keyframe = root -> getNumbersNodeByName("color-keyframe", keyframeIndex++);
	while (keyframe != NULL)
	{
		result -> addColorKeyframe((double)keyframe -> at(0) / 100.0,
									dvec3((double)keyframe -> at(1) / 255.0,
										  (double)keyframe -> at(2) / 255.0,
										  (double)keyframe -> at(3) / 255.0));
		keyframe = root -> getNumbersNodeByName("color-keyframe", keyframeIndex++);
	}

	// read alpha keyframes
	keyframeIndex = 0;
	keyframe = root -> getNumbersNodeByName("alpha-keyframe", keyframeIndex++);
	while (keyframe != NULL)
	{
		result -> addAlphaKeyframe((double)keyframe -> at(0) / 100.0, (double)keyframe -> at(1) / 255.0);
		keyframe = root -> getNumbersNodeByName("alpha-keyframe", keyframeIndex++);
	}

	// read velocity keyframes
	keyframeIndex = 0;
	keyframe = root -> getNumbersNodeByName("velocity-keyframe", keyframeIndex++);
	while (keyframe != NULL)
	{
		result -> addVelocityKeyframe((double)keyframe -> at(0) / 100.0, (double)keyframe -> at(1));
		keyframe = root -> getNumbersNodeByName("velocity-keyframe", keyframeIndex++);
	}

	// read size keyframes
	keyframeIndex = 0;
	keyframe = root -> getNumbersNodeByName("size-keyframe", keyframeIndex++);
	while (keyframe != NULL)
	{
		result -> addSizeKeyframe((double)keyframe -> at(0) / 100.0, (double)keyframe -> at(1));
		keyframe = root -> getNumbersNodeByName("size-keyframe", keyframeIndex++);
	}

	// read spin keyframes
	keyframeIndex = 0;
	keyframe = root -> getNumbersNodeByName("spin-keyframe", keyframeIndex++);
	while (keyframe != NULL)
	{
		result -> addSpinKeyframe((double)keyframe -> at(0) / 100.0, radians((double)keyframe -> at(1)));
		keyframe = root -> getNumbersNodeByName("spin-keyframe", keyframeIndex++);
	}

	return result;
}

void ParticleDescriptor::addColorKeyframe(double time, dvec3 color)
{
	colorKeyframes[time] = color;
}

void ParticleDescriptor::addAlphaKeyframe(double time, double alpha)
{
	alphaKeyframes[time] = alpha;
}

void ParticleDescriptor::addVelocityKeyframe(double time, int velocity)
{
	velocityKeyframes[time] = velocity;
}

void ParticleDescriptor::addSizeKeyframe(double time, int size)
{
	sizeKeyframes[time] = size;
}

void ParticleDescriptor::addSpinKeyframe(double time, int spin)
{
	spinKeyframes[time] = spin;
}
