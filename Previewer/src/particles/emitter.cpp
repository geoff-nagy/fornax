#include "particles/emitter.h"
#include "particles/emitterdescriptor.h"
#include "particles/particledescriptor.h"
#include "particles/particleconfig.h"
#include "particles/particle.h"
#include "particles/particlegroup.h"
#include "particles/particleengine.h"

#include "dash/tree.h"
#include "dash/listnode.h"
using namespace Dash;

#include "glm/glm.hpp"
#include "glm/gtc/random.hpp"
using namespace glm;

#include <map>
#include <vector>
#include <string>
#include <iostream>
using namespace std;

Emitter::Emitter()
{
	life = 0.0;
	maxLife = 1.0;
	radius = 0.0;
	//burstEmitted = false;
	scale = 1.0;
}

void Emitter::setLife(double life)
{
	this -> life = life;
	maxLife = life;
}

void Emitter::setRadius(double radius)
{
	this -> radius = radius;
}

void Emitter::setParticleGroups(vector<ParticleGroup*> *particles)
{
	vector<ParticleGroup*>::iterator i;
	ParticleGroup *curr;

	// assign particles to emit
	this -> particles = particles;

	// build timer objects to track repeated emissions
	for(i = particles -> begin(); i != particles -> end(); i ++)
	{
		curr = *i;
		initialTimers[curr] = curr -> initialDelay;
		burstEmitted[curr] = false;
		if(curr -> reEmit)
		{
			repeatTimers[curr] = curr -> reEmitInterval;
		}
	}
}

void Emitter::setPos(const dvec3 &pos)
{
	this -> pos = pos;
}

void Emitter::setVelocity(const dvec3 &velocity)
{
	this -> velocity = velocity;
}

void Emitter::setScale(double scale)
{
	this -> scale = scale;
}

void Emitter::update(double dt)
{
	vector<ParticleGroup*>::iterator i;
	vector<EmitterDescriptor*>::iterator k;

	ParticleGroup *curr;
	ParticleConfig config;

	dvec3 emitPos;
	double sizeFactor;
	double velocityFactor;
	double lifeFactor;
	bool emit;
	int j;

	// the emitter might die out, so handle that case
	if(life > 0.0)
	{
		// advance life
		life -= dt;
		lifeFactor = glm::clamp(1.0 - (life / maxLife), 0.0, 1.0);

		// move forward
		pos = pos + velocity * dt;

		// emit particle children
		for(i = particles -> begin(); i != particles -> end(); i ++)
		{
			curr = *i;
			emit = false;

			// make sure the particle's initial delay has been overcome
			initialTimers[curr] -= dt;
			if(initialTimers[curr] <= 0.0)
			{
				// emit the single burst if we haven't already
				if(!burstEmitted[curr])
				{
					emit = true;
				}

				// some particle groups might also be released repeatedly, so take those into account too
				if(curr -> reEmit)
				{
					repeatTimers[curr] -= dt;
					if(repeatTimers[curr] <= 0.0)
					{
						emit = true;
						repeatTimers[curr] = curr -> reEmitInterval;
					}
				}

				// emit the particles if it's time to do that
				if(emit)
				{
					// compute emitter life contributions to particle properties
					sizeFactor = 1.0 - (curr -> emitterAffectsSize * (lifeFactor));
					velocityFactor = 1.0 - (curr -> emitterAffectsVelocity * (lifeFactor));
					lifeFactor = 1.0 - (curr -> emitterAffectsLifespan * (lifeFactor));

					// emit the particles
					for(j = 0; j < curr -> emissionCount; j ++)
					{
						emitPos = pos;

						// if the particle does not spawn at the emitter's center directly, compute a position within it
						if(!curr -> spawnCenter)
						{
							emitPos = emitPos + normalize(dvec3(linearRand(-1.0, 1.0), linearRand(-1.0, 1.0), linearRand(-1.0, 1.0))) * linearRand(-radius, radius) * scale;
						}

						// set particle properties
						config = ParticleConfig(curr -> particle, emitPos);
						config.additiveBlending = curr -> additiveBlending;
						config.emitters = curr -> emitters;
						config.sizeFactor = sizeFactor;
						config.velocityFactor = velocityFactor;
						config.lifeFactor = lifeFactor;
						config.velocity = velocity;
						config.scale = scale;

						// now add the particle to the engine
						ParticleEngine::getInstance() -> add(&config, curr -> additiveBlending);
					}

					burstEmitted[curr] = true;
				}
			}
		}

		//burstEmitted = true;
		// = Suzie loves you very, very much, and always will!! XOXOXOXOXO :)
	}
}

bool Emitter::isDead()
{
	return life <= 0.0;
}
