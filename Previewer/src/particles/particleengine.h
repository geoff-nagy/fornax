#pragma once

#include "glm/glm.hpp"

#include <vector>

class Emitter;
class EmitterDescriptor;
class ParticleManager;
class ParticleConfig;
class Shader;
class Camera;

class ParticleEngine
{
private:
	static const int GARBAGE_COLLECTION_INTERVAL;			// we only fire the garbage collector periodically

	static ParticleEngine *instance;						// our singleton instance
	static Shader *shader;									// the shader we use for rendering
	static bool initialized;								// have we done our initial set up?

	static void init();

	ParticleManager *additiveParticles;						// for efficiency, we group additive-blended and standard-
	ParticleManager *standardParticles;						// blended particles together when rendering

	std::vector<Emitter*> buffered;							// emitters waiting to be inserted into the world
	std::vector<Emitter*> emitters;							// the particle emitters active in the world

	double garbageCollectionTimer;							// controls our garbage collection timing

	ParticleEngine();										// forces use of factory method

	void garbageCollect();									// collects old emitters

public:
	static ParticleEngine *getInstance();

	~ParticleEngine();

	// add a particle or emitter to our world
	void add(ParticleConfig *particle, bool additiveBlending);
	void add(EmitterDescriptor *emitter, glm::dvec3 pos, glm::dvec3 velocity = glm::dvec3(0.0), double scale = 1.0);

	void update(double dt);
	void render();

	int getNumActiveParticles();
};
