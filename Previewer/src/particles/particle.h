#pragma once

#include "glm/glm.hpp"
#include "GL/glew.h"

#include <map>
#include <vector>

class Emitter;
class ParticleConfig;

class Particle
{
public:
	Particle();
	~Particle();

	void configure(ParticleConfig config);

	void update(double dt);
	// no render() is supplied; particle manager renders everything as a VBO

	void getPos(float *posArr);
	void getColor(float *colorArr);
	float getAngle();
	float getSize();
	bool getDead();
	GLuint getTexture();

private:
	// keyframes for different properties
	std::map<double, glm::dvec3> *colorKeyframes;
	std::map<double, double> *alphaKeyframes;
	std::map<double, double> *velocityKeyframes;
	std::map<double, double> *sizeKeyframes;
	std::map<double, double> *spinKeyframes;

	// the two colour keyframes we're between
	std::map<double, glm::dvec3>::iterator prevColor;
	std::map<double, glm::dvec3>::iterator nextColor;

	// the two alpha keyframes we're between
	std::map<double, double>::iterator prevAlpha;
	std::map<double, double>::iterator nextAlpha;

	// the two velocity keyframes we're between
	std::map<double, double>::iterator prevVelocity;
	std::map<double, double>::iterator nextVelocity;

	// the two size keyframes we're between
	std::map<double, double>::iterator prevSize;
	std::map<double, double>::iterator nextSize;

	// the two spin keyframes we're between
	std::map<double, double>::iterator prevSpin;
	std::map<double, double>::iterator nextSpin;

	// interpolation of properties keyframes
	glm::dvec3 getCurrentColor(double lifeFactor);
	double getCurrentAlpha(double lifeFactor);
	double getCurrentVelocity(double lifeFactor);
	double getCurrentSize(double lifeFactor);
	double getCurrentSpin(double lifeFactor);

	GLuint texture;
	glm::dvec4 color;
	double size;
	double sizeFactor;

	double scale;

	glm::dvec3 baseVelocity;					// inherited from parent emitter

	glm::dvec3 velocityDirection;				// direction that the particle moves
	double spinDirection;						// direction that the particles spin

	glm::dvec3 pos;
	double angle;
	bool dead;

	double life, maxLife;

	std::vector<Emitter*> childEmitters;

	void addChildEmitter(Emitter *childEmitter);
};
