#pragma once

#include "GL/glew.h"

#include "glm/glm.hpp"

#include <string>

struct GLFWwindow;
struct dtx_font;

class ParticleEngine;
class EmitterDescriptor;
class Shader;

class Previewer
{
private:
	static const int NUM_QUAD_VERTICES;

	static Previewer *instance;

	GLFWwindow *window;					// handle to our OpenGL window

	// delta time tracking for smooth animations when not in export mode
	double oldTime;
	double newTime;

	// particle emitter filename and export options
	std::string emitterFilename;
	bool exportToPNG;
	int frameRate;
	int resolution;
	std::string outputDir;
	std::string outputPrefix;

	// current frame index for exporting
	int currentFrame;

	// text font handling
	Shader *textShader;
	struct dtx_font *font;
	glm::mat4 fontModelMat;

	// our framebuffer handling
	GLuint sceneFBO;
	GLuint sceneTexture;

	// quad rendering
	Shader *quadShader;
	GLuint quadVAO;
	GLuint quadVBOs[2];

	// our particle engine and the emitter we want to test
	ParticleEngine *particles;
	EmitterDescriptor *effect;

	// the viewing angle can be adjusted with the mouse by clicking and dragging
	glm::dvec2 oldMousePos;
	glm::dvec2 viewAngle;

	Previewer();

	void init(const std::string &emitterFilename, bool exportToPNG, int frameRate, int resolution, const std::string &outputDir, const std::string &outputPrefix);

	void initWindow();
	void initText();
	void initSceneFBO();
	void initQuadShader();
	void initQuadVBO();
	void initView();
	void initParticles();

	void getUserInput();
	void updateViewAngle();

	void exportFrame();

	void mainLoop();

public:
	static Previewer *getInstance();

	~Previewer();

	void initPreviewMode(const std::string &emitterFilename);
	void initExportMode(const std::string &emitterFilename, int frameRate, int resolution, const std::string &outputDir, const std::string &outputPrefix);

	void run();
};
