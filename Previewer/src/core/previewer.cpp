#include "core/previewer.h"

#include "assets/assetmanager.h"
#include "assets/shader.h"

#include "particles/particleengine.h"
#include "particles/emitter.h"
#include "particles/emitterdescriptor.h"

#include "util/camera.h"
#include "util/directories.h"
#include "util/gldebugging.h"

#include "lodepng/lodepng.h"

#include "libdrawtext/drawtext.h"

#include "GLFW/glfw3.h"

#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/rotate_vector.hpp"
using namespace glm;

#include <cstdlib>
#include <sstream>
#include <iostream>
#include <string>
using namespace std;

const int Previewer::NUM_QUAD_VERTICES = 4;

Previewer *Previewer::instance = NULL;

Previewer *Previewer::getInstance()
{
	if(instance == NULL)
	{
		instance = new Previewer();
	}

	return instance;
}

Previewer::Previewer()
{
	currentFrame = 0;
}

Previewer::~Previewer()
{
	delete particles;
}

void Previewer::initPreviewMode(const string &emitterFilename)
{
	init(emitterFilename, false, 60, 512, "", "");
}

void Previewer::initExportMode(const string &emitterFilename, int frameRate, int resolution, const string &outputDir, const string &outputPrefix)
{
	init(emitterFilename, true, frameRate, resolution, outputDir, outputPrefix);
}

void Previewer::init(const string &emitterFilename, bool exportToPNG, int frameRate, int resolution, const string &outputDir, const string &outputPrefix)
{
	this -> emitterFilename = emitterFilename;
	this -> exportToPNG = exportToPNG;
	this -> frameRate = frameRate;
	this -> resolution = resolution;
	this -> outputDir = outputDir;
	this -> outputPrefix = outputPrefix;

	initWindow();
	initText();
	initSceneFBO();
	initQuadShader();
	initQuadVBO();
	initView();
	initParticles();
}

void Previewer::initWindow()
{
	// window characteristics
	const string TITLE = string("") + (exportToPNG ? "[EXPORT]: " : "[PREVIEW]: ") + emitterFilename;

	// GL error detected, if any
	GLenum error;

	// we need to intialize GLFW before we create a GLFW window
	if(!glfwInit())
	{
		cerr << "-- Previewer::initWindow() could not initialize GLFW" << endl;
		exit(1);
	}

	// explicitly set our OpenGL context to something that doesn't support any old-school shit
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_REFRESH_RATE, 60);
	glfwWindowHint(GLFW_RESIZABLE, 0);
	glfwWindowHint(GLFW_DECORATED, 1);
	glfwWindowHint(GLFW_FOCUSED, 1);
	#ifdef DEBUG
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
	#endif

	// create our OpenGL window using GLFW
    window = glfwCreateWindow(resolution, resolution,							// specify width and height
							  TITLE.c_str(),									// title of window
							  NULL,												// always windowed mode
							  NULL);											// not sharing resources across monitors
	glfwMakeContextCurrent(window);

	// turn on vsync
	glfwSwapInterval(1);

	// configure our viewing area
	glViewport(0, 0, resolution, resolution);

	// enable our extensions handler
	glewExperimental = true;		// GLEW bug: glewInit() doesn't get all extensions, so we have it explicitly search for everything it can find
	error = glewInit();
	if(error != GLEW_OK)
	{
		cerr << "-- could not initialize GLEW! You probably have out-of-date drivers or a very old machine!" << endl;
		cerr << glewGetErrorString(error) << endl;
		exit(1);
	}

	// clear the OpenGL error code that results from initializing GLEW
	glGetError();

	// print our OpenGL version info
	#ifdef DEBUG
	 	cout << "-- GL version:   " << (char*)glGetString(GL_VERSION) << endl;
		cout << "-- GL vendor:    " << (char*)glGetString(GL_VENDOR) << endl;
		cout << "-- GL renderer:  " << (char*)glGetString(GL_RENDERER) << endl;
		cout << "-- GLSL version: " << (char*)glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
	#endif

	// OpenGL callback debugging is extremely useful, so enable it if we're in debug mode
	#ifdef DEBUG
		initGLDebugger();
	#endif
}

void Previewer::initText()
{
	// full screen canvas quad size; the view and model matrices can just be the identity mat
	mat4 canvasMVPMatrix = ortho(0.0f, (float)resolution, 0.0f, (float)resolution, -1.0f, 1.0f);
	mat4 fontModelMat;

	// where the font goes
	fontModelMat = mat4(1.0f);
	fontModelMat = translate(fontModelMat, vec3(8.0f, resolution - 15.0f, 0.0));

	// initialize libdrawtext and load our font
	dtx_gl_init();
	font = dtx_open_font("ttf/consola.ttf", 12);

	dtx_prepare(font, 12);
	dtx_use_font(font, 12);
	dtx_use_interpolation(1);

	textShader = new Shader("shaders/text.vert", "shaders/text.frag");
    textShader -> bindAttrib("a_Vertex", 0);
    textShader -> bindAttrib("a_TexCoord", 1);
    textShader -> link();
	textShader -> bind();
	textShader -> uniform1i("u_Texture", 0);
	textShader -> uniformVec4("u_Color", vec4(1.0f));
	textShader -> uniformMatrix4fv("u_ProjectionMatrix", 1, (GLfloat*)value_ptr(canvasMVPMatrix));
	textShader -> uniformMatrix4fv("u_ModelviewMatrix", 1, (GLfloat*)value_ptr(fontModelMat));
	textShader -> unbind();
}

void Previewer::initSceneFBO()
{
	// entire scene gets rendered into an FBO for export and display
	glGenFramebuffers(1, &sceneFBO);
	glGenTextures(1, &sceneTexture);
	glBindFramebuffer(GL_FRAMEBUFFER, sceneFBO);
	glBindTexture(GL_TEXTURE_2D, sceneTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, resolution, resolution, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, sceneTexture, 0);
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		cerr << "-- Previewer::initFBO(): incomplete scene FBO " << glCheckFramebufferStatus(GL_FRAMEBUFFER) << endl;
		exit(1);
	}

	// unbind framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Previewer::initQuadShader()
{
	// full screen canvas quad size; the view and model matrices can just be the identity mat
	mat4 canvasMVPMatrix = ortho(-0.5f, 0.5f, -0.5f, 0.5f, -1.0f, 1.0f);

	quadShader = new Shader("shaders/solid.vert", "shaders/solid.frag");
    quadShader -> bindAttrib("a_Vertex", 0);
    quadShader -> bindAttrib("a_TexCoord", 1);
    quadShader -> link();
	quadShader -> bind();
	quadShader -> uniform1i("u_Texture", 0);
	quadShader -> uniformVec4("u_Color", vec4(1.0f));
	quadShader -> uniformMatrix4fv("u_MVPMatrix", 1, (GLfloat*)value_ptr(canvasMVPMatrix));
	quadShader -> unbind();
}

void Previewer::initQuadVBO()
{
	const vec2 VERTICES[] = {vec2(-0.5, -0.5),
							 vec2(-0.5, 0.5),
							 vec2(0.5, -0.5),
							 vec2(0.5, 0.5)};
	const vec2 TEX_COORDS[] = {vec2(0.0, 0.0),
							   vec2(0.0, 1.0),
							   vec2(1.0, 0.0),
							   vec2(1.0, 1.0)};

	// generate our vertex array object and buffers
	glGenVertexArrays(1, &quadVAO);
	glBindVertexArray(quadVAO);
	glGenBuffers(2, quadVBOs);

	// throw the vertex data onto the GPU
	glBindBuffer(GL_ARRAY_BUFFER, quadVBOs[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * NUM_QUAD_VERTICES, VERTICES, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);

	// throw the tex coord data onto the GPU
	glBindBuffer(GL_ARRAY_BUFFER, quadVBOs[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * NUM_QUAD_VERTICES, TEX_COORDS, GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
}

void Previewer::initView()
{
	const double FOV = 45.0;
	const double ASPECT_RATIO = 1.0f;		// since we use a square resolution
	const double Z_NEAR = 0.1;
	const double Z_FAR = 3000.0;

	Camera::configurePerspectiveMode(FOV, ASPECT_RATIO, Z_NEAR, Z_FAR);

	const double OFFSET = 150.0;//150.0;
	const dvec3 LOOK_AT_POS(0.0, 0.0, 0.0);
	const vec3 UP_VEC(0.0, 1.0, 0.0);

	dvec3 pos = dvec3(OFFSET * sinf(viewAngle.x), 0.0, OFFSET * cosf(viewAngle.x));
	Camera::lookAt(Camera::PERSPECTIVE, pos, LOOK_AT_POS, UP_VEC);

	updateViewAngle();
}

void Previewer::initParticles()
{
	effect = AssetManager::getEmitter(emitterFilename);
	particles = ParticleEngine::getInstance();
}

void Previewer::getUserInput()
{
/*
	const double TURN_SENSITIVITY = 0.05;
	dvec2 mousePos;
	dvec2 diff;

	mousePos = arl::getMousePos();
	diff = mousePos - oldMousePos;
	oldMousePos = mousePos;

	if(arl::getMouseButtonDown(arl::MOUSE_BUTTON_LEFT))
	{
		viewAngle.x += diff.x * TURN_SENSITIVITY;
		viewAngle.y += diff.y * TURN_SENSITIVITY;
	}*/

	updateViewAngle();
}

void Previewer::updateViewAngle()
{
	const double OFFSET = 150.0;//150.0;
	const dvec3 LOOK_AT_POS(0.0, 0.0, 0.0);
	const vec3 UP_VEC(0.0, 1.0, 0.0);

	dvec3 pos = dvec3(OFFSET * sinf(viewAngle.x), 0.0, OFFSET * cosf(viewAngle.x));
	Camera::lookAt(Camera::PERSPECTIVE, pos, LOOK_AT_POS, UP_VEC);
}

void Previewer::exportFrame()
{
	unsigned char *pixels = new unsigned char[resolution * resolution * 4];
	stringstream ss;

	// make sure target directory exists
	if(!dirDoesDirectoryExist(outputDir))
	{
		dirMakeDirectory(outputDir);
	}

	// build the filename of the current frame
	ss << outputDir << "/" << outputPrefix << currentFrame << ".png";

	// retrieve the pixel data from the current frame
	glBindTexture(GL_TEXTURE_2D, sceneTexture);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

	// save the data to a PNG file with the appropriate name and location
	lodepng_encode32_file(ss.str().c_str(), pixels, resolution, resolution);
}

void Previewer::mainLoop()
{
	const double EXPORT_TIME_DT = 1.0 / (double)frameRate;
	const double MIN_DT = 0.0001;
	const double MAX_DT = 1.0 / 60.0;//EXPORT_TIME_DT * 4.0;

	const dvec3 EFFECT_POS(0.0);
	const double WAIT_TIME = 0.1;

	const int STATE_WAITING = 0;
	const int STATE_PLAYING = 1;
	const int STATE_DONE_EXPORT = 2;

	stringstream ss;

	int state = STATE_WAITING;
	double delayTimer = 0.0f;
	double dt;

	// exit when ESC is pressed or the user closes the window with the 'X'
	while(!glfwGetKey(window, GLFW_KEY_ESCAPE) && !glfwWindowShouldClose(window) && state != STATE_DONE_EXPORT)
	{
		// if we're exporting, render as fast as possible; otherwise, render at the real-time rate
		if(exportToPNG)
		{
			dt = EXPORT_TIME_DT;
		}
		else
		{
			// inefficient busy-wait
			//oldTime = glfwGetTime();
			//while(glfwGetTime() - oldTime < EXPORT_TIME_DT);

			// get current time step
			//newTime = glfwGetTime();
			//dt = clamp(newTime - oldTime, MIN_DT, MAX_DT);

			oldTime = newTime;
			newTime = glfwGetTime();
			//dt = newTime - oldTime;
			dt = clamp(newTime - oldTime, MIN_DT, MAX_DT);
		}

		// rotate, etc.
		getUserInput();

		// waiting a brief period before firing the emitter again
		if(state == STATE_WAITING)
		{
			delayTimer -= dt;
			if(delayTimer < 0.0)
			{
				delayTimer = WAIT_TIME;
				state = STATE_PLAYING;

				particles -> add(effect, EFFECT_POS);
			}
		}
		// waiting for the particles to all die off before starting again
		else if(state == STATE_PLAYING)
		{
			if(particles -> getNumActiveParticles() == 0)
			{
				state = (exportToPNG ? STATE_DONE_EXPORT : STATE_WAITING);
			}
		}

		// control the particle animations
		particles -> update(dt);

		// prepare to render the particle animations into the FBO
		glBindFramebuffer(GL_FRAMEBUFFER, sceneFBO);
		glViewport(0, 0, resolution, resolution);
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);					// alpha background
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// draw the particles---this rendered image goes into sceneTexture
		particles -> render();

		// export the frame we just rendered, if enabled
		if(exportToPNG && state == STATE_PLAYING)
		{
			exportFrame();
		}

		// advance to next frame index
		currentFrame ++;

		// now show the rendered scene using the full-screen quad
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, resolution, resolution);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);					// black background
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// must disable blending for correct rendering against window-provided framebuffer
		glDisable(GL_BLEND);

		// bind the scene texture
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, sceneTexture);

		// prepare the quad shader
		quadShader -> bind();

		// render the quad containing the rendered scene texture
		glBindVertexArray(quadVAO);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, NUM_QUAD_VERTICES);

		// show text describing the frame and frame rate information
		ss.str("");
		ss << "fps       : " << (int)(1.0f / dt) << endl;
		ss << "size      : " << resolution << endl;
		ss << "export    : " << (exportToPNG ? "ON" : "OFF") << endl;
		ss << "frame     : " << currentFrame << endl;
		ss << "particles : " << particles -> getNumActiveParticles() << endl;
		glEnable(GL_BLEND);
		glDisable(GL_DEPTH_TEST);
		textShader -> bind();
		dtx_string(ss.str().c_str());
		dtx_flush();

		// swap buffers and check for input
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
}

void Previewer::run()
{
	mainLoop();
}
