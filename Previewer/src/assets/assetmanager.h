#pragma once

#include "gl/glew.h"

#include <vector>
#include <map>
#include <string>

class EmitterDescriptor;
class ParticleDescriptor;
class Shader;

class AssetManager
{
private:
	enum AssetType
	{
		TYPE_EMITTER,
		TYPE_PARTICLE,
		TYPE_SHADER,
		TYPE_TEXTURE
	};

	// assets that are loaded, indexed by load time (milliseconds)
	static int loadingBeginTime;
	static std::map<int, std::vector<std::string> > loadTimes;

	// assets that are being prepared for loading
	static std::vector<std::string> preparedAssetNames;
	static std::vector<AssetType> preparedAssetTypes;

	// fonts are prepared differently because they have a size component
	static std::vector<std::pair<std::string, int> > preparedFonts;

	// for asset loading tracking
	static void beginTimer();
	static void endTimer(std::string name);

	// loads an asset based on its type and places it into the specified asset list
	static void loadAsset(AssetType type, std::string name);

	// asset lists
	static std::map<std::string, EmitterDescriptor*> emitters;
	static std::map<std::string, ParticleDescriptor*> particles;
	static std::map<std::string, Shader*> shaders;
	static std::map<std::string, GLuint> textures;

public:
	// prepared assets are not immediately loaded from file, but placed into a list
	static void prepareEmitter(std::string emitter);
	static void prepareParticle(std::string particle);
	static void prepareShader(std::string shader);
	static void prepareTexture(std::string texture);

	// loading prepared assets
	static void loadAllPreparedAssets();						// loads all remaining prepared assets, all at once
	static void loadNextPreparedAsset();						// loads the next remaining prepared asset, for progress indicators
	static int getNumPreparedAssets();							// returns number of remaining prepared assets

	// these methods retrieve the named asset and return it, loading it from file if not already loaded
	static EmitterDescriptor *getEmitter(std::string name);
	static ParticleDescriptor *getParticle(std::string name);
	static Shader *getShader(std::string name);
	static GLuint getTexture(std::string name, bool mipmapping = true);

	// print out the load times for the resources we've loaded so far
	static void printLoadTimes();
};
