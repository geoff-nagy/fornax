#include "assets/assetmanager.h"
#include "assets/shader.h"

#include "particles/emitterdescriptor.h"
#include "particles/particledescriptor.h"

#include "soil/soil.h"

#include "gl/glew.h"

#include <iostream>
#include <vector>
#include <string>
#include <windows.h>
#include <winbase.h>
using namespace std;

int AssetManager::loadingBeginTime = 0;
std::map<int, std::vector<std::string> > AssetManager::loadTimes;

vector<string> AssetManager::preparedAssetNames;
vector<AssetManager::AssetType> AssetManager::preparedAssetTypes;

map<string, EmitterDescriptor*> AssetManager::emitters;
map<string, ParticleDescriptor*> AssetManager::particles;
map<string, Shader*> AssetManager::shaders;
map<string, GLuint> AssetManager::textures;

// prepared assets are not immediately loaded from file, but placed into a list

void AssetManager::prepareEmitter(string emitter)
{
	preparedAssetNames.push_back(emitter);
	preparedAssetTypes.push_back(TYPE_EMITTER);
}

void AssetManager::prepareParticle(string particle)
{
	preparedAssetNames.push_back(particle);
	preparedAssetTypes.push_back(TYPE_PARTICLE);
}

void AssetManager::prepareShader(string shader)
{
	preparedAssetNames.push_back(shader);
	preparedAssetTypes.push_back(TYPE_SHADER);
}

void AssetManager::prepareTexture(string texture)
{
	preparedAssetNames.push_back(texture);
	preparedAssetTypes.push_back(TYPE_TEXTURE);
}

// loading prepared assets

void AssetManager::loadAllPreparedAssets()
{
	vector<string>::iterator i = preparedAssetNames.begin();
	vector<AssetType>::iterator j = preparedAssetTypes.begin();

	while(i != preparedAssetNames.end() && j != preparedAssetTypes.end())
	{
		loadAsset(*j, *i);
	}

	preparedAssetNames.clear();
	preparedAssetTypes.clear();
}

void AssetManager::loadNextPreparedAsset()
{
	if((int)preparedAssetNames.size() > 0)
	{
		loadAsset(preparedAssetTypes.back(), preparedAssetNames.back());
		preparedAssetNames.pop_back();
		preparedAssetTypes.pop_back();
	}
}

int AssetManager::getNumPreparedAssets()
{
	return (int)preparedAssetNames.size();
}

void AssetManager::loadAsset(AssetType type, string name)
{
	if(type == TYPE_EMITTER)
	{
		getEmitter(name);
	}
	else if(type == TYPE_PARTICLE)
	{
		getParticle(name);
	}
	else if(type == TYPE_SHADER)
	{
		getShader(name);
	}
	else if(type == TYPE_TEXTURE)
	{
		getTexture(name, true);
	}
}

EmitterDescriptor *AssetManager::getEmitter(std::string name)
{
    EmitterDescriptor *result = NULL;

    if(emitters.find(name) != emitters.end())
        result = emitters[name];
    else
    {
        beginTimer();
        result = EmitterDescriptor::fromFile(name);

        if(result)
        {
			endTimer(name);
            emitters[name] = result;
		}
        else
        {
            result = NULL;
            delete result;

			cout << "AssetManager::getEmitter() could not load '" << name.c_str() << "'" << endl;
        }
    }

    return result;
}

ParticleDescriptor *AssetManager::getParticle(std::string name)
{
    ParticleDescriptor *result = NULL;

    if(particles.find(name) != particles.end())
    {
        result = particles[name];
	}
    else
    {
		//result = new ParticleDescriptor();
		beginTimer();
		result = ParticleDescriptor::fromFile(name);

        if(result)
        {
			endTimer(name);
            particles[name] = result;
		}
        else
        {
			cout << "AssetManager::getParticle() could not load '" << name.c_str() << "'" << endl;
        }
    }

    return result;
}

Shader *AssetManager::getShader(std::string name)
{
    Shader *result = NULL;

    if(shaders.find(name) != shaders.end())
        result = shaders[name];
    else
    {
		beginTimer();
		result = new Shader(name + ".vert", name + ".frag");

        if(result)
        {
			endTimer(name);
            shaders[name] = result;
		}
        else
        {
			cout << "AssetManager::getShader() could not load '" << name.c_str() << "'" << endl;
			delete result;
			result = NULL;
        }
    }

    return result;
}

GLuint AssetManager::getTexture(std::string name, bool mipmapping)
{
    int width;
    int height;
    unsigned char *data;
    unsigned char *temp;
    int i;
    GLuint result = 0;

	if(name.length() > 0)
	{
		if(textures.find(name) != textures.end())
		{
			result = textures[name];
		}
		else
		{
			beginTimer();

			// much faster than using SOIL_load_OGL_texture(), plus this doesn't use any deprecated functionality
			data = SOIL_load_image(name.c_str(), &width, &height, 0, SOIL_LOAD_RGBA);

			// make sure the load was successful
			if(data)
			{
				// the pixel data is flipped vertically, so we need to flip it back with an in-place reversal
				temp = new unsigned char[width * 4];
				for(i = 0; i < height / 2; i ++)
				{
					memcpy(temp, &data[i * width * 4], (width * 4));								// copy row into temp array
					memcpy(&data[i * width * 4], &data[(height - i - 1) * width * 4], (width * 4));	// copy other side of array into this row
					memcpy(&data[(height - i - 1) * width * 4], temp, (width * 4));					// copy temp into other side of array
				}

				// we can generate a texture object since we had a successful load
				glGenTextures(1, &result);
				glBindTexture(GL_TEXTURE_2D, result);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

				// texture UVs should not wrap
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

				// if the user asked for it, generate mipmaps for this texture
				if(mipmapping)
				{
					glGenerateMipmap(GL_TEXTURE_2D);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
				}

				// release the memory used to perform the loading
				SOIL_free_image_data(data);
				delete[] temp;

				// store the resulting texture and stop the load timer
				endTimer(name);
				textures[name] = result;
			}
			else
			{
				cout << "AssetManager::getTexture could not load '" << name << "'" << endl;
				result = 0;
			}
		}
    }

    return result;
}

void AssetManager::beginTimer()
{
	SYSTEMTIME start;

	GetSystemTime(&start);
	loadingBeginTime = start.wSecond * 1000 + start.wMilliseconds;
}

void AssetManager::endTimer(string name)
{
	SYSTEMTIME end;

	GetSystemTime(&end);
	loadTimes[(end.wSecond * 1000 + end.wMilliseconds) - loadingBeginTime].push_back(name);
}

void AssetManager::printLoadTimes()
{
	map<int, vector<string> >::iterator i;
	vector<string>::iterator j;
	int total = 0;

	for(i = loadTimes.begin(); i != loadTimes.end(); i ++)
	{
		for(j = i -> second.begin(); j != i -> second.end(); j ++)
		{
			cout << i -> first << "ms:    " << *j << endl;
			total += i -> first;
		}
	}

	cout << "= " << total << "ms TOTAL" << endl;
}
