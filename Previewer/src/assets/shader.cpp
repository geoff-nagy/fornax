#include "assets/shader.h"

#include "glm/glm.hpp"
#include "gl/glew.h"

#include <fstream>
#include <cstdlib>
#include <iostream>
#include <string>
using namespace std;
using namespace glm;

Shader *Shader::activeShader = NULL;

Shader::Shader(string vertex, string frag)
{
    GLint vertexCompiled;
    GLint fragCompiled;

    vertexShader = glCreateShader(GL_VERTEX_SHADER);
    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	char *vertexFile = textFileRead(vertex.c_str());
	char *fragFile = textFileRead(frag.c_str());

	const char *vv = vertexFile;
	const char *ff = fragFile;

	glShaderSource(vertexShader, 1, &vv, NULL);
	glShaderSource(fragmentShader, 1, &ff, NULL);

    glCompileShader(vertexShader);
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &vertexCompiled);
    if(!vertexCompiled)
    {
		cout << "Shader::Shader() could not compile vertex shader " << vertex << endl;
		printShaderLogInfo(vertexShader);
		exit(1);
    }

    glCompileShader(fragmentShader);
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &fragCompiled);
    if(!fragCompiled)
    {
		cout << "Shader::Shader() could not compile fragment shader " << frag << endl;
		printShaderLogInfo(fragmentShader);
		exit(1);
    }

    if(vertexCompiled && fragCompiled)
    {
        program = glCreateProgram();
        glAttachShader(program, vertexShader);
        glAttachShader(program, fragmentShader);
    }
}

void Shader::bind()
{
	if(activeShader != this)
	{
		activeShader = this;
		glUseProgram(program);
	}
}

void Shader::link()
{
	GLint linked;

	glLinkProgram(program);
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if(!linked)
	{
		cerr << "Shader::link() could not link shader program" << endl;
		exit(1);
	}
}

void Shader::unbind()
{
	activeShader = NULL;
    glUseProgram(0);
}

void Shader::bindAttrib(const char *var, unsigned int index)
{
    glBindAttribLocation(program, index, var);
}

void Shader::uniform1f(const char *var, float val)
{
    glUniform1f(getUniLoc(program, var), val);
}

void Shader::uniform1i(const char *var, int val)
{
    glUniform1i(getUniLoc(program, var), val);
}

void Shader::uniform1fv(const char *var, int count, float *vals)
{
    glUniform1fv(getUniLoc(program, var), count, vals);
}

void Shader::uniform2f(const char *var, float v1, float v2)
{
    glUniform2f(getUniLoc(program, var), v1, v2);
}

void Shader::uniform2fv(const char *var, int count, float *vals)
{
    glUniform2fv(getUniLoc(program, var), count, vals);
}

void Shader::uniformVec2(const char *var, vec2 v)
{
    uniform2f(var, v.x, v.y);
}

void Shader::uniform3iv(const char *var, int count, int *vals)
{
    glUniform3iv(getUniLoc(program, var), count, vals);
}

void Shader::uniform3fv(const char *var, int count, float *vals)
{
    glUniform3fv(getUniLoc(program, var), count, vals);
}

void Shader::uniform3f(const char *var, const float v1, const float v2, const float v3)
{
    glUniform3f(getUniLoc(program, var), v1, v2, v3);
}

void Shader::uniformVec3(const char *var, vec3 v)
{
    uniform3f(var, v.x, v.y, v.z);
}

void Shader::uniformMatrix3fv(const char *var, GLsizei count, float *vals, bool transpose)
{
    glUniformMatrix3fv(getUniLoc(program, var), count, transpose, vals);
}

void Shader::uniform4iv(const char *var, int count, int *vals)
{
    glUniform4iv(getUniLoc(program, var), count, vals);
}

void Shader::uniform4fv(const char *var, int count, float *vals)
{
    glUniform4fv(getUniLoc(program, var), count, vals);
}

void Shader::uniform4f(const char *var, float v1, float v2, float v3, float v4)
{
    glUniform4f(getUniLoc(program, var), v1, v2, v3, v4);
}

void Shader::uniformVec4(const char *var, vec4 v)
{
    uniform4f(var, v.x, v.y, v.z, v.w);
}

void Shader::uniformMatrix4fv(const char *var, GLsizei count, GLfloat *vals, bool transpose)
{
    glUniformMatrix4fv(getUniLoc(program, var), count, transpose, vals);
}

GLint Shader::getUniLoc(GLuint program, const char *name)
{
    GLint loc;
    loc = glGetUniformLocation(program, name);

	if(loc == -1)
	{
		cout << "Shader::getUniLoc(): uniform '" << name << "' has not been defined" << endl;
	}

    return loc;
}

void Shader::printShaderLogInfo(GLuint obj)
{
    int infologLength = 0;
    int charsWritten  = 0;
    char *infoLog;

	glGetShaderiv(obj, GL_INFO_LOG_LENGTH,&infologLength);

	if (infologLength > 1)
	{
		infoLog = (char *)malloc(infologLength);
		glGetShaderInfoLog(obj, infologLength, &charsWritten, infoLog);
		cout << infoLog << endl;
		free(infoLog);
	}

}

char *Shader::textFileRead(const char *fn)
{
	FILE *fp;
	char *content = NULL;
	int count=0;

	if (fn != NULL)
	{
		fp = fopen(fn,"rt");
		if (fp != NULL)
		{
			fseek(fp, 0, SEEK_END);
			count = ftell(fp);
			rewind(fp);

			if (count > 0)
			{
				content = (char *)malloc(sizeof(char) * (count+1));
				count = fread(content,sizeof(char),count,fp);
				content[count] = '\0';
			}

			fclose(fp);
		}
		else
		{
			cout << "Shader::textFileRead() failed to open '" << fn << "'" << endl;
        }
	}

	return content;
}
