#include "core/previewer.h"

#include <cstdlib>
#include <ctime>
#include <iostream>
using namespace std;

int main(int args, char *argv[])
{
	const int NUM_PREVIEW_ARGS = 2;
	const int NUM_EXPORT_ARGS = 6;

	Previewer *previewer;			// our singleton previewer instance

	// re-seed our random number generator
	srand(time(NULL));

	// make sure we've received the emitter filename, and possibly the export options, too
	if(args == NUM_PREVIEW_ARGS || args == NUM_EXPORT_ARGS)
	{
		// set up our previewer instance
		previewer = Previewer::getInstance();

		// initialize the particle engine and load up the desired emitter to test; we are either
		// simply previewing an emitter, or exporting it to a PNG sequence
		if(args == NUM_PREVIEW_ARGS)
		{
			// pass in filename only
			previewer -> initPreviewMode(argv[1]);
		}
		else if(args == NUM_EXPORT_ARGS)
		{
			// pass in filename, frame rate, resolution, output dir, output prefix
			previewer -> initExportMode(argv[1], atoi(argv[2]), atoi(argv[3]), argv[4], argv[5]);
		}

		// fire our emitter until the user is done
		previewer -> run();
		delete previewer;
	}
	else
	{
		cout << "usage: particlePath [frameRate resolution outputDir outputPrefix]" << endl;
		cout << "\te.g., previewer emitter.emt" << endl;
		cout << "\te.g., previewer emitter.emt 30 256 explosion explodeFrame_" << endl;
		exit(1);
	}

	return 0;
}
