﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MultiThumbSlider
{
	// delegate for selection change event
	public delegate void MultiThumbSliderSelectionEventHandler(Object sender, MultiThumbSliderEventArgs e);
	
	// delegate for a thumb slide event
	public delegate void MultiThumbSliderSlideEventHandler(Object sender, MultiThumbSliderEventArgs e);
	
	public partial class MultiThumbSlider : UserControl
	{
		// provide an event that fires when the user selects a new thumb
		[Category("Action")]
		[Description("Occurs when a thumb is clicked.")]
		public event MultiThumbSliderSelectionEventHandler SelectionChanged;

		// provide an event that fires when the user slides a thumb
		[Category("Action")]
		[Description("Occurs when the user slides a thumb.")]
		public event MultiThumbSliderSlideEventHandler ThumbSlide;

		// default color of the thumbs
		public readonly Color DEFAULT_COLOR = Color.FromArgb(236, 236, 236);

		public const int MIN_VALUE = 0;					// minimum value that a slider can hold
		public const int MAX_VALUE = 100;				// maximum value that a slider can hold

		private const int MIN_SLIDER_POS = 4;			// furthest left-most position
		private const int MAX_SLIDER_POS = 264;			// furthest right-most position
		private const int SLIDER_POS_Y = 3;				// sliders are always this high in the control

		private Thumb activeThumb;						// the slider currently selected
		private int mouseClickX;						// initial x pos of the mouse when a slider is clicked
		private bool mouseDown;							// is the mouse down on a thumb?

		private List<Thumb> thumbs;						// thumbs currently on the slider control

		// public methods //

		public MultiThumbSlider()
		{
			InitializeComponent();
			Reset();
		}

		public int GetThumbCount()
		{
			return thumbs.Count;
		}

		public int GetThumbSlideLocation(int index)
		{
			return thumbs[index].SlideLocation;
		}

		public void SetThumbColor(int index, Color color)
		{
			thumbs[index].BackColor = color;
		}

		public Color GetThumbColor(int index)
		{
			return thumbs[index].BackColor;
		}

		public int GetSelectedIndex()
		{
			int result = -1;
			int i = 0;

			// make sure something is selected first
			if (activeThumb != null)
			{
				// count how many thumbs in the selected thumb is
				while (result == -1 && i < thumbs.Count)
				{
					if (thumbs[i] == activeThumb)
					{
						result = i;
					}
					i++;
				}
			}

			return result;
		}

		public void SetSelectedIndex(int index)
		{
			activeThumb = thumbs[index];
			HighlightActiveThumb();
		}

		public int IndexOf(Object obj)
		{
			int result = -1;
			int i = 0;

			// search until we run out of thumbs or find one with the matching object
			while (result == -1 && i < thumbs.Count)
			{
				if (thumbs[i].Data == obj)
				{
					result = i;
				}
				i++;
			}

			return result;
		}

		public Object GetObjectAtThumb(int index)
		{
			return thumbs[index].Data;
		}

		public void SetObjectAtThumb(int index, Object obj)
		{
			thumbs[index].Data = obj;
			thumbs[index].UpdateLabel();
		}

		public void AddThumb(int location, Object obj)
		{
			AddThumb(location, obj, DEFAULT_COLOR, true);
		}

		public void AddThumb(int location, Object obj, Color color, bool labelled)
		{
			// our new slider object
			Thumb thumb;
			int xPos;

			// clamp value of slider to a reasonable position
			if (location < MIN_VALUE) location = MIN_VALUE;
			if (location > MAX_VALUE) location = MAX_VALUE;

			// compute physical location of thumb
			xPos = MIN_SLIDER_POS + (int)(((double)location / 100.0) * (MAX_SLIDER_POS - MIN_SLIDER_POS));

			// create and position the new slider
			thumb = new Thumb(obj, labelled, location);
			thumb.Image = Properties.Resources.tick_unselected;
			thumb.Location = new Point(xPos, SLIDER_POS_Y);

			// register mouse events so the slider can be dragged
			thumb.MouseDown += control_MouseDown;
			thumb.MouseUp += slider_MouseUp;
			thumb.MouseMove += slider_MouseMove;

			// tweak appearance
			thumb.SizeMode = PictureBoxSizeMode.AutoSize;
			thumb.BackColor = color;
			thumb.UpdateLabel();

			// add the component
			Controls.Add(thumb);
			thumbs.Add(thumb);

			// make this thumb the selected one
			activeThumb = thumb;
			HighlightActiveThumb();

			// now sort the thumbs internally based on their positions
			thumbs.Sort();
		}

		public void RemoveThumb(int index)
		{
			Thumb toRemove;

			// grab a reference to the object to remove so we can delete it from the control
			toRemove = thumbs[index];

			// remove from the sorted list
			thumbs.RemoveAt(index);

			// remove from control
			toRemove.RemoveLabel();				// remove label first
			Controls.Remove(toRemove);
			toRemove.Dispose();

			// if this was the selected thumb, unselect it
			if (toRemove == activeThumb)
			{
				activeThumb = null;
			}

			// now sort the thumbs internally based on their positions
			thumbs.Sort();

			// select the thumb that replaces this one, if we can
			if (thumbs.Count > 0)
			{
				if (index > 0)
				{
					// activate the previous thumb
					activeThumb = thumbs[index - 1];
					HighlightActiveThumb();
				}
				else
				{
					// highlight the one that took its place instead, if there's nothing before this thumb
					activeThumb = thumbs[index];
					HighlightActiveThumb();
				}
			}
			else
			{
				activeThumb = null;
			}

			// raise a custom event saying that the selection index has changed
			OnSelectionChanged(new MultiThumbSliderEventArgs(GetSelectedIndex()));
		}

		public void ClearAllThumbs()
		{
			int i = GetThumbCount();

			while (i > 0)
			{
				RemoveThumb(0);
				i--;
			}
		}

		// custom event handlers

		protected virtual void OnSelectionChanged(MultiThumbSliderEventArgs e)
		{
			SelectionChanged(this, e);
		}

		protected virtual void OnThumbSlide(MultiThumbSliderEventArgs e)
		{
			ThumbSlide(this, e);
		}

		// private methods //

		private void Reset()
		{
			thumbs = new List<Thumb>();

			activeThumb = null;
			mouseDown = false;
		}

		private void HandleSliderDrag(int currentMouseX)
		{
			int newPosX;
			int thumbPosition;

			if (activeThumb != null)
			{
				// compute new location
				newPosX = activeThumb.Location.X + (currentMouseX - mouseClickX);

				// keep it in the trackbar bounds
				if (newPosX < MIN_SLIDER_POS)
				{
					newPosX = MIN_SLIDER_POS;
				}
				else if (newPosX > MAX_SLIDER_POS)
				{
					newPosX = MAX_SLIDER_POS;
				}

				// compute value
				thumbPosition = (int)((double)((double)(newPosX - MIN_SLIDER_POS) / (double)(MAX_SLIDER_POS - MIN_SLIDER_POS)) * 100);
				activeThumb.SlideLocation = thumbPosition;
				activeThumb.UpdateLabel();

				// set physical location
				activeThumb.Location = new Point(newPosX, activeThumb.Location.Y);

				// now sort the thumbs internally based on their positions
				thumbs.Sort();
			}
		}

		private void HighlightActiveThumb()
		{
			int i;

			// unhighlight all other thumbs
			for (i = 0; i < thumbs.Count; i++)
			{
				thumbs[i].Image = Properties.Resources.tick_unselected;
			}

			// and then highlight the active one
			if(activeThumb != null)
			{
				activeThumb.Image = Properties.Resources.tick_selected;
			}
		}

		// .NET events

		private void control_MouseDown(object sender, MouseEventArgs e)
		{
			// set the active thumb selection
			activeThumb = (Thumb)sender;
			HighlightActiveThumb();
			mouseClickX = e.X;
			mouseDown = true;

			// raise a custom event saying that the selection index has changed
			OnSelectionChanged(new MultiThumbSliderEventArgs(GetSelectedIndex()));
		}

		private void slider_MouseUp(object sender, MouseEventArgs e)
		{
			mouseDown = false;
		}

		private void slider_MouseMove(object sender, MouseEventArgs e)
		{
			// if the mouse is down and we have an active thumb, that means the user is dragging it
			if (mouseDown && activeThumb != null)
			{
				HandleSliderDrag(e.X);
				OnThumbSlide(new MultiThumbSliderEventArgs(GetSelectedIndex()));
			}
		}
	}
}
