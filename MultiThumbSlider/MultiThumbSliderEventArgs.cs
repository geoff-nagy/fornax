﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MultiThumbSlider
{
	public class MultiThumbSliderEventArgs : EventArgs
	{
		public int SelectedIndex { get; set; }

		public MultiThumbSliderEventArgs(int selectedIndex)
		{
			SelectedIndex = selectedIndex;
		}
	}
}
