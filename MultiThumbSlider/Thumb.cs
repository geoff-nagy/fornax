﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace MultiThumbSlider
{
	class Thumb : PictureBox, IComparable<Thumb>
	{
		public Object Data { get; set; }				// value being stored
		public int SlideLocation { get; set; }			// location on slider [0-100]
		public Label TextLabel { get; set; }			// handle to label describing data value

		public Thumb(Object data, bool labelled)
		{
			Init(data, labelled, 0);
		}

		public Thumb(Object data, bool labelled, int slideLocation)
		{
			Init(data, labelled, slideLocation);
		}

		private void Init(Object data, bool labelled, int slideLocation)
		{
			// assign normal properties
			Data = data;
			SlideLocation = slideLocation;
			TextLabel = null;

			// create a label for this thumb if requested
			if (labelled)
			{
				this.ParentChanged += new EventHandler(Thumb_ParentChanged);
			}
		}

		private void CreateLabel()
		{
			// create a text label that is supposed to appear below the thumb
			TextLabel = new Label();
			TextLabel.AutoSize = true;
			TextLabel.TextAlign = ContentAlignment.MiddleCenter;

			// add it to the parent object
			Parent.Controls.Add(TextLabel);
			UpdateLabel();
		}

		public int CompareTo(Thumb other)
		{
			// labels are sorted in non-descending order of location x-coordinate
			if (this.Location.X < other.Location.X) return -1;
			else if (this.Location.X > other.Location.X) return 1;
			else return 0;
		}

		public void UpdateLabel()
		{
			const int HEIGHT_OFFSET = 28;

			// update the location of the label if we have one
			if (TextLabel != null)
			{
				TextLabel.Text = Data.ToString();
				TextLabel.Location = new Point(Location.X - (TextLabel.Size.Width / 2) + (Size.Width / 2),
											   Location.Y + HEIGHT_OFFSET);
			}
		}

		public void RemoveLabel()
		{
			// remove and dispose of the label if we have one
			if (TextLabel != null)
			{
				Parent.Controls.Remove(TextLabel);
				TextLabel.Dispose();
			}
		}

		// .NET events

		public void Thumb_ParentChanged(object sender, EventArgs e)
		{
			if (Parent != null)
			{
				CreateLabel();
			}
		}
	}
}
