#include <stdio.h>
#include <math.h>

int main(int args, char *argv[])
{
	const int START_SIZE = 0;
	const int END_SIZE = 180;
	const int SIZE_RANGE = END_SIZE - START_SIZE;
	const int NUM_STEPS = 11;					// this will include the 0th and also the final step

	const float POWER = 0.45;

	float factor;
	float value;
	int i;

	// print stats
	printf("-- from %d to %d (power %0.2f) in %d steps:\n", START_SIZE, END_SIZE, POWER, NUM_STEPS);

	for(i = 0; i < NUM_STEPS; ++ i)
	{
		factor = (float)i / (NUM_STEPS - 1);
		value = START_SIZE + pow(factor, POWER) * SIZE_RANGE;
		printf("[%02d]: %d\n", i, (int)round(value));
	}

	return 0;
}
