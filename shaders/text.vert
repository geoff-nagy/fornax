#version 330

uniform mat4 u_ProjectionMatrix;
uniform mat4 u_ModelviewMatrix;
uniform vec4 u_Color;

layout(location = 0) in vec2 a_Vertex;
layout(location = 1) in vec2 a_TexCoord;

out vec4 v_Color;
out vec2 v_TexCoord;

void main()
{
    v_Color = u_Color;
    v_TexCoord = a_TexCoord;

    gl_Position = u_ProjectionMatrix * u_ModelviewMatrix * vec4(a_Vertex, 0.0, 1.0);
}
