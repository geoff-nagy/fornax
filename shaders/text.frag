#version 330

uniform sampler2D u_Texture;

in vec4 v_Color;
in vec2 v_TexCoord;

out vec4 f_Color;

void main()
{
	float luminance = texture(u_Texture, v_TexCoord).r;
    f_Color = v_Color * vec4(luminance);
}
