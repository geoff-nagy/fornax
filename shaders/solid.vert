#version 330

uniform mat4 u_MVPMatrix;

layout(location = 0) in vec3 a_Vertex;
layout(location = 1) in vec2 a_TexCoord;

out vec2 v_TexCoord;

void main()
{
    v_TexCoord = a_TexCoord;
    gl_Position = u_MVPMatrix * vec4(a_Vertex, 1.0);
}
