#version 330

uniform sampler2D u_Texture;

in vec4 v_FragColor;
in vec2 v_TexCoord;

out vec4 f_FragColor;

void main()
{
	f_FragColor = v_FragColor * texture(u_Texture, v_TexCoord).a;
}
