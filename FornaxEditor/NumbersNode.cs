﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FornaxEditor
{
	class NumbersNode : Node
	{
		private List<int> numbers;

		public NumbersNode(String name) : base(name)
		{
			numbers = new List<int>();
		}

		public void AddNumber(int number)
		{
			numbers.Add(number);
		}

		public int At(int index)
		{
			return numbers[index];
		}

		public int GetNumberCount()
		{
			return numbers.Count;
		}

		// this is mostly just a debugging function used to make sure that Dash is parsing things correctly
		public override String ToString(int indentLevel)
		{
			int i;
			String result = "";

			// indent to the appropriate level
			for(i = 0; i < indentLevel; i ++) result += "\t";

			// add the node name
			result += GetName() + " ";

			// now print out the numbers we have
			for(i = 0; i < numbers.Count; i ++)
			{
				result += numbers[i] + " ";
			}

			// output a new line for the next node
			result += Environment.NewLine;

			return result;
		}

	}
}
