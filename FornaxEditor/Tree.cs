﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace FornaxEditor
{
	class Tree
	{
		private static String fileContents;
		private static int locationInFile;
		private static SortedDictionary<String, String> constants;		// any user-defined constants
		private Node root;												// a dash tree root node can be any type of node

		public Tree(Node root)
		{
			this.root = root;
		}

		public static Tree CreateFromFile(String filename)
		{
			Tree result = null;
			Node root = null;

			// allocate space for the preface directive constants
			constants = new SortedDictionary<string, string>();

			// open the file and make sure it exists
			StreamReader sr = new StreamReader(filename);
			try
			{
				// begin reading from start of file
				fileContents = sr.ReadToEnd();
				locationInFile = 0;
			}
			catch (IOException err)
			{
				Console.WriteLine("Tree::createFromFile() could not open " + filename);
				Console.WriteLine(err);
			}
			finally
			{
				// we should always close the file no matter what
				sr.Close();
			}

			// see if there are any defined constants (or any other types of preface directives we might implement in the future)
			ParsePrefaceDirectives();

			// dash doesn't care what kind of node we have at the root
			root = ParseNode();

			// if the parse was successful, allocate the tree and set its root to what we just parsed
			if(root != null)
			{
				result = new Tree(root);
			}
			else
			{
				Console.WriteLine("Tree::createFromFile() failed to create a non-null root");
				Application.Exit();
			}

			return result;
		}

		private static int GetFileChar()
		{
			int ch = fileContents[locationInFile++];
			return ch;
		}

		private static void UngetFileChar()
		{
			locationInFile--;
		}

		public override String ToString()
		{
			return root.ToString(0);
		}

		public Node GetRoot()
		{
			return root;
		}

		private static bool IsWhitespace(int c)
		{
			return c <= 32 && c >= 9;
		}

		private static bool IsTokenEnd(int c)
		{
			return IsWhitespace(c) || c == '(' || c == ')' || c == ',';
		}

		private static bool IsQuote(int c)
		{
			return c == '\"';
		}

		private static bool IsNumeric(int c)
		{
			return c == '-' || (c >= '0' && c <= '9');
		}

		private static bool IsNumeric(String str, out double number)
		{
			return Double.TryParse(str, out number);
		}

		private static bool IsValidNodeStart(int c)
		{
			return c >= 'a' && c <= 'z';
		}

		private static bool IsConstant(int c)
		{
			return c >= 'A' && c <= 'Z';
		}

		private static void ConsumeWhitespace()
		{
			int c;

			// skip over the general repeating pattern of [whitespace][comment] until we run out
			do
			{
				// skip over any characters that are whitespace (tabs, spaces, new lines, etc.)
				do
				{
					c = GetFileChar();
				}
				while(IsWhitespace(c));

				// skip over any comments
				if(c == '#')
				{
					// skip over characters until we encounter a new line
					do
					{
						c = GetFileChar();
					}
					while(!(c == '\r' || c == '\n'));
				}
			}
			while(IsWhitespace(c));

			// we've consumed a visible char that's not a comment, so we need to spit it back out
			UngetFileChar();
		}

		private static void ConsumeUpToNodeValue()
		{
			int c;

			// assumes that the node name has been read by now; so, eat any whitespace
			ConsumeWhitespace();

			// the next char should be a semicolon
			c = GetFileChar();
			if(c != '(')
			{
				Console.WriteLine("Tree::consumeUpToNodeValue() expected '(' but got " + (char)c);
				Application.Exit();
			}

			// now read any remaining whitespace
			ConsumeWhitespace();
		}

		private static String ConsumeToken()
		{
			string result = "";
			int c;

			// get rid of any whitespace
			ConsumeWhitespace();

			// see what kind of data we're dealing with
			c = GetFileChar();
			UngetFileChar();

			if(IsConstant(c))
			{
				result = ConsumeConstantToken();
			}
			else if(IsQuote(c))
			{
				result = ConsumeStringToken();
			}
			else
			{
				result = ConsumeWordToken();
			}

			return result;
		}

		private static String ConsumeConstantToken()
		{
			// get the name of the constant
			String name = ConsumeWordToken();
			String result = null;

			// make sure the constant exists!
			if(constants.ContainsKey(name))
			{
				result = constants[name];
			}
			else
			{
				Console.WriteLine("Tree::consumeConstantToken cannot find a constant named " + name);
				Application.Exit();
			}

			return result;
		}

		private static String ConsumeStringToken()
		{
			int c;
			bool done = false;
			string result = "";

			// make sure we begin by reading a double quote
			c = GetFileChar();
			if(c != '\"')
			{
				Console.WriteLine("Tree::consumeStringToken() expected opening double-quote '\"', but got " + (char)c);
				Application.Exit();
			}

			// consume anything up to, but not including, the closing quote
			do
			{
				c = GetFileChar();
				done = c == '\"';

				if(!done)
				{
					result += (char)c;
				}
			}
			while(!done);

			return "\"" + result + "\"";
		}

		private static String ConsumeWordToken()
		{
			string result = "";
			int c;
			bool done = false;

			// build the node name char by char until we hit a '(' or some whitespace
			do
			{
				c = GetFileChar();
				done = IsTokenEnd(c);

				if(!done)
				{
					result += (char)c;
				}
			}
			while(!done);

			UngetFileChar();

			return result;
		}

		private static void ParsePrefaceDirectives()
		{
			String directive;
			bool done = false;
			int c;

			// clear any defined constants
			constants.Clear();

			// keep going while we have more directives to check out
			while(!done)
			{
				// first go over any whitespace or comments
				ConsumeWhitespace();

				// get the first char and see if it is a preface directive
				c = GetFileChar();
				if(c == '$')
				{
					// read the directive and decide what to do with it
					directive = ConsumeWordToken();
					if(directive.Equals("def"))
					{
						ParseConstant();
					}
					else
					{
						Console.WriteLine("Tree::parsePrefaceDirectives() does not know preface directive " + directive);
						Application.Exit();
					}
				}
				else
				{
					UngetFileChar();
					done = true;
				}
			}
		}

		private static void ParseConstant()
		{
			String name;
			String value;
			int c;

			// consume some more whitespace
			ConsumeWhitespace();

			// get the name of the constant and see if it already exists
			name = ConsumeWordToken();
			if(!constants.ContainsKey(name))
			{
				// get the first character of the constant value
				ConsumeWhitespace();
				c = GetFileChar();
				UngetFileChar();

				// ...decide what to based on the type of data we encounter
				if(IsQuote(c))
				{
					value = ConsumeStringToken();
					constants[name] = value;
				}
				else if(IsNumeric(c))
				{
					value = ConsumeWordToken();
					constants[name] = value;
				}
				else
				{
					Console.WriteLine("Tree::parseConstant() only allows constants to be string literals or numeric literals");
					Application.Exit();
				}
			}
			else
			{
				Console.WriteLine("Tree::parseConstant() found a duplicate constant: " + name);
				Application.Exit();
			}
		}

		private static Node ParseNode()
		{
			Node result = null;
			string constantValue;
			int pos;
			int c;

			// read the name of the node
			string nodeName = ConsumeWordToken();

			// skip up to the node value (i.e., past the opening semicolon and to the first
			// char of whatever the node value is)
			ConsumeUpToNodeValue();

			// read the first char...
			c = GetFileChar();
			UngetFileChar();

			// determine if this is a constant
			if(IsConstant(c))
			{
				// if it is, read the whole name (but keep track of where we started, because
				// we have to re-read it again later)
				pos = locationInFile;
				constantValue = ConsumeConstantToken();
				c = constantValue[0];

				// and back up, too, because we have to read it again
				locationInFile = pos;
			}

			// ...and decide what to do with the node value based on that first char
			if(IsNumeric(c))
			{
				result = ParseNumberList(nodeName);
			}
			else if(IsQuote(c))
			{
				result = ParseStringNode(nodeName);
			}
			else if(IsValidNodeStart(c))
			{
				result = ParseListNode(nodeName);
			}
			else
			{
				Console.WriteLine("Tree::parseNode() expected numeric literal, string literal, constant, or node, but got character " + (char)c);
				Application.Exit();
			}

			return result;
		}

		private static NumbersNode ParseNumberList(String nodeName)
		{
			NumbersNode result = new NumbersNode(nodeName);			// the new node we insert into the tree
			string numberStr;										// the number value interpreted as a string
			double numberValue;										// the number value converted to a double
			
			int c;
			bool done = false;

			// we don't know how many numbers we'll get, so start looping
			while(!done)
			{
				// skip any leading whitespace
				ConsumeWhitespace();

				// read the next number in the sequence
				numberStr = ConsumeToken();

				// make sure it's actually a number
				if(!IsNumeric(numberStr, out numberValue))
				{
					Console.WriteLine("Tree::parseNumberList() does not allow non-numeric data like " + numberStr);
					Application.Exit();
				}

				// by now, we know a conversion will be successful; add the number to the list in this node
				result.AddNumber((int)Math.Round(numberValue));

				// skip trailing whitespace
				ConsumeWhitespace();

				// the next char should be a comma, a plus or minus, or a closing parentheses
				c = GetFileChar();
				if(c == ')')
				{
					done = true;
				}
				else if(c != ',')
				{
					Console.WriteLine("Tree::parseNumberList() expected comma or terminating ')' but got " + (char)c);
					Application.Exit();
				}
			}

			return result;
		}

		private static StringNode ParseStringNode(String nodeName)
		{
			StringNode result = new StringNode(nodeName);
			string str;
			int c;

			// read over any whitespace
			ConsumeWhitespace();

			// read the string data
			str = ConsumeToken();

			// remove the quotes and put the value into the string node
			str = str.Substring(1, str.Length - 2);
			result.SetValue(str);

			// read any trailing whitespace
			ConsumeWhitespace();

			// ensure that there is a terminating ')' char
			c = GetFileChar();
			if(c != ')')
			{
				Console.WriteLine("Tree::parseStringNode() expected terminating ')' but got " + (char)c);
				Application.Exit();
			}

			return result;
		}

		private static ListNode ParseListNode(String nodeName)
		{
			ListNode result = new ListNode(nodeName);
			Node item;
			int c;
			bool done = false;

			while(!done)
			{
				// parse the node recursively and add it to the node list
				item = ParseNode();
				result.AddNode(item);

				// skip past any whitespace
				ConsumeWhitespace();

				// if we reach a semicolon, we're done
				c = GetFileChar();
				if(c == ')')
				{
					done = true;
				}
				else
				{
					// if this isn't a semicolon, it's something we need, so put it back
					UngetFileChar();
				}
			}

			return result;
		}

	}
}
