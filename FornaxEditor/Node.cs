﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FornaxEditor
{
	abstract class Node
	{
		private String name;

		public Node(String name)
		{
			this.name = name;
		}

		public String GetName()
		{
			return name;
		}

		public abstract String ToString(int indentLevel);
	}
}
