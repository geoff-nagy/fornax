﻿namespace FornaxEditor
{
	partial class ExportWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExportWindow));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label1 = new System.Windows.Forms.Label();
			this.numFrameRate = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtOutputDir = new System.Windows.Forms.TextBox();
			this.txtPrefix = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.numResolution = new System.Windows.Forms.NumericUpDown();
			this.label6 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.rdoPNG = new System.Windows.Forms.RadioButton();
			this.label7 = new System.Windows.Forms.Label();
			this.btnExport = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numFrameRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numResolution)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.numResolution);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.txtPrefix);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.txtOutputDir);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.numFrameRate);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(253, 129);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Playback Options";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 21);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(60, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Frame rate:";
			// 
			// numFrameRate
			// 
			this.numFrameRate.Location = new System.Drawing.Point(85, 19);
			this.numFrameRate.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
			this.numFrameRate.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numFrameRate.Name = "numFrameRate";
			this.numFrameRate.Size = new System.Drawing.Size(123, 20);
			this.numFrameRate.TabIndex = 1;
			this.numFrameRate.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(226, 21);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(21, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "fps";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 74);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(56, 13);
			this.label3.TabIndex = 3;
			this.label3.Text = "Output dir:";
			// 
			// txtOutputDir
			// 
			this.txtOutputDir.Location = new System.Drawing.Point(85, 71);
			this.txtOutputDir.Name = "txtOutputDir";
			this.txtOutputDir.Size = new System.Drawing.Size(162, 20);
			this.txtOutputDir.TabIndex = 4;
			// 
			// txtPrefix
			// 
			this.txtPrefix.Location = new System.Drawing.Point(85, 97);
			this.txtPrefix.Name = "txtPrefix";
			this.txtPrefix.Size = new System.Drawing.Size(162, 20);
			this.txtPrefix.TabIndex = 6;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 100);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(36, 13);
			this.label4.TabIndex = 5;
			this.label4.Text = "Prefix:";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(214, 47);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(33, 13);
			this.label5.TabIndex = 9;
			this.label5.Text = "pixels";
			// 
			// numResolution
			// 
			this.numResolution.Increment = new decimal(new int[] {
            32,
            0,
            0,
            0});
			this.numResolution.Location = new System.Drawing.Point(85, 45);
			this.numResolution.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
			this.numResolution.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
			this.numResolution.Name = "numResolution";
			this.numResolution.Size = new System.Drawing.Size(123, 20);
			this.numResolution.TabIndex = 8;
			this.numResolution.Value = new decimal(new int[] {
            256,
            0,
            0,
            0});
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(6, 47);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(60, 13);
			this.label6.TabIndex = 7;
			this.label6.Text = "Resolution:";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.label7);
			this.groupBox2.Controls.Add(this.rdoPNG);
			this.groupBox2.Location = new System.Drawing.Point(12, 147);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(253, 63);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "File Format";
			// 
			// rdoPNG
			// 
			this.rdoPNG.AutoSize = true;
			this.rdoPNG.Checked = true;
			this.rdoPNG.Enabled = false;
			this.rdoPNG.Location = new System.Drawing.Point(9, 19);
			this.rdoPNG.Name = "rdoPNG";
			this.rdoPNG.Size = new System.Drawing.Size(186, 17);
			this.rdoPNG.TabIndex = 0;
			this.rdoPNG.TabStop = true;
			this.rdoPNG.Text = "Portable Networks Graphic (*.png)";
			this.rdoPNG.UseVisualStyleBackColor = true;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
			this.label7.Location = new System.Drawing.Point(7, 39);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(183, 13);
			this.label7.TabIndex = 1;
			this.label7.Text = "Only PNGs are supported at this time.";
			// 
			// btnExport
			// 
			this.btnExport.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnExport.Location = new System.Drawing.Point(12, 216);
			this.btnExport.Name = "btnExport";
			this.btnExport.Size = new System.Drawing.Size(105, 34);
			this.btnExport.TabIndex = 2;
			this.btnExport.Text = "Export";
			this.btnExport.UseVisualStyleBackColor = true;
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(160, 216);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(105, 34);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			// 
			// ExportWindow
			// 
			this.AcceptButton = this.btnExport;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(277, 262);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnExport);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ExportWindow";
			this.ShowIcon = false;
			this.Text = "Export";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numFrameRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numResolution)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown numFrameRate;
		private System.Windows.Forms.TextBox txtOutputDir;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtPrefix;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.NumericUpDown numResolution;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.RadioButton rdoPNG;
		private System.Windows.Forms.Button btnExport;
		private System.Windows.Forms.Button btnCancel;
	}
}