﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FornaxEditor
{
	public partial class ExportWindow : Form
	{
		private string LongFileName = null;

		public ExportWindow(string LongFileName)
		{
			this.LongFileName = LongFileName;
			InitializeComponent();
		}

		private void StartExport()
		{
			int frameRate = (int)numFrameRate.Value;
			int resolution = (int)numResolution.Value;
			string outputDir = txtOutputDir.Text;
			string prefix = txtPrefix.Text;

			// start the previewer window in export mode
			PreviewStarter.Export(LongFileName, frameRate, resolution, outputDir, prefix);
		}

		private void ExportButton()
		{
			if(!String.IsNullOrWhiteSpace(txtOutputDir.Text) &&
			   !String.IsNullOrWhiteSpace(txtPrefix.Text))
			{
				StartExport();
			}
			else
			{
				MessageBox.Show("You must enter a valid, non-empty output directory and file prefix!",
								"Invalid Export Options",
								MessageBoxButtons.OK,
								MessageBoxIcon.Information);
				this.DialogResult = DialogResult.None;
			}
		}

		private void btnExport_Click(object sender, EventArgs e)
		{
			ExportButton();
		}
	}
}
