﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FornaxEditor
{
	class ListNode : Node
	{
		private List<Node> nodes;

		public ListNode(String name) : base(name)
		{
			nodes = new List<Node>();
		}

		public void AddNode(Node node)
		{
			nodes.Add(node);
		}

		public Node GetNode(int index)
		{
			return nodes[index];
		}

		public int GetNodeCount()
		{
			return nodes.Count;
		}

		public int GetNumber(String path, int index, int defaultValue)
		{
			int result = defaultValue;						// defaults to default return value
			String childNodeName;							// name of the next child node
			Node childNode;									// the actual child node
			ListNode childAsListNode;						// the actual child node as a list node
			String childPath;								// the remaining path after the child
			Node targetNode;								// the node we're targeting (the string node we want) at the end
			NumbersNode targetNodeAsNumbers;				// the node we're targeting as a string node
			int periodIndex;								// where the period '.' appears

			// if there's a period somewhere, we still have to traverse at least one more list node down
			if (path.Contains("."))
			{
				// get the name of the child node, if any
				periodIndex = path.IndexOf(".");
				childNodeName = path.Substring(0, periodIndex);

				// now get the child node and figure out what it is
				childNode = GetNodeByName(childNodeName);
				childAsListNode = childNode as ListNode;

				// if it's a list, then continue parsing the path string
				if(childAsListNode != null)
				{
					childPath = path.Substring(periodIndex + 1, path.Length - (periodIndex + 1));
					result = childAsListNode.GetNumber(childPath, index, defaultValue);
				}
				else
				{
					Console.WriteLine("ListNode::GetNumber() cannot find a node path to " + path);
				}
			}
			else
			{
				// if there's no period, we should be there!
				targetNode = GetNodeByName(path);
				targetNodeAsNumbers = targetNode as NumbersNode;
				if (targetNodeAsNumbers != null)
				{
					if (index < targetNodeAsNumbers.GetNumberCount())
					{
						result = targetNodeAsNumbers.At(index);
					}
					else
					{
						Console.WriteLine("ListNode::GetNumber() found a numbers node at " + path + " but index " + index + " is out of range");
					}
				}
				else
				{
					Console.WriteLine("ListNode::GetNumber() found a node called " + path + " but it is not a numbers node");
				}
			}

			return result;
		}

		public String GetString(String path, String defaultValue)
		{
			String result = defaultValue;					// defaults to default return value
			String childNodeName;							// name of the next child node
			Node childNode;									// the actual child node
			ListNode childAsListNode;						// the actual child node as a list node
			String childPath;								// the remaining path after the child
			Node targetNode;								// the node we're targeting (the string node we want) at the end
			StringNode targetNodeAsString;					// the node we're targeting as a string node
			int periodIndex;								// where the period '.' appears

			// if there's a period somewhere, we still have to traverse at least one more list node down
			if (path.Contains("."))
			{
				// get the name of the child node, if any
				periodIndex = path.IndexOf(".");
				childNodeName = path.Substring(0, periodIndex);

				// now get the child node and figure out what it is
				childNode = GetNodeByName(childNodeName);
				childAsListNode = childNode as ListNode;

				// if it's a list, then continue parsing the path string
				if (childAsListNode != null)
				{
					childPath = path.Substring(periodIndex + 1, path.Length - (periodIndex + 1));
					result = childAsListNode.GetString(childPath, defaultValue);
				}
				else
				{
					Console.WriteLine("ListNode::GetString() cannot find a node path to " + path);
				}
			}
			else
			{
				// if there's no period, we should be there!
				targetNode = GetNodeByName(path);
				targetNodeAsString = targetNode as StringNode;
				if (targetNodeAsString != null)
				{
					result = targetNodeAsString.GetValue();
				}
				else
				{
					Console.WriteLine("ListNode::GetString() found a node called " + path + " but it is not a string node");
				}
			}

			return result;
		}

		public Node GetNodeByName(String name)
		{
			int i = 0;
			Node curr;
			Node result = null;

			while(result == null && i < nodes.Count)
			{
				// any node matching the given name is good enough
				curr = nodes[i];
				if(curr.GetName().Equals(name))
				{
					result = curr;
				}
				i++;
			}

			return result;
		}

		public ListNode GetListNodeByName(String name, int nth)
		{
			int i = 0;					// search index among all nodes
			int count = 0;				// which list node index we're currently reading

			Node curr;					// node we're currently reading
			ListNode temp;				// attempt to transform current node into a list node
			ListNode result = null;

			while (result == null && i < nodes.Count)
			{
				curr = nodes[i];

				// if we found a name match, so far so good
				if (curr.GetName().Equals(name))
				{
					// we only consider matches that are actually list nodes, since we want a list node anyways
					temp = curr as ListNode;
					if (temp != null)
					{
						// if we've reached the right count, we're done!
						if (count == nth)
						{
							result = temp;
						}
						count++;
					}
				}
				i++;
			}

			return result;
		}

		public NumbersNode GetNumbersNodeByName(String name, int nth)
		{
			int i = 0;					// search index among all nodes
			int count = 0;				// which list node index we're currently reading

			Node curr;					// node we're currently reading
			NumbersNode temp;			// attempt to transform current node into a numbers node
			NumbersNode result = null;

			while (result == null && i < nodes.Count)
			{
				curr = nodes[i];

				// if we found a name match, so far so good
				if (curr.GetName().Equals(name))
				{
					// we only consider matches that are actually number nodes, since we want a number node anyways
					temp = curr as NumbersNode;
					if (temp != null)
					{
						// if we've reached the right count, we're done!
						if (count == nth)
						{
							result = temp;
						}
						count++;
					}
				}
				i++;
			}

			return result;
		}

		public StringNode GetStringNodeByName(String name, int nth)
		{
			int i = 0;					// search index among all nodes
			int count = 0;				// which list node index we're currently reading

			Node curr;					// node we're currently reading
			StringNode temp;			// attempt to transform current node into a numbers node
			StringNode result = null;

			while (result == null && i < nodes.Count)
			{
				curr = nodes[i];

				// if we found a name match, so far so good
				if (curr.GetName().Equals(name))
				{
					// we only consider matches that are actually string nodes, since we want a string node anyways
					temp = curr as StringNode;
					if (temp != null)
					{
						// if we've reached the right count, we're done!
						if (count == nth)
						{
							result = temp;
						}
						count++;
					}
				}
				i++;
			}

			return result;
		}

		public override String ToString(int indentLevel)
		{
			String result = "";
			int i;

			// indent to the appropriate level
			for(i = 0; i < indentLevel; i ++) result += "\t";

			// add the node name
			result += GetName() + Environment.NewLine;

			// now print out the nodes we own
			for(i = 0; i < nodes.Count; i ++)
			{
				result += nodes[i].ToString(indentLevel + 1);
			}

			return result;
		}
	}
}
