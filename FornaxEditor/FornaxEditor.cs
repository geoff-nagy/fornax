﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace FornaxEditor
{
	public partial class FornaxEditor : Form
	{
		/*
		Remaining tasks:
		X preview integration
		X fix save as on new emitter bug
		- OPTIONAL: if possible, make it possible to edit particle textures that are open in this editor
		X finish child emitter/properties redesign
		X add particle bug: it duplicates current particle properties instead of refreshing them
		X add PNG export window
		X prevent previewing/exporting of empty particle emitters
		X keyframe value scaling
		*/

		// versioning info for my own interest
		private const String VERSION_MAJOR = "0";
		private const String VERSION_MINOR = "8";
		private const String VERSION_STRING = VERSION_MAJOR + "." + VERSION_MINOR;
		private const String YEAR = "2015";

		// default untitled name of the file
		private const String DEFAULT_SHORT_FILENAME = "untitled-emitter.emt";

		// file extension settings
		private const String EMITTER_FILE_EXTENSION = "emt";
		private const String PARTICLE_FILE_EXTENSION = "pds";

		private String DirectoryName;								// absolute path to folder containing current emitter file
		private String ShortFileName;								// name without the path
		private String LongFileName;								// name with the path; initially null until saved
		private bool Changed;										// has the user made changes since the last save?

		private int GUILocks;										// how many programmatic changes to GUI elements are underway

		private Emitter CurrentEmitter;								// emitter open in the editor
		private ParticleGroup CurrentParticleGroup;					// particle emission settings open in the editor
		private ParticleDescriptor CurrentParticleDescriptor;		// particle open in editor

		public FornaxEditor()
		{
			InitializeComponent();
			Reset();
		}

		private void Reset()
		{
			// get ready to build our emitter objects
			CurrentEmitter = new Emitter();
			CurrentParticleGroup = null;
			CurrentParticleDescriptor = null;

			// remove any particle groups
			lstParticlesToEmit.Items.Clear();
			
			// prepare to receive .NET control content change events
			GUILocks = 0;

			// set file properties
			ShortFileName = DEFAULT_SHORT_FILENAME;
			LongFileName = null;
			DirectoryName = null;
			Changed = false;

			// enable buttons based on unsaved status
			EnableParticleGroupGUI();
			EnableParticlePropertiesGUI();
			UpdateGUIFromParticleGroup();
			UpdateFormTitle();
			EnableKeyframeButtons();
		}

		// basic buttons

		private void NewButton()
		{
			Process.Start(Application.ExecutablePath);
		}

		private void OpenButton()
		{
			OpenFileDialog open = new OpenFileDialog();
			int i;

			open.Title = "Open Text File";
			open.Filter = "Emitter Files Files (*.emt)|*.emt|All files (*.*)|*.*";

			// make sure we've saved the current file first
			if (Changed && SaveBeforeOpening())
			{
				SaveAllButton();
			}

			if (open.ShowDialog() == DialogResult.OK)
			{
				// clear the existing layout and reset everything
				Reset();

				// load up the emitter from the dash file
				CurrentEmitter = FileReader.ReadEmitter(open.FileName);

				// update our file name and directory tracking
				LongFileName = open.FileName;
				ShortFileName = Util.GetShortFilename(LongFileName);
				DirectoryName = Util.GetDirectoryName(LongFileName);

				// update the GUI to show that our new file is open
				UpdateFormTitle();
				EnableParticleGroupGUI();
				UpdateGUIFromParticleGroup();
				UpdateGUIFromEmitter();
				
				// add the particles to the emission list
				for (i = 0; i < CurrentEmitter.GetParticleGroupCount(); i++)
				{
					lstParticlesToEmit.Items.Add(CurrentEmitter.GetParticleGroup(i).Name);
				}

				// activate the first particle group, if there is one
				if (CurrentEmitter.GetParticleGroupCount() > 0)
				{
					CurrentParticleGroup = CurrentEmitter.GetParticleGroup(0);
					lstParticlesToEmit.SelectedIndex = 0;
				}
				
				// now enable any particle buttons
				EnableKeyframeButtons();

				// update changed status
				SetFileAsChanged(false);
			}
		}

		private void SaveAllButton()
		{
			if (!String.IsNullOrWhiteSpace(LongFileName))
			{
				WriterEmitterToFile(CurrentEmitter);
				SaveAllParticles();
			}
			else
			{
				SaveAsButton();
			}
		}

		private void SaveAsButton()
		{
			SaveFileDialog save = new SaveFileDialog();

			save.Title = "Save Text File";
			save.Filter = "Emitter Files (*.emt)|*.emt|All files (*.*)|*.*";
			save.AddExtension = false;
			save.FileName = ShortFileName;

			if (save.ShowDialog() == DialogResult.OK)
			{
				try
				{
					LongFileName = save.FileName;
					if (!LongFileName.Contains("."))
					{
						LongFileName += ".emt";
					}

					// update the short filename and where the emitter file resides
					ShortFileName = Util.GetShortFilename(LongFileName);
					DirectoryName = Util.GetDirectoryName(LongFileName);
					CurrentEmitter.AbsoluteFileName = LongFileName;

					// if we've saved, then we can start adding particle references
					EnableParticleGroupGUI();
					EnableParticlePropertiesGUI();
					
					// if we don't have a particle group active, then we can't expose those controls
					if (CurrentParticleGroup != null)
					{
						UpdateGUIFromParticleGroup();
						UpdateGUIFromParticleDescriptor();

						// now save everything
						WriterEmitterToFile(CurrentEmitter);
						SaveAllParticles();
					}

					// update changed status
					SetFileAsChanged(false);
				}
				catch (Exception ex)
				{
					MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
				}
			}
		}

		private void AboutButton()
		{
			MessageBox.Show("Fornax Particle Editor Version " + VERSION_STRING +
							"\n\nCopyright ©  " + YEAR + " Geoff Nagy." +
							"\nAll rights reserved." +
							"\n\nA minimalist emitter and particle editor.",
							"About Fornax Particle Editor",
							MessageBoxButtons.OK,
							MessageBoxIcon.Information);
		}

		// utility dialogs

		private bool SaveBeforeOpening()
		{
			DialogResult result = MessageBox.Show("Would you like to save '" + ShortFileName + "' before opening another file?",
												  "Save First?",
												  MessageBoxButtons.YesNo,
												  MessageBoxIcon.Question);
			return result == DialogResult.Yes;
		}

		private DialogResult SaveBeforeClosing()
		{
			DialogResult result = MessageBox.Show("Would you like to save '" + ShortFileName + "' before closing?",
												  "Save First?",
												  MessageBoxButtons.YesNoCancel,
												  MessageBoxIcon.Question);
			return result;
		}

		private DialogResult CloseFile()
		{
			DialogResult result = DialogResult.No;

			if (Changed)
			{
				result = SaveBeforeClosing();
				if (result == DialogResult.Yes)
				{
					SaveAllButton();
				}
			}

			return result;
		}

		// file handling

		private void SetFileAsChanged(bool changed = true)
		{
			Changed = changed;
			UpdateFormTitle();
			btnSaveAll.Enabled = Changed;
		}

		private void WriterEmitterToFile(Emitter emitter)
		{
			if (LongFileName != null)
			{
				try
				{
					// write the emitter to disk
					using (StreamWriter sw = new StreamWriter(LongFileName))
					{
						sw.Write(emitter.ToString());
						sw.Close();
					}

					// update changed status
					SetFileAsChanged(false);
				}
				catch (Exception ex)
				{
					MessageBox.Show("Error: SaveButton() could not write file to disk. Original error: " + ex.Message);
				}
			}
			else
			{
				SaveAsButton();
			}
		}

		private void WriteParticleDescriptorToFile(ParticleDescriptor particle)
		{
			if (particle != null)
			{
				try
				{
					// write the emitter to disk
					using (StreamWriter sw = new StreamWriter(particle.GetFilename()))
					{
						sw.Write(particle.ToString());
						sw.Close();
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show("Error: WriteParticleDescriptorToFile() could not write file to disk. Original error: " + ex.Message);
				}
			}
		}

		private void SaveAllParticles()
		{
			int i;

			// make sure we have a valid emitter object
			if (CurrentEmitter != null)
			{
				// update the current particle descriptor
				UpdateParticleDescriptorFromGUI();

				// write each and every reference particle to file
				for (i = 0; i < CurrentEmitter.GetParticleGroupCount(); i++)
				{
					WriteParticleDescriptorToFile(CurrentEmitter.GetParticleGroup(i).Particle);
				}
			}
		}

		private void ShowNoEmitterError()
		{
			MessageBox.Show("You must have a saved, active emitter with at least one particle reference before you can preview or export.",
							"No Emitter Ready",
							MessageBoxButtons.OK,
							MessageBoxIcon.Information);
		}

		private bool CanPreviewOrExport()
		{
			return !String.IsNullOrWhiteSpace(LongFileName) &&
					CurrentEmitter != null &&
					!CurrentEmitter.IsEmpty();
		}

		private void PreviewButton()
		{
			if(CanPreviewOrExport())
			{
				// start the previewer window in preview mode
				SaveAllButton();
				PreviewStarter.Preview(LongFileName);
			}
			else
			{
				ShowNoEmitterError();
			}
		}

		private void ExportButton()
		{
			ExportWindow exportWindow;

			if(CanPreviewOrExport())
			{
				SaveAllButton();
				exportWindow = new ExportWindow(LongFileName);
				exportWindow.StartPosition = FormStartPosition.CenterParent;
				exportWindow.ShowDialog();
			}
			else
			{
				ShowNoEmitterError();
			}
		}

		private void ScaleKeyframeValuesButton()
		{
			// show the scaling dialog window
			ScaleKeyframeValuesForm scaleForm = new ScaleKeyframeValuesForm();
			scaleForm.StartPosition = FormStartPosition.CenterParent;
			scaleForm.ShowDialog();

			// if the user didn't cancel, we need to perform the scale they asked for
			if(scaleForm.DialogResult == DialogResult.OK)
			{
				CurrentParticleDescriptor.ScaleAlpha(scaleForm.GetAlphaScale());
				CurrentParticleDescriptor.ScaleVelocity(scaleForm.GetVelocityScale());
				CurrentParticleDescriptor.ScaleSize(scaleForm.GetSizeScale());
				CurrentParticleDescriptor.ScaleSpin(scaleForm.GetSpinScale());
				UpdateGUIFromParticleDescriptor();
				SetFileAsChanged();
			}
		}

		// keyframe button click handling

		private void SelectColor()
		{
			ColorDialog dialog = new ColorDialog();
			dialog.AllowFullOpen = true;
			dialog.FullOpen = true;
			dialog.Color = btnColor.BackColor;

			if (dialog.ShowDialog() == DialogResult.OK)
			{
				if (mtsColor.GetSelectedIndex() >= 0)
				{
					// save color in keyframe
					btnColor.BackColor = dialog.Color;
					mtsColor.SetObjectAtThumb(mtsColor.GetSelectedIndex(), dialog.Color);
					mtsColor.SetThumbColor(mtsColor.GetSelectedIndex(), dialog.Color);
				}
			}
		}

		private void SelectAlpha()
		{
			ColorDialog dialog = new ColorDialog();
			Color color;
			int maxChannel;
			
			dialog.AllowFullOpen = true;
			dialog.FullOpen = true;
			dialog.Color = btnAlpha.BackColor;

			if (dialog.ShowDialog() == DialogResult.OK)
			{
				// desaturate the selected color so we can use just one channel for alpha
				maxChannel = ColorToAlpha(dialog.Color);
				color = Color.FromArgb(maxChannel, maxChannel, maxChannel);

				// save alpha in keyframe
				btnAlpha.BackColor = color;
				mtsAlpha.SetObjectAtThumb(mtsAlpha.GetSelectedIndex(), maxChannel);
				mtsAlpha.SetThumbColor(mtsAlpha.GetSelectedIndex(), color);
			}
		}

		private int ColorToAlpha(Color color)
		{
			int maxChannel = Math.Max(color.R, Math.Max(color.G, color.B));
			return maxChannel;
		}

		// particle creation/deletion

		private void AddParticleButton()
		{
			InputDialog input = new InputDialog("Add Particle", "Enter the name of the particle:", "");
			String name;
			String absoluteDirectory;
			String absoluteName;
			DialogResult dialog;
			ParticleDescriptor newParticleDescriptor = null;

			// if the user hit 'ok', grab their input and prepare to build a new particle group
			if (input.ShowDialog(this) == DialogResult.OK)
			{
				// make sure the user typed in something sensible
				name = input.GetInputText();
				if (!String.IsNullOrWhiteSpace(name))
				{
					// compute the directory where the particle should go
					absoluteDirectory = DirectoryName + "\\pds";

					// compute what the absolute path of the particle would be
					absoluteName = absoluteDirectory + "\\" + name + "." + PARTICLE_FILE_EXTENSION;

					// check if the particle already exists
					if (File.Exists(absoluteName))
					{
						dialog = MessageBox.Show("The particle descriptor '" + absoluteName + "' already exists on file. Would you still like to reference it?",
												 "Particle Already Exists",
												 MessageBoxButtons.YesNo,
												 MessageBoxIcon.Question);
						if (dialog == DialogResult.Yes)
						{
							newParticleDescriptor = ParticleDescriptor.FromFile(absoluteName);
						}
					}
					else
					{
						// create the directory if it doesn't exist yet
						if (!Directory.Exists(absoluteDirectory))
						{
							Directory.CreateDirectory(absoluteDirectory);
						}

						// now create the brand new particle descriptor
						newParticleDescriptor = ParticleDescriptor.CreateDefault(absoluteName);
					}

					// if the user wants to add the particle to the emitter, do so
					if (newParticleDescriptor != null)
					{
						// add the particle to the emitter
						CurrentEmitter.AddParticleGroup(new ParticleGroup(name, newParticleDescriptor));

						// and now select the new particle (by its short name) so we can edit it
						lstParticlesToEmit.Items.Add(name);
						lstParticlesToEmit.SelectedIndex = lstParticlesToEmit.Items.Count - 1;
						// this selection will trigger the selection changed event and update the keyframe GUI elements

						// refresh our keyframe control buttons for the new particle
						EnableKeyframeButtons();
						UpdateGUIKeyframeValues();
					}
				}
			}

			input.Dispose();
		}

		private void DuplicateParticleButton()
		{
			InputDialog input = new InputDialog("Duplicate Particle", "Enter the file name of the duplicate particle:", "");
			String name;
			String absoluteDirectory;
			String absoluteName;
			DialogResult dialog;
			ParticleGroup duplicate;
			bool addParticle = false;

			// if the user hit 'ok', grab their input and prepare to build a new particle group
			if (input.ShowDialog(this) == DialogResult.OK)
			{
				// make sure the user typed in something sensible
				name = input.GetInputText();
				if (!String.IsNullOrWhiteSpace(name))
				{
					// compute the directory where the particle should go
					absoluteDirectory = DirectoryName + "\\pds";

					// compute what the absolute path of the particle would be
					absoluteName = absoluteDirectory + "\\" + name + "." + PARTICLE_FILE_EXTENSION;

					// check if the particle already exists
					if (File.Exists(absoluteName))
					{
						dialog = MessageBox.Show("The particle descriptor '" + absoluteName + "' already exists on file. Would you like to replace it with this new particle?",
												 "Particle Already Exists",
												 MessageBoxButtons.YesNo,
												 MessageBoxIcon.Question);
						if (dialog == DialogResult.Yes)
						{
							addParticle = true;
						}
					}
					else
					{
						addParticle = true;
					}

					// if the user wants to add the particle to the emitter, do so
					if (addParticle)
					{
						duplicate = CurrentParticleGroup.Clone(name);
						duplicate.Particle = CurrentParticleDescriptor.Clone(absoluteName);

						// add the particle to the emitter
						CurrentEmitter.AddParticleGroup(duplicate);

						// and now select the new particle (by its short name) so we can edit it
						lstParticlesToEmit.Items.Add(duplicate.Name);
						lstParticlesToEmit.SelectedIndex = lstParticlesToEmit.Items.Count - 1;
						// this selection will trigger the selection changed event and update the keyframe GUI elements

						// refresh our keyframe control buttons for the new particle
						EnableKeyframeButtons();
						UpdateGUIKeyframeValues();
					}
				}
			}

			input.Dispose();
		}

		private void RemoveSelectedParticleButton()
		{
			int selected;
			String name;
			DialogResult result;

			if (CurrentParticleGroup != null)
			{
				selected = lstParticlesToEmit.SelectedIndex;
				name = lstParticlesToEmit.Items[selected].ToString();

				// make sure our selection index is valid (it should be since we have a non-null particle)
				if (selected >= 0)
				{
					// make sure the user really meant it (not that much data is lost...)
					result = MessageBox.Show("You can always temporarily disable a particle. Are you sure you want to delete the reference to particle '" + name + "'?",
											 "Confirm Delete",
											 MessageBoxButtons.YesNo,
											 MessageBoxIcon.Warning);
					if (result == DialogResult.Yes)
					{
						// remove the item
						lstParticlesToEmit.Items.RemoveAt(selected);

						// remove the item from the current emitter
						CurrentEmitter.RemoveParticle(selected);

						// select the item that replaces it, or the one before if we've removed the last one
						if (selected < lstParticlesToEmit.Items.Count)
						{
							lstParticlesToEmit.SelectedIndex = selected;
						}
						else
						{
							selected--;
							lstParticlesToEmit.SelectedIndex = selected;
						}

						// indicate that changes need to be saved
						SetFileAsChanged();
					}
				}
			}
		}

		// child emitter handling

		private void AddChildEmitter()
		{
			InputDialog input = new InputDialog("Add Child Emitter", "Enter the name of the emitter:", "");
			String name;

			// if the user hit 'ok', grab their input and prepare to build a new particle group
			if (input.ShowDialog(this) == DialogResult.OK)
			{
				// make sure the user typed in something sensible
				name = input.GetInputText();
				if (!String.IsNullOrWhiteSpace(name))
				{
					// add the name of the emitter to the GUI list
					lstChildEmitters.Items.Add(name);

					// add the name of the emitter to the particle emitter list
					CurrentParticleGroup.ChildEmitters.Add(name);

					// mark the file as changed
					SetFileAsChanged();
				}
			}

			input.Dispose();
		}

		private void RemoveChildEmitter()
		{
			int selected;
			String name;
			DialogResult result;

			if (CurrentParticleGroup != null)
			{
				selected = lstChildEmitters.SelectedIndex;
				name = lstChildEmitters.Items[selected].ToString();

				// make sure our selection index is valid (it should be since we have a non-null particle)
				if (selected >= 0)
				{
					// make sure the user really meant it (not that much data is lost...)
					result = MessageBox.Show("Are you sure you want to delete the reference to emitter '" + name + "'?",
											 "Confirm Delete",
											 MessageBoxButtons.YesNo,
											 MessageBoxIcon.Warning);
					if (result == DialogResult.Yes)
					{
						// remove the item
						lstChildEmitters.Items.RemoveAt(selected);

						// remove the item from the current emitter
						CurrentParticleGroup.ChildEmitters.RemoveAt(selected);

						// select the item that replaces it, or the one before if we've removed the last one
						if (selected < lstChildEmitters.Items.Count)
						{
							lstChildEmitters.SelectedIndex = selected;
						}
						else
						{
							selected--;
							lstChildEmitters.SelectedIndex = selected;
						}

						// indicate that changes need to be saved
						SetFileAsChanged();
					}
				}
			}
		}

		// keyframe handling

		private void EnableKeyframeButtons()
		{
			EnableColorButtons();
			EnableAlphaButtons();
			EnableVelocityButtons();
			EnableSizeButtons();
			EnableSpinButtons();

			numVelocityVariance.Enabled = lstParticlesToEmit.SelectedIndex >= 0;
			numSizeVariance.Enabled = lstParticlesToEmit.SelectedIndex >= 0;
			numSpinVariance.Enabled = lstParticlesToEmit.SelectedIndex >= 0;
		}

		private void EnableColorButtons()
		{
			btnRemoveColor.Enabled = mtsColor.GetThumbCount() > 1;
			btnColor.Enabled = mtsColor.GetSelectedIndex() >= 0;
		}

		private void EnableAlphaButtons()
		{
			btnRemoveAlpha.Enabled = mtsAlpha.GetThumbCount() > 1;
			btnAlpha.Enabled = mtsAlpha.GetSelectedIndex() >= 0;
		}

		private void EnableVelocityButtons()
		{
			btnRemoveVelocity.Enabled = mtsVelocity.GetThumbCount() > 1;
			numVelocityValue.Enabled = mtsVelocity.GetSelectedIndex() >= 0;
		}

		private void EnableSizeButtons()
		{
			btnRemoveSize.Enabled = mtsSize.GetThumbCount() > 1;
			numSizeValue.Enabled = mtsSize.GetSelectedIndex() >= 0;
		}

		private void EnableSpinButtons()
		{
			btnRemoveSpin.Enabled = mtsSpin.GetThumbCount() > 1;
			numSpinValue.Enabled = mtsSpin.GetSelectedIndex() >= 0;
		}

		// updating of active keyframe values

		private void UpdateGUIKeyframeValues()
		{
			UpdateGUIColorValue();
			UpdateGUIAlphaValue();
			UpdateGUIVelocityValue();
			UpdateGUISizeValue();
			UpdateGUISpinValue();
		}

		private void UpdateGUIColorValue()
		{
			Color currentColor;

			if (mtsColor.GetSelectedIndex() >= 0)
			{
				currentColor = (Color)mtsColor.GetObjectAtThumb(mtsColor.GetSelectedIndex());
				btnColor.BackColor = currentColor;
			}
		}

		private void UpdateGUIAlphaValue()
		{
			int currentAlpha;

			if (mtsAlpha.GetSelectedIndex() >= 0)
			{
				currentAlpha = Convert.ToInt32(mtsAlpha.GetObjectAtThumb(mtsAlpha.GetSelectedIndex()));
				btnAlpha.BackColor = Color.FromArgb(currentAlpha, currentAlpha, currentAlpha);
			}
		}

		private void UpdateGUIVelocityValue()
		{
			int currentVelocity;

			if (mtsVelocity.GetSelectedIndex() >= 0)
			{
				GUILocks ++;
				currentVelocity = Convert.ToInt32(mtsVelocity.GetObjectAtThumb(mtsVelocity.GetSelectedIndex()));
				numVelocityValue.Value = currentVelocity;
				GUILocks --;
			}
		}

		private void UpdateGUISizeValue()
		{
			int currentSize;

			if (mtsSize.GetSelectedIndex() >= 0)
			{
				GUILocks ++;
				currentSize = Convert.ToInt32(mtsSize.GetObjectAtThumb(mtsSize.GetSelectedIndex()));
				numSizeValue.Value = currentSize;
				GUILocks --;
			}
		}

		private void UpdateGUISpinValue()
		{
			int currentSpin;

			if (mtsSpin.GetSelectedIndex() >= 0)
			{
				GUILocks ++;
				currentSpin = Convert.ToInt32(mtsSpin.GetObjectAtThumb(mtsSpin.GetSelectedIndex()));
				numSpinValue.Value = currentSpin;
				GUILocks --;
			}
		}

		// update data structures based on GUI input

		private void UpdateEmitterFromGUI()
		{
			CurrentEmitter.Life = (int)numEmitterLife.Value;
			CurrentEmitter.Radius = (int)numEmitterRadius.Value;
		}
		
		private void UpdateParticleGroupFromGUI()
		{
			int i;

			if (CurrentParticleGroup != null && GUILocks == 0)
			{
				// basic particle properties
				CurrentParticleGroup.EmissionCount = (int)numParticleCount.Value;
				CurrentParticleGroup.InitialDelay = (int)numInitialDelay.Value;
				CurrentParticleGroup.ReEmit = chkReEmit.Checked;
				CurrentParticleGroup.ReEmitInterval = (int)numReEmitInterval.Value;
				CurrentParticleGroup.Enabled = chkParticleEnabled.Checked;
				CurrentParticleGroup.Solo = chkParticleSolo.Checked;
				CurrentParticleGroup.SpawnCenter = chkSpawnCenter.Checked;
				CurrentParticleGroup.AdditiveBlending = chkAdditiveBlending.Checked;

				// life inheritance properties
				CurrentParticleGroup.EmitterAffectsSize = chkAffectsSize.Checked;
				CurrentParticleGroup.EmitterAffectsVelocity = chkAffectsVelocity.Checked;
				CurrentParticleGroup.EmitterAffectsLifespan = chkAffectsLifespan.Checked;

				// child emitter list
				CurrentParticleGroup.ChildEmitters.Clear();
				for (i = 0; i < lstChildEmitters.Items.Count; i++)
				{
					CurrentParticleGroup.ChildEmitters.Add((String)lstChildEmitters.Items[i]);
				}
			}
		}

		private void UpdateParticleDescriptorFromGUI()
		{
			int i;

			// update texture filename
			CurrentParticleDescriptor.Texture = txtTexture.Text;

			// update life properties
			
			CurrentParticleDescriptor.Life = (int)numParticleLife.Value;
			CurrentParticleDescriptor.LifeVariance = (int)numParticleLifeVariance.Value;

			// update color keyframes
			CurrentParticleDescriptor.ClearColorKeyframes();
			for (i = 0; i < mtsColor.GetThumbCount(); i++)
			{
				CurrentParticleDescriptor.AddColorKeyframe(mtsColor.GetThumbSlideLocation(i),
														   (Color)mtsColor.GetObjectAtThumb(i));
			}

			// update alpha keyframes
			CurrentParticleDescriptor.ClearAlphaKeyframes();
			for (i = 0; i < mtsAlpha.GetThumbCount(); i++)
			{
				CurrentParticleDescriptor.AddAlphaKeyframe(mtsAlpha.GetThumbSlideLocation(i),
														   Convert.ToInt32(mtsAlpha.GetObjectAtThumb(i)));
			}

			// update velocity keyframes
			CurrentParticleDescriptor.ClearVelocityKeyframes();
			for (i = 0; i < mtsVelocity.GetThumbCount(); i++)
			{
				CurrentParticleDescriptor.AddVelocityKeyframe(mtsVelocity.GetThumbSlideLocation(i),
															  Convert.ToInt32(mtsVelocity.GetObjectAtThumb(i)));
			}

			// update size keyframes
			CurrentParticleDescriptor.ClearSizeKeyframes();
			for (i = 0; i < mtsSize.GetThumbCount(); i++)
			{
				CurrentParticleDescriptor.AddSizeKeyframe(mtsSize.GetThumbSlideLocation(i),
														  Convert.ToInt32(mtsSize.GetObjectAtThumb(i)));
			}

			// update spin keyframes
			CurrentParticleDescriptor.ClearSpinKeyframes();
			for (i = 0; i < mtsSpin.GetThumbCount(); i++)
			{
				CurrentParticleDescriptor.AddSpinKeyframe(mtsSpin.GetThumbSlideLocation(i),
														  Convert.ToInt32(mtsSpin.GetObjectAtThumb(i)));
			}

			// update variances for integer properties
			CurrentParticleDescriptor.VelocityVariance = (int)numVelocityVariance.Value;
			CurrentParticleDescriptor.SizeVariance = (int)numSizeVariance.Value;
			CurrentParticleDescriptor.SpinVariance = (int)numSpinVariance.Value;
		}

		// update GUI output based on data structures

		private void UpdateGUIFromEmitter()
		{
			GUILocks ++;
			numEmitterLife.Value = CurrentEmitter.Life;
			numEmitterRadius.Value = CurrentEmitter.Radius;
			GUILocks --;
		}

		private void UpdateGUIFromParticleDescriptor()
		{
			int i;
			int position;
			Color colorValue;
			int numValue;

			GUILocks ++;

			// make sure we actually have a particle to play with
			EnableParticlePropertiesGUI();
			if (CurrentParticleDescriptor != null)
			{
				// filename and texture data
				txtParticleFilename.Text = CurrentParticleDescriptor.GetFilename();
				txtTexture.Text = CurrentParticleDescriptor.Texture;

				// life data
				numParticleLife.Value = CurrentParticleDescriptor.Life;
				numParticleLifeVariance.Value = CurrentParticleDescriptor.LifeVariance;

				// color keyframes
				mtsColor.ClearAllThumbs();
				for (i = 0; i < CurrentParticleDescriptor.GetNumColorKeyframes(); i++)
				{
					CurrentParticleDescriptor.GetColorKeyframe(i, out position, out colorValue);
					mtsColor.AddThumb(position, colorValue, colorValue, false);
				}
				mtsColor.SetSelectedIndex(0);

				// alpha keyframes
				mtsAlpha.ClearAllThumbs();
				for (i = 0; i < CurrentParticleDescriptor.GetNumAlphaKeyframes(); i++)
				{
					CurrentParticleDescriptor.GetAlphaKeyframe(i, out position, out numValue);
					mtsAlpha.AddThumb(position, numValue, Color.FromArgb(numValue, numValue, numValue), false);
				}
				mtsAlpha.SetSelectedIndex(0);

				// velocity keyframes
				mtsVelocity.ClearAllThumbs();
				for (i = 0; i < CurrentParticleDescriptor.GetNumVelocityKeyframes(); i++)
				{
					CurrentParticleDescriptor.GetVelocityKeyframe(i, out position, out numValue);
					mtsVelocity.AddThumb(position, numValue);
				}
				mtsVelocity.SetSelectedIndex(0);
				numVelocityVariance.Value = CurrentParticleDescriptor.VelocityVariance;

				// size keyframes
				mtsSize.ClearAllThumbs();
				for (i = 0; i < CurrentParticleDescriptor.GetNumSizeKeyframes(); i++)
				{
					CurrentParticleDescriptor.GetSizeKeyframe(i, out position, out numValue);
					mtsSize.AddThumb(position, numValue);
				}
				mtsSize.SetSelectedIndex(0);
				numSizeVariance.Value = CurrentParticleDescriptor.SizeVariance;

				// spin keyframes
				mtsSpin.ClearAllThumbs();
				for (i = 0; i < CurrentParticleDescriptor.GetNumSpinKeyframes(); i++)
				{
					CurrentParticleDescriptor.GetSpinKeyframe(i, out position, out numValue);
					mtsSpin.AddThumb(position, numValue);
				}
				mtsSpin.SetSelectedIndex(0);
				numSpinVariance.Value = CurrentParticleDescriptor.SpinVariance;

				// also show the texture that we have selected
				UpdateTexturePreview();
			}

			GUILocks --;
		}

		private void UpdateGUIFromParticleGroup()
		{
			int i;

			if (CurrentParticleGroup != null)
			{
				GUILocks ++;

				grpParticleGroupProperties.Enabled = true;

				// assign basic particle properties to GUI
				txtParticleToEmit.Text = CurrentParticleGroup.Name;
				numParticleCount.Value = CurrentParticleGroup.EmissionCount;
				numInitialDelay.Value = CurrentParticleGroup.InitialDelay;
				chkReEmit.Checked = CurrentParticleGroup.ReEmit;
				numReEmitInterval.Value = CurrentParticleGroup.ReEmitInterval;
				chkParticleEnabled.Checked = CurrentParticleGroup.Enabled;
				chkParticleSolo.Checked = CurrentParticleGroup.Solo;
				chkSpawnCenter.Checked = CurrentParticleGroup.SpawnCenter;
				chkAdditiveBlending.Checked = CurrentParticleGroup.AdditiveBlending;

				// life inheritance properties
				chkAffectsSize.Checked = CurrentParticleGroup.EmitterAffectsSize;
				chkAffectsVelocity.Checked = CurrentParticleGroup.EmitterAffectsVelocity;
				chkAffectsLifespan.Checked = CurrentParticleGroup.EmitterAffectsLifespan;

				// child emitter names
				lstChildEmitters.Items.Clear();
				for (i = 0; i < CurrentParticleGroup.ChildEmitters.Count; i++)
				{
					lstChildEmitters.Items.Add(CurrentParticleGroup.ChildEmitters[i]);
				}

				GUILocks --;
			}
			else
			{
				grpParticleGroupProperties.Enabled = false;
			}
		}

		private void UpdateFormTitle()
		{
			this.Text = ShortFileName + (Changed ? "*" : "") + " - Fornax Particle Editor";
		}

		private void UpdateTexturePreview()
		{
			String basePath;
			String texturePath;

			if (CurrentParticleDescriptor != null)
			{
				basePath = Util.GetDirectoryName(CurrentEmitter.AbsoluteFileName);
				texturePath = basePath + "/../" + CurrentParticleDescriptor.Texture;

				if (File.Exists(texturePath))
				{
					// thanks, StackoverFlow: https://stackoverflow.com/a/5961665/11336153
					using (System.IO.FileStream fs = new System.IO.FileStream(texturePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
					{
						System.IO.MemoryStream ms = new System.IO.MemoryStream();
						fs.CopyTo(ms);
						ms.Seek(0, System.IO.SeekOrigin.Begin);
						picTexture.Image = Image.FromStream(ms);
					} 
				}
				else
				{
					picTexture.Image = Properties.Resources.no_image;
				}
			}
		}
		
		private void EnableParticleGroupGUI()
		{
			bool fileOpenOrSaved = !String.IsNullOrWhiteSpace(LongFileName);

			grpParticlesToEmit.Enabled = fileOpenOrSaved;
			grpEmitterProperties.Enabled = fileOpenOrSaved;
		}

		private void EnableParticlePropertiesGUI()
		{
			grpProperties.Enabled = !String.IsNullOrWhiteSpace(LongFileName) && CurrentParticleDescriptor != null && CurrentParticleGroup != null;
		}

		// .NET events

		private void btnColor_Click(object sender, EventArgs e)
		{
			SelectColor();
			SetFileAsChanged();
		}

		private void btnAlpha_Click(object sender, EventArgs e)
		{
			SelectAlpha();
			SetFileAsChanged();
		}

		private void btnAddParticle_Click(object sender, EventArgs e)
		{
			AddParticleButton();
			SetFileAsChanged();
		}

		private void lstParticlesToEmit_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (CurrentParticleGroup != null && CurrentParticleDescriptor != null)
			{
				UpdateParticleDescriptorFromGUI();
				UpdateParticleGroupFromGUI();
			}

			if (lstParticlesToEmit.SelectedIndex >= 0)
			{
				CurrentParticleGroup = CurrentEmitter.GetParticleGroup(lstParticlesToEmit.SelectedIndex);
				CurrentParticleDescriptor = CurrentParticleGroup.Particle;
			}
			else
			{
				CurrentParticleGroup = null;
			}

			GUILocks ++;
			UpdateGUIFromParticleGroup();
			UpdateGUIFromParticleDescriptor();
			UpdateGUIKeyframeValues();
			GUILocks --;
		}

		private void numEmissionCount_ValueChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleGroupFromGUI();
				SetFileAsChanged();
			}
		}

		private void chkParticleEnabled_CheckedChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleGroupFromGUI();
				SetFileAsChanged();
			}
		}

		private void chkParticleSolo_CheckedChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleGroupFromGUI();
				SetFileAsChanged();
			}
		}

		private void chkSpawnCenter_CheckedChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleGroupFromGUI();
				SetFileAsChanged();
			}
		}

		private void txtTrail_TextChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleGroupFromGUI();
				SetFileAsChanged();
			}
		}

		private void rdoTrailAdditive_CheckedChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleGroupFromGUI();
				SetFileAsChanged();
			}
		}

		private void rdoTrailStandard_CheckedChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleGroupFromGUI();
				SetFileAsChanged();
			}
		}

		private void chkAffectsSize_CheckedChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleGroupFromGUI();
				SetFileAsChanged();
			}
		}

		private void chkAffectsVelocity_CheckedChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleGroupFromGUI();
				SetFileAsChanged();
			}
		}

		private void chkAffectsLifespan_CheckedChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleGroupFromGUI();
				SetFileAsChanged();
			}
		}

		private void btnRemoveParticle_Click(object sender, EventArgs e)
		{
			RemoveSelectedParticleButton();

			// indicate that changes need to be saved
			SetFileAsChanged();
		}

		private void btnAddColor_Click(object sender, EventArgs e)
		{
			Color colorToUse = Color.White;
			int valueToUse = 0;

			// see if we can use a selected keyframe to guide the creation of our new one
			if (mtsColor.GetSelectedIndex() >= 0)
			{
				colorToUse = (Color)mtsColor.GetObjectAtThumb(mtsColor.GetSelectedIndex());
				valueToUse = mtsColor.GetThumbSlideLocation(mtsColor.GetSelectedIndex()) + 10;
			}

			// create our new keyframe
			mtsColor.AddThumb(valueToUse, colorToUse, colorToUse, false);

			// enable the keyframe buttons
			EnableColorButtons();
		}

		private void btnRemoveColor_Click(object sender, EventArgs e)
		{
			// see if there's anything to unselect at all
			if (mtsColor.GetSelectedIndex() >= 0)
			{
				// we need at least one keyframe
				if (mtsColor.GetThumbCount() > 1)
				{
					mtsColor.RemoveThumb(mtsColor.GetSelectedIndex());
					EnableColorButtons();
				}
			}

			// indicate that changes need to be saved
			SetFileAsChanged();
		}

		private void mtsColor_SelectionChanged(object sender, MultiThumbSlider.MultiThumbSliderEventArgs e)
		{
			UpdateGUIColorValue();
		}

		private void btnAddAlpha_Click(object sender, EventArgs e)
		{
			int alphaToUse = 255;
			int valueToUse = 0;

			// see if we can use a selected keyframe to guide the creation of our new one
			if (mtsAlpha.GetSelectedIndex() >= 0)
			{
				alphaToUse = Convert.ToInt32(mtsAlpha.GetObjectAtThumb(mtsAlpha.GetSelectedIndex()));
				valueToUse = mtsAlpha.GetThumbSlideLocation(mtsAlpha.GetSelectedIndex()) + 10;
			}

			// create our new keyframe
			mtsAlpha.AddThumb(valueToUse, alphaToUse, Color.FromArgb(alphaToUse, alphaToUse, alphaToUse), false);

			// enable the keyframe buttons
			EnableAlphaButtons();

			// indicate that changes need to be saved
			SetFileAsChanged();
		}

		private void btnRemoveAlpha_Click(object sender, EventArgs e)
		{
			// see if there's anything to unselect at all
			if (mtsAlpha.GetSelectedIndex() >= 0)
			{
				// we need at least one keyframe
				if (mtsAlpha.GetThumbCount() > 1)
				{
					mtsAlpha.RemoveThumb(mtsAlpha.GetSelectedIndex());
					EnableAlphaButtons();
				}
			}

			// indicate that changes need to be saved
			SetFileAsChanged();
		}

		private void mtsAlpha_SelectionChanged(object sender, MultiThumbSlider.MultiThumbSliderEventArgs e)
		{
			UpdateGUIAlphaValue();
		}

		private void btnAddVelocity_Click(object sender, EventArgs e)
		{
			int velocityToUse = 0;
			int valueToUse = 0;

			// see if we can use a selected keyframe to guide the creation of our new one
			if (mtsVelocity.GetSelectedIndex() >= 0)
			{
				velocityToUse = Convert.ToInt32(mtsVelocity.GetObjectAtThumb(mtsVelocity.GetSelectedIndex()));
				valueToUse = mtsVelocity.GetThumbSlideLocation(mtsVelocity.GetSelectedIndex()) + 10;
			}

			// create our new keyframe
			mtsVelocity.AddThumb(valueToUse, velocityToUse);

			// enable the keyframe buttons
			EnableVelocityButtons();

			// indicate that changes need to be saved
			SetFileAsChanged();
		}

		private void btnRemoveVelocity_Click(object sender, EventArgs e)
		{
			// see if there's anything to unselect at all
			if (mtsVelocity.GetSelectedIndex() >= 0)
			{
				// we need at least one keyframe
				if (mtsVelocity.GetThumbCount() > 1)
				{
					mtsVelocity.RemoveThumb(mtsVelocity.GetSelectedIndex());
					EnableVelocityButtons();
				}
			}

			// indicate that changes need to be saved
			SetFileAsChanged();
		}

		private void mtsVelocity_SelectionChanged(object sender, MultiThumbSlider.MultiThumbSliderEventArgs e)
		{
			UpdateGUIVelocityValue();
		}

		private void numVelocityValue_ValueChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				if (mtsVelocity.GetSelectedIndex() >= 0)
				{
					// save value in keyframe
					mtsVelocity.SetObjectAtThumb(mtsVelocity.GetSelectedIndex(), ((NumericUpDown)sender).Value);
				}

				// indicate that changes need to be saved
				SetFileAsChanged();
			}
		}

		private void btnAddSize_Click(object sender, EventArgs e)
		{
			int sizeToUse = 0;
			int valueToUse = 0;

			// see if we can use a selected keyframe to guide the creation of our new one
			if (mtsSize.GetSelectedIndex() >= 0)
			{
				sizeToUse = Convert.ToInt32(mtsSize.GetObjectAtThumb(mtsSize.GetSelectedIndex()));
				valueToUse = mtsSize.GetThumbSlideLocation(mtsSize.GetSelectedIndex()) + 10;
			}

			// create our new keyframe
			mtsSize.AddThumb(valueToUse, sizeToUse);

			// enable the keyframe buttons
			EnableSizeButtons();

			// indicate that changes need to be saved
			SetFileAsChanged();
		}

		private void btnRemoveSize_Click(object sender, EventArgs e)
		{
			// see if there's anything to unselect at all
			if (mtsSize.GetSelectedIndex() >= 0)
			{
				// we need at least one keyframe
				if (mtsSize.GetThumbCount() > 1)
				{
					mtsSize.RemoveThumb(mtsSize.GetSelectedIndex());
					EnableSizeButtons();
				}
			}

			// indicate that changes need to be saved
			SetFileAsChanged();
		}

		private void mtsSize_SelectionChanged(object sender, MultiThumbSlider.MultiThumbSliderEventArgs e)
		{
			UpdateGUISizeValue();
		}

		private void numSizeValue_ValueChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				if (mtsSize.GetSelectedIndex() >= 0)
				{
					// save value in keyframe
					mtsSize.SetObjectAtThumb(mtsSize.GetSelectedIndex(), ((NumericUpDown)sender).Value);
				}

				// indicate that changes need to be saved
				SetFileAsChanged();
			}
		}

		private void btnAddSpin_Click(object sender, EventArgs e)
		{
			int spinToUse = 0;
			int valueToUse = 0;

			// see if we can use a selected keyframe to guide the creation of our new one
			if (mtsSpin.GetSelectedIndex() >= 0)
			{
				spinToUse = Convert.ToInt32(mtsSpin.GetObjectAtThumb(mtsSpin.GetSelectedIndex()));
				valueToUse = mtsSpin.GetThumbSlideLocation(mtsSpin.GetSelectedIndex()) + 10;
			}

			// create our new keyframe
			mtsSpin.AddThumb(valueToUse, spinToUse);

			// enable the keyframe buttons
			EnableSpinButtons();

			// indicate that changes need to be saved
			SetFileAsChanged();
		}

		private void btnRemoveSpin_Click(object sender, EventArgs e)
		{
			// see if there's anything to unselect at all
			if (mtsSpin.GetSelectedIndex() >= 0)
			{
				// we need at least one keyframe
				if (mtsSpin.GetThumbCount() > 1)
				{
					mtsSpin.RemoveThumb(mtsSpin.GetSelectedIndex());
					EnableSizeButtons();
				}
			}

			// indicate that changes need to be saved
			SetFileAsChanged();
		}

		private void mtsSpin_SelectionChanged(object sender, MultiThumbSlider.MultiThumbSliderEventArgs e)
		{
			UpdateGUISpinValue();
		}

		private void numSpinValue_ValueChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				if (mtsSpin.GetSelectedIndex() >= 0)
				{
					// save value in keyframe
					mtsSpin.SetObjectAtThumb(mtsSpin.GetSelectedIndex(), ((NumericUpDown)sender).Value);
				}

				// indicate that changes need to be saved
				SetFileAsChanged();
			}
		}

		private void btnSaveAs_Click(object sender, EventArgs e)
		{
			SaveAsButton();
		}

		private void numEmitterLife_ValueChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateEmitterFromGUI();
				SetFileAsChanged();
			}
		}

		private void numEmitterRadius_ValueChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateEmitterFromGUI();
				SetFileAsChanged();
			}
		}

		private void btnAbout_Click(object sender, EventArgs e)
		{
			AboutButton();
		}

		private void btnOpen_Click(object sender, EventArgs e)
		{
			OpenButton();
		}

		private void btnSaveAll_Click(object sender, EventArgs e)
		{
			SaveAllButton();
		}

		private void newToolStripMenuItem_Click(object sender, EventArgs e)
		{
			NewButton();
		}

		private void openToolStripMenuItem_Click(object sender, EventArgs e)
		{
			OpenButton();
		}

		private void saveEmitterAsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SaveAsButton();
		}

		private void saveAllToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SaveAllButton();
		}

		private void txtTexture_TextChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleDescriptorFromGUI();
				SetFileAsChanged();
			}
		}

		private void numParticleLife_ValueChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleDescriptorFromGUI();
				SetFileAsChanged();
			}
		}

		private void numParticleLifeVariance_ValueChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleDescriptorFromGUI();
				SetFileAsChanged();
			}
		}

		private void mtsColor_ThumbSlide(object sender, MultiThumbSlider.MultiThumbSliderEventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleDescriptorFromGUI();
				SetFileAsChanged(true);
			}
		}

		private void mtsAlpha_ThumbSlide(object sender, MultiThumbSlider.MultiThumbSliderEventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleDescriptorFromGUI();
				SetFileAsChanged(true);
			}
		}

		private void mtsVelocity_ThumbSlide(object sender, MultiThumbSlider.MultiThumbSliderEventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleDescriptorFromGUI();
				SetFileAsChanged(true);
			}
		}

		private void mtsSize_ThumbSlide(object sender, MultiThumbSlider.MultiThumbSliderEventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleDescriptorFromGUI();
				SetFileAsChanged(true);
			}
		}

		private void mtsSpin_ThumbSlide(object sender, MultiThumbSlider.MultiThumbSliderEventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleDescriptorFromGUI();
				SetFileAsChanged(true);
			}
		}

		private void numVelocityVariance_ValueChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleDescriptorFromGUI();
				SetFileAsChanged(true);
			}
		}

		private void numSizeVariance_ValueChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleDescriptorFromGUI();
				SetFileAsChanged(true);
			}
		}

		private void numSpinVariance_ValueChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleDescriptorFromGUI();
				SetFileAsChanged();
			}
		}

		private void btnNew_Click(object sender, EventArgs e)
		{
			NewButton();
		}

		private void btnQuit_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void quitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void FornaxEditor_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (CloseFile() == DialogResult.Cancel)
			{
				e.Cancel = true;
			}
		}

		private void btnShowTexture_Click(object sender, EventArgs e)
		{
			UpdateTexturePreview();
		}

		private void btnDuplicate_Click(object sender, EventArgs e)
		{
			DuplicateParticleButton();
			SetFileAsChanged();
		}

		private void btnAddChild_Click(object sender, EventArgs e)
		{
			AddChildEmitter();
		}

		private void btnRemoveChild_Click(object sender, EventArgs e)
		{
			RemoveChildEmitter();
		}

		private void numInitialDelay_ValueChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleGroupFromGUI();
				SetFileAsChanged();
			}
		}

		private void chkReEmit_CheckedChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleGroupFromGUI();
				SetFileAsChanged();
			}
		}

		private void numReEmitInterval_ValueChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleGroupFromGUI();
				SetFileAsChanged();
			}
		}

		private void chkAdditiveBlending_CheckedChanged(object sender, EventArgs e)
		{
			if (GUILocks == 0)
			{
				UpdateParticleGroupFromGUI();
				SetFileAsChanged();
			}
		}

		private void btnPreview_Click(object sender, EventArgs e)
		{
			PreviewButton();
		}

		private void btnExport_Click(object sender, EventArgs e)
		{
			ExportButton();
		}

		private void btnScaleKeyframeValues_Click(object sender, EventArgs e)
		{
			ScaleKeyframeValuesButton();
		}
	}
}
