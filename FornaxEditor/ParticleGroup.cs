﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FornaxEditor
{
	class ParticleGroup
	{
		public String Name;						// short name of particle without path or extension

		public ParticleDescriptor Particle;		// describes the behaviour and appearance of the particle
		public int EmissionCount;				// how many particles of this type to emit
		public int InitialDelay;				// delay before particle is actually spawned
		public bool ReEmit;						// do we emit this particle repeatedly?
		public int ReEmitInterval;				// how often to repeat, in ms, if we are repeating
		public bool Enabled;					// will the particle be processed by the particle engine?
		public bool Solo;						// enables only this and other solo particles
		public bool SpawnCenter;				// ignore emitter radius and always spawn at its center
		public bool AdditiveBlending;			// does this particle use additive blending?

		public bool EmitterAffectsSize;			// does size scale down as the emitter's life expires?
		public bool EmitterAffectsVelocity;		// does velocity scale down as the emitter's life expires?
		public bool EmitterAffectsLifespan;		// does lifetime scale down as the emitter's life expires?

		public List<String> ChildEmitters;		// particles can also own emitters for more advanced effects

		public ParticleGroup(String name, ParticleDescriptor particle)
		{
			Name = name;
			Particle = particle;
			EmissionCount = 1;
			InitialDelay = 0;
			ReEmit = false;
			ReEmitInterval = 0;
			Enabled = true;
			Solo = false;
			SpawnCenter = false;
			AdditiveBlending = false;

			EmitterAffectsSize = true;
			EmitterAffectsVelocity = false;
			EmitterAffectsLifespan = false;

			ChildEmitters = new List<String>();
		}

		public ParticleGroup Clone(String name)
		{
			ParticleGroup result = new ParticleGroup(String.Copy(name), Particle);

			result.EmissionCount = EmissionCount;
			result.InitialDelay = InitialDelay;
			result.ReEmit = ReEmit;
			result.ReEmitInterval = ReEmitInterval;
			result.Enabled = Enabled;
			result.Solo = Solo;
			result.SpawnCenter = SpawnCenter;
			result.AdditiveBlending = AdditiveBlending;

			result.EmitterAffectsSize = EmitterAffectsSize;
			result.EmitterAffectsVelocity = EmitterAffectsVelocity;
			result.EmitterAffectsLifespan = EmitterAffectsLifespan;

			result.ChildEmitters = ChildEmitters;

			return result;
		}

		public override String ToString()
		{
			String result = "";
			int i;

			// particle list node
			result += "\tparticle" + Environment.NewLine;
			result += "\t(" + Environment.NewLine;

			// basic particle properties
			result += "\t\tname(\"" + Name + "\")" + Environment.NewLine;
			result += "\t\temission-count(" + EmissionCount + ")" + Environment.NewLine;
			result += "\t\tinitial-delay(" + InitialDelay + ")" + Environment.NewLine;
			result += "\t\tre-emit(" + (ReEmit ? "1" : "0") + ")" + Environment.NewLine;
			result += "\t\tre-emit-interval(" + ReEmitInterval + ")" + Environment.NewLine;
			result += "\t\tenabled(" + (Enabled ? "1" : "0") + ")" + Environment.NewLine;
			result += "\t\tsolo(" + (Solo ? "1" : "0") + ")" + Environment.NewLine;
			result += "\t\tspawn-center(" + (SpawnCenter ? "1" : "0") + ")" + Environment.NewLine;
			result += "\t\tadditive-blending(" + (AdditiveBlending ? "1" : "0") + ")" + Environment.NewLine;

			// emitter effects on particle properties
			result += "\t\tscale-size(" + (EmitterAffectsSize ? "1" : "0") + ")" + Environment.NewLine;
			result += "\t\tscale-velocity(" + (EmitterAffectsVelocity ? "1" : "0") + ")" + Environment.NewLine;
			result += "\t\tscale-life(" + (EmitterAffectsLifespan ? "1" : "0") + ")" + Environment.NewLine;

			// iterate over child emitters and output them to the file
			for (i = 0; i < ChildEmitters.Count; i++)
			{
				result += "\t\tchild-emitter(\"" + ChildEmitters[i] + "\")" + Environment.NewLine;
			}

			// end of list node
			result += "\t)" + Environment.NewLine;

			return result;
		}
	}
}
