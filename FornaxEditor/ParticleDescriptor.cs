﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace FornaxEditor
{
	class ParticleDescriptor
	{
		// short filename of the particle
		private String Filename;

		// texture we're using
		public String Texture { get; set; }

		// life properties
		public int Life { get; set; }
		public int LifeVariance { get; set; }

		// these are the keyframes for different properties over the life of the particle
		private SortedDictionary<int, Color> ColorKeyframes;
		private SortedDictionary<int, int> AlphaKeyframes;
		private SortedDictionary<int, int> VelocityKeyframes;
		private SortedDictionary<int, int> SizeKeyframes;
		private SortedDictionary<int, int> SpinKeyframes;

		// variance values for particle properties
		public int VelocityVariance { get; set; }
		public int SizeVariance { get; set; }
		public int SpinVariance { get; set; }

		public static ParticleDescriptor CreateDefault(String filename)
		{
			// create the particle and assign the filename to it
			ParticleDescriptor result = new ParticleDescriptor();
			result.Filename = filename;

			// life properties
			result.Life = 1000;
			result.LifeVariance = 0;

			// add some default keyframes since there should be at least one for each property
			result.AddColorKeyframe(0, Color.White);
			result.AddAlphaKeyframe(0, 255);
			result.AddVelocityKeyframe(0, 0);
			result.AddSizeKeyframe(0, 100);
			result.AddSpinKeyframe(0, 0);

			return result;
		}

		public static ParticleDescriptor FromFile(String filename)
		{
			ParticleDescriptor result = new ParticleDescriptor();	// particle to return
			Tree tree = Tree.CreateFromFile(filename);				// tree container of dash structure
			ListNode root = tree.GetRoot() as ListNode;				// root of dash structure (should be a list node)

			NumbersNode keyframe;									// a number node (probably one of several) describing one keyframe in this particle
			int keyframeIndex;										// denotes the (keyframeIndex)th list node we're loading (starting at 0)

			// read basic particle properties
			result.Filename = filename;
			result.Texture = root.GetString("texture", "");

			// read life properties
			result.Life = root.GetNumber("life", 0, 1000);
			result.LifeVariance = root.GetNumber("life-variance", 0, 0);

			// read property variances
			result.VelocityVariance = root.GetNumber("velocity-variance", 0, 0);
			result.SizeVariance = root.GetNumber("size-variance", 0, 0);
			result.SpinVariance = root.GetNumber("spin-variance", 0, 0);
			
			// read color keyframes
			keyframeIndex = 0;
			keyframe = root.GetNumbersNodeByName("color-keyframe", keyframeIndex++);
			while (keyframe != null)
			{
				result.AddColorKeyframe(keyframe.At(0), Color.FromArgb(keyframe.At(1), keyframe.At(2), keyframe.At(3)));
				keyframe = root.GetNumbersNodeByName("color-keyframe", keyframeIndex++);
			}

			// read alpha keyframes
			keyframeIndex = 0;
			keyframe = root.GetNumbersNodeByName("alpha-keyframe", keyframeIndex++);
			while (keyframe != null)
			{
				result.AddAlphaKeyframe(keyframe.At(0), keyframe.At(1));
				keyframe = root.GetNumbersNodeByName("alpha-keyframe", keyframeIndex++);
			}

			// read velocity keyframes
			keyframeIndex = 0;
			keyframe = root.GetNumbersNodeByName("velocity-keyframe", keyframeIndex++);
			while (keyframe != null)
			{
				result.AddVelocityKeyframe(keyframe.At(0), keyframe.At(1));
				keyframe = root.GetNumbersNodeByName("velocity-keyframe", keyframeIndex++);
			}

			// read size keyframes
			keyframeIndex = 0;
			keyframe = root.GetNumbersNodeByName("size-keyframe", keyframeIndex++);
			while (keyframe != null)
			{
				result.AddSizeKeyframe(keyframe.At(0), keyframe.At(1));
				keyframe = root.GetNumbersNodeByName("size-keyframe", keyframeIndex++);
			}

			// read spin keyframes
			keyframeIndex = 0;
			keyframe = root.GetNumbersNodeByName("spin-keyframe", keyframeIndex++);
			while (keyframe != null)
			{
				result.AddSpinKeyframe(keyframe.At(0), keyframe.At(1));
				keyframe = root.GetNumbersNodeByName("spin-keyframe", keyframeIndex++);
			}

			return result;
		}

		private ParticleDescriptor()
		{
			Filename = "";
			Texture = "png/smoke.png";

			Life = 1000;
			LifeVariance = 0;

			ColorKeyframes = new SortedDictionary<int, Color>();
			AlphaKeyframes = new SortedDictionary<int, int>();
			VelocityKeyframes = new SortedDictionary<int, int>();
			SizeKeyframes = new SortedDictionary<int, int>();
			SpinKeyframes = new SortedDictionary<int, int>();

			VelocityVariance = 0;
			SizeVariance = 0;
			SpinVariance = 0;
		}

		public ParticleDescriptor Clone(String filename)
		{
			ParticleDescriptor result = new ParticleDescriptor();

			result.Filename = filename;
			result.Texture = Texture;

			result.Life = Life;
			result.LifeVariance = LifeVariance;

			result.ColorKeyframes = new SortedDictionary<int, Color>(ColorKeyframes);
			result.AlphaKeyframes = new SortedDictionary<int, int>(AlphaKeyframes);
			result.VelocityKeyframes = new SortedDictionary<int, int>(VelocityKeyframes);
			result.SizeKeyframes = new SortedDictionary<int, int>(SizeKeyframes);
			result.SpinKeyframes = new SortedDictionary<int, int>(SpinKeyframes);

			result.VelocityVariance = VelocityVariance;
			result.SizeVariance = SizeVariance;
			result.SpinVariance = SpinVariance;

			return result;
		}

		public String GetFilename()
		{
			return Filename;
		}

		public int GetNumColorKeyframes()
		{
			return ColorKeyframes.Count;
		}

		public int GetNumAlphaKeyframes()
		{
			return AlphaKeyframes.Count;
		}

		public int GetNumVelocityKeyframes()
		{
			return VelocityKeyframes.Count;
		}

		public int GetNumSizeKeyframes()
		{
			return SizeKeyframes.Count;
		}

		public int GetNumSpinKeyframes()
		{
			return SpinKeyframes.Count;
		}

		public void GetColorKeyframe(int index, out int position, out Color value)
		{
			position = ColorKeyframes.Keys.ElementAt(index);
			value = ColorKeyframes[position];
		}

		public void GetAlphaKeyframe(int index, out int position, out int value)
		{
			position = AlphaKeyframes.Keys.ElementAt(index);
			value = AlphaKeyframes[position];
		}

		public void GetVelocityKeyframe(int index, out int position, out int value)
		{
			position = VelocityKeyframes.Keys.ElementAt(index);
			value = VelocityKeyframes[position];
		}

		public void GetSizeKeyframe(int index, out int position, out int value)
		{
			position = SizeKeyframes.Keys.ElementAt(index);
			value = SizeKeyframes[position];
		}

		public void GetSpinKeyframe(int index, out int position, out int value)
		{
			position = SpinKeyframes.Keys.ElementAt(index);
			value = SpinKeyframes[position];
		}

		public void ClearColorKeyframes()
		{
			ColorKeyframes.Clear();
		}

		public void ClearAlphaKeyframes()
		{
			AlphaKeyframes.Clear();
		}

		public void ClearVelocityKeyframes()
		{
			VelocityKeyframes.Clear();
		}

		public void ClearSizeKeyframes()
		{
			SizeKeyframes.Clear();
		}

		public void ClearSpinKeyframes()
		{
			SpinKeyframes.Clear();
		}

		public void AddColorKeyframe(int time, Color color)
		{
			ColorKeyframes[time] = color;
		}

		public void AddAlphaKeyframe(int time, int alpha)
		{
			AlphaKeyframes[time] = alpha;
		}

		public void AddVelocityKeyframe(int time, int velocity)
		{
			VelocityKeyframes[time] = velocity;
		}

		public void AddSizeKeyframe(int time, int size)
		{
			SizeKeyframes[time] = size;
		}

		public void AddSpinKeyframe(int time, int spin)
		{
			SpinKeyframes[time] = spin;
		}

		public void ScaleAlpha(double amount)
		{
			SortedDictionary<int, int> NewKeyframes = new SortedDictionary<int, int>();

			foreach (KeyValuePair<int, int> keyframe in AlphaKeyframes)
			{
				NewKeyframes[keyframe.Key] = (int)Math.Round((double)keyframe.Value * amount);
			}

			AlphaKeyframes = NewKeyframes;
		}

		public void ScaleVelocity(double amount)
		{
			SortedDictionary<int, int> NewKeyframes = new SortedDictionary<int, int>();

			foreach (KeyValuePair<int, int> keyframe in VelocityKeyframes)
			{
				NewKeyframes[keyframe.Key] = (int)Math.Round((double)keyframe.Value * amount);
			}

			VelocityKeyframes = NewKeyframes;
		}

		public void ScaleSize(double amount)
		{
			SortedDictionary<int, int> NewKeyframes = new SortedDictionary<int, int>();

			foreach (KeyValuePair<int, int> keyframe in SizeKeyframes)
			{
				NewKeyframes[keyframe.Key] = (int)Math.Round((double)keyframe.Value * amount);
			}

			SizeKeyframes = NewKeyframes;
		}

		public void ScaleSpin(double amount)
		{
			SortedDictionary<int, int> NewKeyframes = new SortedDictionary<int, int>();

			foreach (KeyValuePair<int, int> keyframe in SpinKeyframes)
			{
				NewKeyframes[keyframe.Key] = (int)Math.Round((double)keyframe.Value * amount);
			}

			SpinKeyframes = NewKeyframes;
		}

		public override String ToString()
		{
			String result = "";
			int i;

			// beginning comment
			result += "# Particle generated by the Fornax Particle Editor." + Environment.NewLine;
			result += Environment.NewLine;

			// add root node
			result += "particle" + Environment.NewLine;
			result += "(" + Environment.NewLine;
			result += "\ttexture(\"" + Texture + "\")" + Environment.NewLine;
			result += "\tlife(" + Life + ")" + Environment.NewLine;
			result += "\tlife-variance(" + LifeVariance + ")" + Environment.NewLine;
			result += Environment.NewLine;

			// add color keyframes
			result += "\t# color keyframe data" + Environment.NewLine;
			for (i = 0; i < ColorKeyframes.Count; i++)
			{
				result += "\tcolor-keyframe(" + ColorKeyframes.Keys.ElementAt(i) + ", ";
				result += ColorKeyframes.Values.ElementAt(i).R + ", ";
				result += ColorKeyframes.Values.ElementAt(i).G + ", ";
				result += ColorKeyframes.Values.ElementAt(i).B + ")";
				result += Environment.NewLine;
			}
			result += Environment.NewLine;

			// add alpha keyframes
			result += "\t# alpha keyframe data" + Environment.NewLine;
			for (i = 0; i < AlphaKeyframes.Count; i++)
			{
				result += "\talpha-keyframe(" + AlphaKeyframes.Keys.ElementAt(i) + ", " + AlphaKeyframes.Values.ElementAt(i) + ")" + Environment.NewLine;
			}
			result += Environment.NewLine;

			// add velocity keyframes
			result += "\t# velocity keyframe data" + Environment.NewLine;
			for (i = 0; i < VelocityKeyframes.Count; i++)
			{
				result += "\tvelocity-keyframe(" + VelocityKeyframes.Keys.ElementAt(i) + ", " + VelocityKeyframes.Values.ElementAt(i) + ")" + Environment.NewLine;
			}
			result += "\tvelocity-variance(" + VelocityVariance + ")" + Environment.NewLine;
			result += Environment.NewLine;

			// add size keyframes
			result += "\t# size keyframe data" + Environment.NewLine;
			for (i = 0; i < SizeKeyframes.Count; i++)
			{
				result += "\tsize-keyframe(" + SizeKeyframes.Keys.ElementAt(i) + ", " + SizeKeyframes.Values.ElementAt(i) + ")" + Environment.NewLine;
			}
			result += "\tsize-variance(" + SizeVariance + ")" + Environment.NewLine;
			result += Environment.NewLine;

			// add spin keyframes
			result += "\t# spin keyframe data" + Environment.NewLine;
			for (i = 0; i < SpinKeyframes.Count; i++)
			{
				result += "\tspin-keyframe(" + SpinKeyframes.Keys.ElementAt(i) + ", " + SpinKeyframes.Values.ElementAt(i) + ")" + Environment.NewLine;
			}
			result += "\tspin-variance(" + SpinVariance + ")" + Environment.NewLine;

			// add final closing parenthesis
			result += ")" + Environment.NewLine;

			// and we're done!
			return result;
		}
	}
}
