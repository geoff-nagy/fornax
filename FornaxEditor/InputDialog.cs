﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FornaxEditor
{
	public partial class InputDialog : Form
	{
		public InputDialog(String title, String message, String defaultText)
		{
			InitializeComponent();
			this.Text = title;
			lblMessage.Text = message;
			txtText.Text = defaultText;
		}

		public String GetInputText()
		{
			return txtText.Text;
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
			Close();
		}

		private void btnAccept_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.OK;
			Close();
		}
	}
}
