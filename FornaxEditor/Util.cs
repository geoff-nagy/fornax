﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace FornaxEditor
{
	class Util
	{
		// given an absolute file path, remove the directory info so we just get the name of the file
		public static String GetShortFilename(String fullFileName)
		{
			int ShortFilenameStart = fullFileName.LastIndexOf(Path.DirectorySeparatorChar) + 1;
			return fullFileName.Substring(ShortFilenameStart, fullFileName.Length - ShortFilenameStart);
		}

		// given an absolute file path, remove the name of the file so we just get the absolute path to the containing folder
		public static String GetDirectoryName(String fullFileName)
		{
			int ShortFilenameStart = fullFileName.LastIndexOf(Path.DirectorySeparatorChar) + 1;
			return fullFileName.Substring(0, ShortFilenameStart - 1);
		}
	}
}
