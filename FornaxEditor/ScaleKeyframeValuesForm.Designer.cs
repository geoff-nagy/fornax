﻿namespace FornaxEditor
{
	partial class ScaleKeyframeValuesForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtVelocityScale = new System.Windows.Forms.TextBox();
			this.txtSizeScale = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtSpinScale = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.btnAccept = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.txtAlphaScale = new System.Windows.Forms.TextBox();
			this.errAlpha = new System.Windows.Forms.ErrorProvider(this.components);
			this.errVelocity = new System.Windows.Forms.ErrorProvider(this.components);
			this.errSize = new System.Windows.Forms.ErrorProvider(this.components);
			this.errSpin = new System.Windows.Forms.ErrorProvider(this.components);
			((System.ComponentModel.ISupportInitialize)(this.errAlpha)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errVelocity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errSize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errSpin)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(73, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Alpha scaling:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 42);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(83, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "Velocity scaling:";
			// 
			// txtVelocityScale
			// 
			this.errAlpha.SetIconPadding(this.txtVelocityScale, 5);
			this.errVelocity.SetIconPadding(this.txtVelocityScale, 5);
			this.errSpin.SetIconPadding(this.txtVelocityScale, 5);
			this.errSize.SetIconPadding(this.txtVelocityScale, 5);
			this.txtVelocityScale.Location = new System.Drawing.Point(127, 39);
			this.txtVelocityScale.Name = "txtVelocityScale";
			this.txtVelocityScale.Size = new System.Drawing.Size(88, 20);
			this.txtVelocityScale.TabIndex = 1;
			this.txtVelocityScale.Text = "1.0";
			this.txtVelocityScale.Validating += new System.ComponentModel.CancelEventHandler(this.txtVelocityScale_Validating);
			this.txtVelocityScale.Validated += new System.EventHandler(this.txtVelocityScale_Validated);
			// 
			// txtSizeScale
			// 
			this.errAlpha.SetIconPadding(this.txtSizeScale, 5);
			this.errVelocity.SetIconPadding(this.txtSizeScale, 5);
			this.errSpin.SetIconPadding(this.txtSizeScale, 5);
			this.errSize.SetIconPadding(this.txtSizeScale, 5);
			this.txtSizeScale.Location = new System.Drawing.Point(127, 65);
			this.txtSizeScale.Name = "txtSizeScale";
			this.txtSizeScale.Size = new System.Drawing.Size(88, 20);
			this.txtSizeScale.TabIndex = 2;
			this.txtSizeScale.Text = "1.0";
			this.txtSizeScale.Validating += new System.ComponentModel.CancelEventHandler(this.txtSizeScale_Validating);
			this.txtSizeScale.Validated += new System.EventHandler(this.txtSizeScale_Validated);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 68);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(66, 13);
			this.label3.TabIndex = 5;
			this.label3.Text = "Size scaling:";
			// 
			// txtSpinScale
			// 
			this.errAlpha.SetIconPadding(this.txtSpinScale, 5);
			this.errVelocity.SetIconPadding(this.txtSpinScale, 5);
			this.errSpin.SetIconPadding(this.txtSpinScale, 5);
			this.errSize.SetIconPadding(this.txtSpinScale, 5);
			this.txtSpinScale.Location = new System.Drawing.Point(127, 91);
			this.txtSpinScale.Name = "txtSpinScale";
			this.txtSpinScale.Size = new System.Drawing.Size(88, 20);
			this.txtSpinScale.TabIndex = 3;
			this.txtSpinScale.Text = "1.0";
			this.txtSpinScale.Validating += new System.ComponentModel.CancelEventHandler(this.txtSpinScale_Validating);
			this.txtSpinScale.Validated += new System.EventHandler(this.txtSpinScale_Validated);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 94);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(67, 13);
			this.label4.TabIndex = 7;
			this.label4.Text = "Spin scaling:";
			// 
			// btnAccept
			// 
			this.btnAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnAccept.Location = new System.Drawing.Point(127, 129);
			this.btnAccept.Name = "btnAccept";
			this.btnAccept.Size = new System.Drawing.Size(109, 32);
			this.btnAccept.TabIndex = 5;
			this.btnAccept.Text = "Accept";
			this.btnAccept.UseVisualStyleBackColor = true;
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(12, 129);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(109, 32);
			this.btnCancel.TabIndex = 4;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			// 
			// txtAlphaScale
			// 
			this.txtAlphaScale.BackColor = System.Drawing.SystemColors.Window;
			this.txtAlphaScale.ForeColor = System.Drawing.SystemColors.WindowText;
			this.errAlpha.SetIconPadding(this.txtAlphaScale, 5);
			this.errVelocity.SetIconPadding(this.txtAlphaScale, 5);
			this.errSpin.SetIconPadding(this.txtAlphaScale, 5);
			this.errSize.SetIconPadding(this.txtAlphaScale, 5);
			this.txtAlphaScale.Location = new System.Drawing.Point(127, 13);
			this.txtAlphaScale.Name = "txtAlphaScale";
			this.txtAlphaScale.Size = new System.Drawing.Size(88, 20);
			this.txtAlphaScale.TabIndex = 0;
			this.txtAlphaScale.Text = "1.0";
			this.txtAlphaScale.Validating += new System.ComponentModel.CancelEventHandler(this.txtAlphaScale_Validating);
			this.txtAlphaScale.Validated += new System.EventHandler(this.txtAlphaScale_Validated);
			// 
			// errAlpha
			// 
			this.errAlpha.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
			this.errAlpha.ContainerControl = this;
			// 
			// errVelocity
			// 
			this.errVelocity.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
			this.errVelocity.ContainerControl = this;
			// 
			// errSize
			// 
			this.errSize.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
			this.errSize.ContainerControl = this;
			// 
			// errSpin
			// 
			this.errSpin.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
			this.errSpin.ContainerControl = this;
			// 
			// ScaleKeyframeValuesForm
			// 
			this.AcceptButton = this.btnAccept;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(246, 174);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnAccept);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.txtSpinScale);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txtSizeScale);
			this.Controls.Add(this.txtVelocityScale);
			this.Controls.Add(this.txtAlphaScale);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ScaleKeyframeValuesForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Scale Keyframe Values";
			((System.ComponentModel.ISupportInitialize)(this.errAlpha)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errVelocity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errSize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errSpin)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtVelocityScale;
		private System.Windows.Forms.TextBox txtSizeScale;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtSpinScale;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button btnAccept;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.TextBox txtAlphaScale;
		private System.Windows.Forms.ErrorProvider errAlpha;
		private System.Windows.Forms.ErrorProvider errVelocity;
		private System.Windows.Forms.ErrorProvider errSize;
		private System.Windows.Forms.ErrorProvider errSpin;
	}
}