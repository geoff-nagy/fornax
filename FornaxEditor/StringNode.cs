﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FornaxEditor
{
	class StringNode : Node
	{
		private String value;

		public StringNode(String name) : base(name) { }

		public void SetValue(String value)
		{
			this.value = value;
		}

		public string GetValue()
		{
			return value;
		}

		public override String ToString(int indentLevel)
		{
			int i;
			String result = "";

			// indent to the appropriate level
			for (i = 0; i < indentLevel; i++) result += "\t";

			// add the node name, and value in quotes
			result += GetName() + " \"" + value + "\"" + Environment.NewLine; ;

			return result;
		}

	}
}
