﻿namespace FornaxEditor
{
	partial class FornaxEditor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FornaxEditor));
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.btnNew = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.btnOpen = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.btnSaveAs = new System.Windows.Forms.ToolStripButton();
			this.btnSaveAll = new System.Windows.Forms.ToolStripButton();
			this.btnQuit = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
			this.btnAbout = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
			this.btnExport = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
			this.btnPreview = new System.Windows.Forms.ToolStripButton();
			this.lstParticlesToEmit = new System.Windows.Forms.ListBox();
			this.btnAddParticle = new System.Windows.Forms.Button();
			this.btnRemoveParticle = new System.Windows.Forms.Button();
			this.grpParticlesToEmit = new System.Windows.Forms.GroupBox();
			this.btnDuplicate = new System.Windows.Forms.Button();
			this.numParticleCount = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.chkParticleEnabled = new System.Windows.Forms.CheckBox();
			this.chkParticleSolo = new System.Windows.Forms.CheckBox();
			this.numReEmitInterval = new System.Windows.Forms.NumericUpDown();
			this.txtParticleToEmit = new System.Windows.Forms.TextBox();
			this.grpParticleGroupProperties = new System.Windows.Forms.GroupBox();
			this.numInitialDelay = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.chkReEmit = new System.Windows.Forms.CheckBox();
			this.chkAdditiveBlending = new System.Windows.Forms.CheckBox();
			this.chkSpawnCenter = new System.Windows.Forms.CheckBox();
			this.grpChildEmitters = new System.Windows.Forms.GroupBox();
			this.btnRemoveChild = new System.Windows.Forms.Button();
			this.btnAddChild = new System.Windows.Forms.Button();
			this.lstChildEmitters = new System.Windows.Forms.ListBox();
			this.grpParticleLifespan = new System.Windows.Forms.GroupBox();
			this.chkAffectsLifespan = new System.Windows.Forms.CheckBox();
			this.chkAffectsVelocity = new System.Windows.Forms.CheckBox();
			this.chkAffectsSize = new System.Windows.Forms.CheckBox();
			this.label1 = new System.Windows.Forms.Label();
			this.numEmitterLife = new System.Windows.Forms.NumericUpDown();
			this.numEmitterRadius = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.grpEmitterProperties = new System.Windows.Forms.GroupBox();
			this.label6 = new System.Windows.Forms.Label();
			this.txtParticleFilename = new System.Windows.Forms.TextBox();
			this.txtTexture = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.grpColor = new System.Windows.Forms.GroupBox();
			this.mtsColor = new MultiThumbSlider.MultiThumbSlider();
			this.btnColor = new System.Windows.Forms.Button();
			this.btnRemoveColor = new System.Windows.Forms.Button();
			this.btnAddColor = new System.Windows.Forms.Button();
			this.grpAlpha = new System.Windows.Forms.GroupBox();
			this.mtsAlpha = new MultiThumbSlider.MultiThumbSlider();
			this.btnAlpha = new System.Windows.Forms.Button();
			this.btnRemoveAlpha = new System.Windows.Forms.Button();
			this.btnAddAlpha = new System.Windows.Forms.Button();
			this.grpVelocity = new System.Windows.Forms.GroupBox();
			this.mtsVelocity = new MultiThumbSlider.MultiThumbSlider();
			this.numVelocityVariance = new System.Windows.Forms.NumericUpDown();
			this.label9 = new System.Windows.Forms.Label();
			this.numVelocityValue = new System.Windows.Forms.NumericUpDown();
			this.btnRemoveVelocity = new System.Windows.Forms.Button();
			this.btnAddVelocity = new System.Windows.Forms.Button();
			this.grpSize = new System.Windows.Forms.GroupBox();
			this.mtsSize = new MultiThumbSlider.MultiThumbSlider();
			this.numSizeVariance = new System.Windows.Forms.NumericUpDown();
			this.label10 = new System.Windows.Forms.Label();
			this.numSizeValue = new System.Windows.Forms.NumericUpDown();
			this.btnRemoveSize = new System.Windows.Forms.Button();
			this.btnAddSize = new System.Windows.Forms.Button();
			this.grpSpin = new System.Windows.Forms.GroupBox();
			this.mtsSpin = new MultiThumbSlider.MultiThumbSlider();
			this.numSpinVariance = new System.Windows.Forms.NumericUpDown();
			this.label8 = new System.Windows.Forms.Label();
			this.numSpinValue = new System.Windows.Forms.NumericUpDown();
			this.btnRemoveSpin = new System.Windows.Forms.Button();
			this.btnAddSpin = new System.Windows.Forms.Button();
			this.btnShowTexture = new System.Windows.Forms.Button();
			this.grpProperties = new System.Windows.Forms.GroupBox();
			this.numParticleLifeVariance = new System.Windows.Forms.NumericUpDown();
			this.label12 = new System.Windows.Forms.Label();
			this.numParticleLife = new System.Windows.Forms.NumericUpDown();
			this.label11 = new System.Windows.Forms.Label();
			this.picTexture = new System.Windows.Forms.PictureBox();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
			this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
			this.saveEmitterAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
			this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.btnScaleKeyframeValues = new System.Windows.Forms.Button();
			this.toolStrip1.SuspendLayout();
			this.grpParticlesToEmit.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numParticleCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numReEmitInterval)).BeginInit();
			this.grpParticleGroupProperties.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numInitialDelay)).BeginInit();
			this.grpChildEmitters.SuspendLayout();
			this.grpParticleLifespan.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numEmitterLife)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numEmitterRadius)).BeginInit();
			this.grpEmitterProperties.SuspendLayout();
			this.grpColor.SuspendLayout();
			this.grpAlpha.SuspendLayout();
			this.grpVelocity.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numVelocityVariance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numVelocityValue)).BeginInit();
			this.grpSize.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numSizeVariance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numSizeValue)).BeginInit();
			this.grpSpin.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numSpinVariance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numSpinValue)).BeginInit();
			this.grpProperties.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numParticleLifeVariance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numParticleLife)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picTexture)).BeginInit();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// toolStrip1
			// 
			this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
			this.toolStrip1.CanOverflow = false;
			this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.btnNew,
            this.toolStripSeparator2,
            this.btnOpen,
            this.toolStripSeparator3,
            this.btnSaveAs,
            this.btnSaveAll,
            this.btnQuit,
            this.toolStripSeparator4,
            this.btnAbout,
            this.toolStripSeparator5,
            this.btnExport,
            this.toolStripSeparator6,
            this.btnPreview});
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(945, 25);
			this.toolStrip1.TabIndex = 0;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// btnNew
			// 
			this.btnNew.Image = global::FornaxEditor.Properties.Resources._new;
			this.btnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnNew.Name = "btnNew";
			this.btnNew.Size = new System.Drawing.Size(51, 22);
			this.btnNew.Text = "New";
			this.btnNew.ToolTipText = "Start another instance of the editor";
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
			// 
			// btnOpen
			// 
			this.btnOpen.Image = global::FornaxEditor.Properties.Resources.open;
			this.btnOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnOpen.Name = "btnOpen";
			this.btnOpen.Size = new System.Drawing.Size(65, 22);
			this.btnOpen.Text = "Open...";
			this.btnOpen.ToolTipText = "Open another emitter file";
			this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
			// 
			// btnSaveAs
			// 
			this.btnSaveAs.Image = global::FornaxEditor.Properties.Resources.save;
			this.btnSaveAs.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnSaveAs.Name = "btnSaveAs";
			this.btnSaveAs.Size = new System.Drawing.Size(117, 22);
			this.btnSaveAs.Text = "Save Emitter As...";
			this.btnSaveAs.ToolTipText = "Save the emitter to a new file, and save any referenced particles";
			this.btnSaveAs.Click += new System.EventHandler(this.btnSaveAs_Click);
			// 
			// btnSaveAll
			// 
			this.btnSaveAll.Image = global::FornaxEditor.Properties.Resources.save_all;
			this.btnSaveAll.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnSaveAll.Name = "btnSaveAll";
			this.btnSaveAll.Size = new System.Drawing.Size(68, 22);
			this.btnSaveAll.Text = "Save All";
			this.btnSaveAll.ToolTipText = "Save the emitter and any referenced particles";
			this.btnSaveAll.Click += new System.EventHandler(this.btnSaveAll_Click);
			// 
			// btnQuit
			// 
			this.btnQuit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.btnQuit.Image = global::FornaxEditor.Properties.Resources.quit;
			this.btnQuit.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnQuit.Name = "btnQuit";
			this.btnQuit.Size = new System.Drawing.Size(50, 22);
			this.btnQuit.Text = "Quit";
			this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
			// 
			// toolStripSeparator4
			// 
			this.toolStripSeparator4.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.toolStripSeparator4.Name = "toolStripSeparator4";
			this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
			// 
			// btnAbout
			// 
			this.btnAbout.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.btnAbout.Image = global::FornaxEditor.Properties.Resources.about;
			this.btnAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnAbout.Name = "btnAbout";
			this.btnAbout.Size = new System.Drawing.Size(60, 22);
			this.btnAbout.Text = "About";
			this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
			// 
			// toolStripSeparator5
			// 
			this.toolStripSeparator5.Name = "toolStripSeparator5";
			this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
			// 
			// btnExport
			// 
			this.btnExport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnExport.Image = ((System.Drawing.Image)(resources.GetObject("btnExport.Image")));
			this.btnExport.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnExport.Name = "btnExport";
			this.btnExport.Size = new System.Drawing.Size(53, 22);
			this.btnExport.Text = "Export...";
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			// 
			// toolStripSeparator6
			// 
			this.toolStripSeparator6.Name = "toolStripSeparator6";
			this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
			// 
			// btnPreview
			// 
			this.btnPreview.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnPreview.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnPreview.Name = "btnPreview";
			this.btnPreview.Size = new System.Drawing.Size(86, 22);
			this.btnPreview.Text = "Quick Preview";
			this.btnPreview.ToolTipText = "Preview the emitter in an OpenGL window";
			this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
			// 
			// lstParticlesToEmit
			// 
			this.lstParticlesToEmit.FormattingEnabled = true;
			this.lstParticlesToEmit.Location = new System.Drawing.Point(6, 19);
			this.lstParticlesToEmit.Name = "lstParticlesToEmit";
			this.lstParticlesToEmit.Size = new System.Drawing.Size(201, 277);
			this.lstParticlesToEmit.TabIndex = 0;
			this.lstParticlesToEmit.SelectedIndexChanged += new System.EventHandler(this.lstParticlesToEmit_SelectedIndexChanged);
			// 
			// btnAddParticle
			// 
			this.btnAddParticle.Location = new System.Drawing.Point(6, 302);
			this.btnAddParticle.Name = "btnAddParticle";
			this.btnAddParticle.Size = new System.Drawing.Size(201, 27);
			this.btnAddParticle.TabIndex = 1;
			this.btnAddParticle.Text = "Add Particle Reference";
			this.btnAddParticle.UseVisualStyleBackColor = true;
			this.btnAddParticle.Click += new System.EventHandler(this.btnAddParticle_Click);
			// 
			// btnRemoveParticle
			// 
			this.btnRemoveParticle.Location = new System.Drawing.Point(6, 403);
			this.btnRemoveParticle.Name = "btnRemoveParticle";
			this.btnRemoveParticle.Size = new System.Drawing.Size(201, 27);
			this.btnRemoveParticle.TabIndex = 2;
			this.btnRemoveParticle.Text = "Remove From Emitter";
			this.btnRemoveParticle.UseVisualStyleBackColor = true;
			this.btnRemoveParticle.Click += new System.EventHandler(this.btnRemoveParticle_Click);
			// 
			// grpParticlesToEmit
			// 
			this.grpParticlesToEmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.grpParticlesToEmit.Controls.Add(this.btnDuplicate);
			this.grpParticlesToEmit.Controls.Add(this.btnRemoveParticle);
			this.grpParticlesToEmit.Controls.Add(this.btnAddParticle);
			this.grpParticlesToEmit.Controls.Add(this.lstParticlesToEmit);
			this.grpParticlesToEmit.Location = new System.Drawing.Point(12, 105);
			this.grpParticlesToEmit.Name = "grpParticlesToEmit";
			this.grpParticlesToEmit.Size = new System.Drawing.Size(213, 436);
			this.grpParticlesToEmit.TabIndex = 1;
			this.grpParticlesToEmit.TabStop = false;
			this.grpParticlesToEmit.Text = "Particles to Emit";
			// 
			// btnDuplicate
			// 
			this.btnDuplicate.Location = new System.Drawing.Point(6, 335);
			this.btnDuplicate.Name = "btnDuplicate";
			this.btnDuplicate.Size = new System.Drawing.Size(201, 27);
			this.btnDuplicate.TabIndex = 3;
			this.btnDuplicate.Text = "Duplicate Particle";
			this.btnDuplicate.UseVisualStyleBackColor = true;
			this.btnDuplicate.Click += new System.EventHandler(this.btnDuplicate_Click);
			// 
			// numParticleCount
			// 
			this.numParticleCount.Location = new System.Drawing.Point(133, 51);
			this.numParticleCount.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.numParticleCount.Name = "numParticleCount";
			this.numParticleCount.Size = new System.Drawing.Size(102, 20);
			this.numParticleCount.TabIndex = 0;
			this.numParticleCount.ValueChanged += new System.EventHandler(this.numEmissionCount_ValueChanged);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(3, 53);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(75, 13);
			this.label5.TabIndex = 8;
			this.label5.Text = "Particle count:";
			// 
			// chkParticleEnabled
			// 
			this.chkParticleEnabled.AutoSize = true;
			this.chkParticleEnabled.Location = new System.Drawing.Point(6, 127);
			this.chkParticleEnabled.Name = "chkParticleEnabled";
			this.chkParticleEnabled.Size = new System.Drawing.Size(115, 17);
			this.chkParticleEnabled.TabIndex = 11;
			this.chkParticleEnabled.Text = "Enable this particle";
			this.chkParticleEnabled.UseVisualStyleBackColor = true;
			this.chkParticleEnabled.CheckedChanged += new System.EventHandler(this.chkParticleEnabled_CheckedChanged);
			// 
			// chkParticleSolo
			// 
			this.chkParticleSolo.AutoSize = true;
			this.chkParticleSolo.Location = new System.Drawing.Point(6, 150);
			this.chkParticleSolo.Name = "chkParticleSolo";
			this.chkParticleSolo.Size = new System.Drawing.Size(165, 17);
			this.chkParticleSolo.TabIndex = 12;
			this.chkParticleSolo.Text = "Enable only this particle (solo)";
			this.chkParticleSolo.UseVisualStyleBackColor = true;
			this.chkParticleSolo.CheckedChanged += new System.EventHandler(this.chkParticleSolo_CheckedChanged);
			// 
			// numReEmitInterval
			// 
			this.numReEmitInterval.Location = new System.Drawing.Point(133, 103);
			this.numReEmitInterval.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.numReEmitInterval.Name = "numReEmitInterval";
			this.numReEmitInterval.Size = new System.Drawing.Size(102, 20);
			this.numReEmitInterval.TabIndex = 2;
			this.numReEmitInterval.ValueChanged += new System.EventHandler(this.numReEmitInterval_ValueChanged);
			// 
			// txtParticleToEmit
			// 
			this.txtParticleToEmit.Location = new System.Drawing.Point(6, 22);
			this.txtParticleToEmit.Name = "txtParticleToEmit";
			this.txtParticleToEmit.ReadOnly = true;
			this.txtParticleToEmit.Size = new System.Drawing.Size(229, 20);
			this.txtParticleToEmit.TabIndex = 14;
			// 
			// grpParticleGroupProperties
			// 
			this.grpParticleGroupProperties.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.grpParticleGroupProperties.Controls.Add(this.numInitialDelay);
			this.grpParticleGroupProperties.Controls.Add(this.label2);
			this.grpParticleGroupProperties.Controls.Add(this.chkReEmit);
			this.grpParticleGroupProperties.Controls.Add(this.numReEmitInterval);
			this.grpParticleGroupProperties.Controls.Add(this.chkAdditiveBlending);
			this.grpParticleGroupProperties.Controls.Add(this.chkSpawnCenter);
			this.grpParticleGroupProperties.Controls.Add(this.grpChildEmitters);
			this.grpParticleGroupProperties.Controls.Add(this.txtParticleToEmit);
			this.grpParticleGroupProperties.Controls.Add(this.grpParticleLifespan);
			this.grpParticleGroupProperties.Controls.Add(this.chkParticleSolo);
			this.grpParticleGroupProperties.Controls.Add(this.chkParticleEnabled);
			this.grpParticleGroupProperties.Controls.Add(this.label5);
			this.grpParticleGroupProperties.Controls.Add(this.numParticleCount);
			this.grpParticleGroupProperties.Location = new System.Drawing.Point(229, 28);
			this.grpParticleGroupProperties.Name = "grpParticleGroupProperties";
			this.grpParticleGroupProperties.Size = new System.Drawing.Size(241, 513);
			this.grpParticleGroupProperties.TabIndex = 2;
			this.grpParticleGroupProperties.TabStop = false;
			this.grpParticleGroupProperties.Text = "Particle Emission";
			// 
			// numInitialDelay
			// 
			this.numInitialDelay.Location = new System.Drawing.Point(133, 77);
			this.numInitialDelay.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.numInitialDelay.Name = "numInitialDelay";
			this.numInitialDelay.Size = new System.Drawing.Size(102, 20);
			this.numInitialDelay.TabIndex = 20;
			this.numInitialDelay.ValueChanged += new System.EventHandler(this.numInitialDelay_ValueChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(2, 79);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(84, 13);
			this.label2.TabIndex = 19;
			this.label2.Text = "Initial delay (ms):";
			// 
			// chkReEmit
			// 
			this.chkReEmit.AutoSize = true;
			this.chkReEmit.Location = new System.Drawing.Point(6, 104);
			this.chkReEmit.Name = "chkReEmit";
			this.chkReEmit.Size = new System.Drawing.Size(116, 17);
			this.chkReEmit.TabIndex = 18;
			this.chkReEmit.Text = "Re-emit every (ms):";
			this.chkReEmit.UseVisualStyleBackColor = true;
			this.chkReEmit.CheckedChanged += new System.EventHandler(this.chkReEmit_CheckedChanged);
			// 
			// chkAdditiveBlending
			// 
			this.chkAdditiveBlending.AutoSize = true;
			this.chkAdditiveBlending.Location = new System.Drawing.Point(6, 196);
			this.chkAdditiveBlending.Name = "chkAdditiveBlending";
			this.chkAdditiveBlending.Size = new System.Drawing.Size(169, 17);
			this.chkAdditiveBlending.TabIndex = 17;
			this.chkAdditiveBlending.Text = "Particle uses additive blending";
			this.chkAdditiveBlending.UseVisualStyleBackColor = true;
			this.chkAdditiveBlending.CheckedChanged += new System.EventHandler(this.chkAdditiveBlending_CheckedChanged);
			// 
			// chkSpawnCenter
			// 
			this.chkSpawnCenter.AutoSize = true;
			this.chkSpawnCenter.Location = new System.Drawing.Point(6, 173);
			this.chkSpawnCenter.Name = "chkSpawnCenter";
			this.chkSpawnCenter.Size = new System.Drawing.Size(207, 17);
			this.chkSpawnCenter.TabIndex = 16;
			this.chkSpawnCenter.Text = "Spawn at emitter center (ignore radius)";
			this.chkSpawnCenter.UseVisualStyleBackColor = true;
			this.chkSpawnCenter.CheckedChanged += new System.EventHandler(this.chkSpawnCenter_CheckedChanged);
			// 
			// grpChildEmitters
			// 
			this.grpChildEmitters.Controls.Add(this.btnRemoveChild);
			this.grpChildEmitters.Controls.Add(this.btnAddChild);
			this.grpChildEmitters.Controls.Add(this.lstChildEmitters);
			this.grpChildEmitters.Location = new System.Drawing.Point(6, 316);
			this.grpChildEmitters.Name = "grpChildEmitters";
			this.grpChildEmitters.Size = new System.Drawing.Size(229, 191);
			this.grpChildEmitters.TabIndex = 15;
			this.grpChildEmitters.TabStop = false;
			this.grpChildEmitters.Text = "Child Emitters";
			// 
			// btnRemoveChild
			// 
			this.btnRemoveChild.Location = new System.Drawing.Point(6, 153);
			this.btnRemoveChild.Name = "btnRemoveChild";
			this.btnRemoveChild.Size = new System.Drawing.Size(217, 27);
			this.btnRemoveChild.TabIndex = 6;
			this.btnRemoveChild.Text = "Remove Selected Child";
			this.btnRemoveChild.UseVisualStyleBackColor = true;
			this.btnRemoveChild.Click += new System.EventHandler(this.btnRemoveChild_Click);
			// 
			// btnAddChild
			// 
			this.btnAddChild.Location = new System.Drawing.Point(6, 120);
			this.btnAddChild.Name = "btnAddChild";
			this.btnAddChild.Size = new System.Drawing.Size(217, 27);
			this.btnAddChild.TabIndex = 4;
			this.btnAddChild.Text = "Add Child";
			this.btnAddChild.UseVisualStyleBackColor = true;
			this.btnAddChild.Click += new System.EventHandler(this.btnAddChild_Click);
			// 
			// lstChildEmitters
			// 
			this.lstChildEmitters.FormattingEnabled = true;
			this.lstChildEmitters.Location = new System.Drawing.Point(6, 19);
			this.lstChildEmitters.Name = "lstChildEmitters";
			this.lstChildEmitters.Size = new System.Drawing.Size(217, 95);
			this.lstChildEmitters.TabIndex = 0;
			// 
			// grpParticleLifespan
			// 
			this.grpParticleLifespan.Controls.Add(this.chkAffectsLifespan);
			this.grpParticleLifespan.Controls.Add(this.chkAffectsVelocity);
			this.grpParticleLifespan.Controls.Add(this.chkAffectsSize);
			this.grpParticleLifespan.Location = new System.Drawing.Point(6, 219);
			this.grpParticleLifespan.Name = "grpParticleLifespan";
			this.grpParticleLifespan.Size = new System.Drawing.Size(229, 91);
			this.grpParticleLifespan.TabIndex = 13;
			this.grpParticleLifespan.TabStop = false;
			this.grpParticleLifespan.Text = "Lifespan Control";
			// 
			// chkAffectsLifespan
			// 
			this.chkAffectsLifespan.AutoSize = true;
			this.chkAffectsLifespan.Location = new System.Drawing.Point(6, 65);
			this.chkAffectsLifespan.Name = "chkAffectsLifespan";
			this.chkAffectsLifespan.Size = new System.Drawing.Size(169, 17);
			this.chkAffectsLifespan.TabIndex = 2;
			this.chkAffectsLifespan.Text = "Life decreases over emitter life";
			this.chkAffectsLifespan.UseVisualStyleBackColor = true;
			this.chkAffectsLifespan.CheckedChanged += new System.EventHandler(this.chkAffectsLifespan_CheckedChanged);
			// 
			// chkAffectsVelocity
			// 
			this.chkAffectsVelocity.AutoSize = true;
			this.chkAffectsVelocity.Location = new System.Drawing.Point(6, 42);
			this.chkAffectsVelocity.Name = "chkAffectsVelocity";
			this.chkAffectsVelocity.Size = new System.Drawing.Size(189, 17);
			this.chkAffectsVelocity.TabIndex = 1;
			this.chkAffectsVelocity.Text = "Velocity decreases over emitter life";
			this.chkAffectsVelocity.UseVisualStyleBackColor = true;
			this.chkAffectsVelocity.CheckedChanged += new System.EventHandler(this.chkAffectsVelocity_CheckedChanged);
			// 
			// chkAffectsSize
			// 
			this.chkAffectsSize.AutoSize = true;
			this.chkAffectsSize.Location = new System.Drawing.Point(6, 19);
			this.chkAffectsSize.Name = "chkAffectsSize";
			this.chkAffectsSize.Size = new System.Drawing.Size(172, 17);
			this.chkAffectsSize.TabIndex = 0;
			this.chkAffectsSize.Text = "Size decreases over emitter life";
			this.chkAffectsSize.UseVisualStyleBackColor = true;
			this.chkAffectsSize.CheckedChanged += new System.EventHandler(this.chkAffectsSize_CheckedChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 22);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(49, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Life (ms):";
			// 
			// numEmitterLife
			// 
			this.numEmitterLife.Location = new System.Drawing.Point(124, 20);
			this.numEmitterLife.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.numEmitterLife.Name = "numEmitterLife";
			this.numEmitterLife.Size = new System.Drawing.Size(81, 20);
			this.numEmitterLife.TabIndex = 1;
			this.numEmitterLife.ThousandsSeparator = true;
			this.numEmitterLife.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numEmitterLife.ValueChanged += new System.EventHandler(this.numEmitterLife_ValueChanged);
			// 
			// numEmitterRadius
			// 
			this.numEmitterRadius.Location = new System.Drawing.Point(124, 46);
			this.numEmitterRadius.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
			this.numEmitterRadius.Name = "numEmitterRadius";
			this.numEmitterRadius.Size = new System.Drawing.Size(81, 20);
			this.numEmitterRadius.TabIndex = 4;
			this.numEmitterRadius.ThousandsSeparator = true;
			this.numEmitterRadius.ValueChanged += new System.EventHandler(this.numEmitterRadius_ValueChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 49);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(60, 13);
			this.label3.TabIndex = 5;
			this.label3.Text = "Radius (m):";
			// 
			// grpEmitterProperties
			// 
			this.grpEmitterProperties.Controls.Add(this.label3);
			this.grpEmitterProperties.Controls.Add(this.numEmitterRadius);
			this.grpEmitterProperties.Controls.Add(this.numEmitterLife);
			this.grpEmitterProperties.Controls.Add(this.label1);
			this.grpEmitterProperties.Location = new System.Drawing.Point(12, 28);
			this.grpEmitterProperties.Name = "grpEmitterProperties";
			this.grpEmitterProperties.Size = new System.Drawing.Size(211, 71);
			this.grpEmitterProperties.TabIndex = 0;
			this.grpEmitterProperties.TabStop = false;
			this.grpEmitterProperties.Text = "Emitter Properties";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(6, 22);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(52, 13);
			this.label6.TabIndex = 0;
			this.label6.Text = "Filename:";
			// 
			// txtParticleFilename
			// 
			this.txtParticleFilename.Location = new System.Drawing.Point(64, 19);
			this.txtParticleFilename.Name = "txtParticleFilename";
			this.txtParticleFilename.ReadOnly = true;
			this.txtParticleFilename.Size = new System.Drawing.Size(387, 20);
			this.txtParticleFilename.TabIndex = 1;
			// 
			// txtTexture
			// 
			this.txtTexture.Location = new System.Drawing.Point(64, 46);
			this.txtTexture.Name = "txtTexture";
			this.txtTexture.Size = new System.Drawing.Size(317, 20);
			this.txtTexture.TabIndex = 2;
			this.txtTexture.TextChanged += new System.EventHandler(this.txtTexture_TextChanged);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(6, 48);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(46, 13);
			this.label7.TabIndex = 3;
			this.label7.Text = "Texture:";
			// 
			// grpColor
			// 
			this.grpColor.Controls.Add(this.mtsColor);
			this.grpColor.Controls.Add(this.btnColor);
			this.grpColor.Controls.Add(this.btnRemoveColor);
			this.grpColor.Controls.Add(this.btnAddColor);
			this.grpColor.Location = new System.Drawing.Point(6, 136);
			this.grpColor.Name = "grpColor";
			this.grpColor.Size = new System.Drawing.Size(445, 61);
			this.grpColor.TabIndex = 16;
			this.grpColor.TabStop = false;
			this.grpColor.Text = "Color";
			// 
			// mtsColor
			// 
			this.mtsColor.BackColor = System.Drawing.Color.Transparent;
			this.mtsColor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mtsColor.BackgroundImage")));
			this.mtsColor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.mtsColor.Location = new System.Drawing.Point(6, 19);
			this.mtsColor.Name = "mtsColor";
			this.mtsColor.Size = new System.Drawing.Size(280, 30);
			this.mtsColor.TabIndex = 16;
			this.mtsColor.SelectionChanged += new MultiThumbSlider.MultiThumbSliderSelectionEventHandler(this.mtsColor_SelectionChanged);
			this.mtsColor.ThumbSlide += new MultiThumbSlider.MultiThumbSliderSlideEventHandler(this.mtsColor_ThumbSlide);
			// 
			// btnColor
			// 
			this.btnColor.BackColor = System.Drawing.Color.White;
			this.btnColor.Location = new System.Drawing.Point(350, 19);
			this.btnColor.Name = "btnColor";
			this.btnColor.Size = new System.Drawing.Size(89, 23);
			this.btnColor.TabIndex = 11;
			this.btnColor.UseVisualStyleBackColor = false;
			this.btnColor.Click += new System.EventHandler(this.btnColor_Click);
			// 
			// btnRemoveColor
			// 
			this.btnRemoveColor.AutoSize = true;
			this.btnRemoveColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnRemoveColor.Location = new System.Drawing.Point(321, 19);
			this.btnRemoveColor.Name = "btnRemoveColor";
			this.btnRemoveColor.Size = new System.Drawing.Size(23, 23);
			this.btnRemoveColor.TabIndex = 15;
			this.btnRemoveColor.Text = "-";
			this.btnRemoveColor.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnRemoveColor.UseVisualStyleBackColor = true;
			this.btnRemoveColor.Click += new System.EventHandler(this.btnRemoveColor_Click);
			// 
			// btnAddColor
			// 
			this.btnAddColor.AutoSize = true;
			this.btnAddColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnAddColor.Location = new System.Drawing.Point(292, 19);
			this.btnAddColor.Name = "btnAddColor";
			this.btnAddColor.Size = new System.Drawing.Size(24, 23);
			this.btnAddColor.TabIndex = 14;
			this.btnAddColor.Text = "+";
			this.btnAddColor.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnAddColor.UseVisualStyleBackColor = true;
			this.btnAddColor.Click += new System.EventHandler(this.btnAddColor_Click);
			// 
			// grpAlpha
			// 
			this.grpAlpha.Controls.Add(this.mtsAlpha);
			this.grpAlpha.Controls.Add(this.btnAlpha);
			this.grpAlpha.Controls.Add(this.btnRemoveAlpha);
			this.grpAlpha.Controls.Add(this.btnAddAlpha);
			this.grpAlpha.Location = new System.Drawing.Point(6, 203);
			this.grpAlpha.Name = "grpAlpha";
			this.grpAlpha.Size = new System.Drawing.Size(445, 61);
			this.grpAlpha.TabIndex = 17;
			this.grpAlpha.TabStop = false;
			this.grpAlpha.Text = "Alpha";
			// 
			// mtsAlpha
			// 
			this.mtsAlpha.BackColor = System.Drawing.Color.White;
			this.mtsAlpha.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mtsAlpha.BackgroundImage")));
			this.mtsAlpha.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.mtsAlpha.Location = new System.Drawing.Point(6, 19);
			this.mtsAlpha.Name = "mtsAlpha";
			this.mtsAlpha.Size = new System.Drawing.Size(280, 36);
			this.mtsAlpha.TabIndex = 17;
			this.mtsAlpha.SelectionChanged += new MultiThumbSlider.MultiThumbSliderSelectionEventHandler(this.mtsAlpha_SelectionChanged);
			this.mtsAlpha.ThumbSlide += new MultiThumbSlider.MultiThumbSliderSlideEventHandler(this.mtsAlpha_ThumbSlide);
			// 
			// btnAlpha
			// 
			this.btnAlpha.BackColor = System.Drawing.Color.White;
			this.btnAlpha.Location = new System.Drawing.Point(350, 19);
			this.btnAlpha.Name = "btnAlpha";
			this.btnAlpha.Size = new System.Drawing.Size(89, 23);
			this.btnAlpha.TabIndex = 11;
			this.btnAlpha.UseVisualStyleBackColor = false;
			this.btnAlpha.Click += new System.EventHandler(this.btnAlpha_Click);
			// 
			// btnRemoveAlpha
			// 
			this.btnRemoveAlpha.AutoSize = true;
			this.btnRemoveAlpha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnRemoveAlpha.Location = new System.Drawing.Point(321, 19);
			this.btnRemoveAlpha.Name = "btnRemoveAlpha";
			this.btnRemoveAlpha.Size = new System.Drawing.Size(23, 23);
			this.btnRemoveAlpha.TabIndex = 15;
			this.btnRemoveAlpha.Text = "-";
			this.btnRemoveAlpha.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnRemoveAlpha.UseVisualStyleBackColor = true;
			this.btnRemoveAlpha.Click += new System.EventHandler(this.btnRemoveAlpha_Click);
			// 
			// btnAddAlpha
			// 
			this.btnAddAlpha.AutoSize = true;
			this.btnAddAlpha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnAddAlpha.Location = new System.Drawing.Point(292, 19);
			this.btnAddAlpha.Name = "btnAddAlpha";
			this.btnAddAlpha.Size = new System.Drawing.Size(24, 23);
			this.btnAddAlpha.TabIndex = 14;
			this.btnAddAlpha.Text = "+";
			this.btnAddAlpha.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnAddAlpha.UseVisualStyleBackColor = true;
			this.btnAddAlpha.Click += new System.EventHandler(this.btnAddAlpha_Click);
			// 
			// grpVelocity
			// 
			this.grpVelocity.Controls.Add(this.mtsVelocity);
			this.grpVelocity.Controls.Add(this.numVelocityVariance);
			this.grpVelocity.Controls.Add(this.label9);
			this.grpVelocity.Controls.Add(this.numVelocityValue);
			this.grpVelocity.Controls.Add(this.btnRemoveVelocity);
			this.grpVelocity.Controls.Add(this.btnAddVelocity);
			this.grpVelocity.Location = new System.Drawing.Point(6, 270);
			this.grpVelocity.Name = "grpVelocity";
			this.grpVelocity.Size = new System.Drawing.Size(445, 75);
			this.grpVelocity.TabIndex = 18;
			this.grpVelocity.TabStop = false;
			this.grpVelocity.Text = "Velocity";
			// 
			// mtsVelocity
			// 
			this.mtsVelocity.BackColor = System.Drawing.Color.Transparent;
			this.mtsVelocity.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mtsVelocity.BackgroundImage")));
			this.mtsVelocity.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.mtsVelocity.Location = new System.Drawing.Point(6, 19);
			this.mtsVelocity.Name = "mtsVelocity";
			this.mtsVelocity.Size = new System.Drawing.Size(280, 50);
			this.mtsVelocity.TabIndex = 18;
			this.mtsVelocity.SelectionChanged += new MultiThumbSlider.MultiThumbSliderSelectionEventHandler(this.mtsVelocity_SelectionChanged);
			this.mtsVelocity.ThumbSlide += new MultiThumbSlider.MultiThumbSliderSlideEventHandler(this.mtsVelocity_ThumbSlide);
			// 
			// numVelocityVariance
			// 
			this.numVelocityVariance.Location = new System.Drawing.Point(361, 46);
			this.numVelocityVariance.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
			this.numVelocityVariance.Name = "numVelocityVariance";
			this.numVelocityVariance.Size = new System.Drawing.Size(78, 20);
			this.numVelocityVariance.TabIndex = 18;
			this.numVelocityVariance.ValueChanged += new System.EventHandler(this.numVelocityVariance_ValueChanged);
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(292, 49);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(63, 13);
			this.label9.TabIndex = 17;
			this.label9.Text = "Variance %:";
			// 
			// numVelocityValue
			// 
			this.numVelocityValue.Location = new System.Drawing.Point(361, 20);
			this.numVelocityValue.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
			this.numVelocityValue.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
			this.numVelocityValue.Name = "numVelocityValue";
			this.numVelocityValue.Size = new System.Drawing.Size(78, 20);
			this.numVelocityValue.TabIndex = 16;
			this.numVelocityValue.ValueChanged += new System.EventHandler(this.numVelocityValue_ValueChanged);
			// 
			// btnRemoveVelocity
			// 
			this.btnRemoveVelocity.AutoSize = true;
			this.btnRemoveVelocity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnRemoveVelocity.Location = new System.Drawing.Point(321, 19);
			this.btnRemoveVelocity.Name = "btnRemoveVelocity";
			this.btnRemoveVelocity.Size = new System.Drawing.Size(23, 23);
			this.btnRemoveVelocity.TabIndex = 15;
			this.btnRemoveVelocity.Text = "-";
			this.btnRemoveVelocity.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnRemoveVelocity.UseVisualStyleBackColor = true;
			this.btnRemoveVelocity.Click += new System.EventHandler(this.btnRemoveVelocity_Click);
			// 
			// btnAddVelocity
			// 
			this.btnAddVelocity.AutoSize = true;
			this.btnAddVelocity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnAddVelocity.Location = new System.Drawing.Point(292, 19);
			this.btnAddVelocity.Name = "btnAddVelocity";
			this.btnAddVelocity.Size = new System.Drawing.Size(24, 23);
			this.btnAddVelocity.TabIndex = 14;
			this.btnAddVelocity.Text = "+";
			this.btnAddVelocity.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnAddVelocity.UseVisualStyleBackColor = true;
			this.btnAddVelocity.Click += new System.EventHandler(this.btnAddVelocity_Click);
			// 
			// grpSize
			// 
			this.grpSize.Controls.Add(this.mtsSize);
			this.grpSize.Controls.Add(this.numSizeVariance);
			this.grpSize.Controls.Add(this.label10);
			this.grpSize.Controls.Add(this.numSizeValue);
			this.grpSize.Controls.Add(this.btnRemoveSize);
			this.grpSize.Controls.Add(this.btnAddSize);
			this.grpSize.Location = new System.Drawing.Point(6, 351);
			this.grpSize.Name = "grpSize";
			this.grpSize.Size = new System.Drawing.Size(445, 75);
			this.grpSize.TabIndex = 19;
			this.grpSize.TabStop = false;
			this.grpSize.Text = "Size";
			// 
			// mtsSize
			// 
			this.mtsSize.BackColor = System.Drawing.Color.Transparent;
			this.mtsSize.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mtsSize.BackgroundImage")));
			this.mtsSize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.mtsSize.Location = new System.Drawing.Point(6, 19);
			this.mtsSize.Name = "mtsSize";
			this.mtsSize.Size = new System.Drawing.Size(280, 50);
			this.mtsSize.TabIndex = 19;
			this.mtsSize.SelectionChanged += new MultiThumbSlider.MultiThumbSliderSelectionEventHandler(this.mtsSize_SelectionChanged);
			this.mtsSize.ThumbSlide += new MultiThumbSlider.MultiThumbSliderSlideEventHandler(this.mtsSize_ThumbSlide);
			// 
			// numSizeVariance
			// 
			this.numSizeVariance.Location = new System.Drawing.Point(361, 46);
			this.numSizeVariance.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
			this.numSizeVariance.Name = "numSizeVariance";
			this.numSizeVariance.Size = new System.Drawing.Size(78, 20);
			this.numSizeVariance.TabIndex = 18;
			this.numSizeVariance.ValueChanged += new System.EventHandler(this.numSizeVariance_ValueChanged);
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(292, 49);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(63, 13);
			this.label10.TabIndex = 17;
			this.label10.Text = "Variance %:";
			// 
			// numSizeValue
			// 
			this.numSizeValue.Location = new System.Drawing.Point(361, 20);
			this.numSizeValue.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
			this.numSizeValue.Name = "numSizeValue";
			this.numSizeValue.Size = new System.Drawing.Size(78, 20);
			this.numSizeValue.TabIndex = 16;
			this.numSizeValue.ValueChanged += new System.EventHandler(this.numSizeValue_ValueChanged);
			// 
			// btnRemoveSize
			// 
			this.btnRemoveSize.AutoSize = true;
			this.btnRemoveSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnRemoveSize.Location = new System.Drawing.Point(321, 19);
			this.btnRemoveSize.Name = "btnRemoveSize";
			this.btnRemoveSize.Size = new System.Drawing.Size(23, 23);
			this.btnRemoveSize.TabIndex = 15;
			this.btnRemoveSize.Text = "-";
			this.btnRemoveSize.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnRemoveSize.UseVisualStyleBackColor = true;
			this.btnRemoveSize.Click += new System.EventHandler(this.btnRemoveSize_Click);
			// 
			// btnAddSize
			// 
			this.btnAddSize.AutoSize = true;
			this.btnAddSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnAddSize.Location = new System.Drawing.Point(292, 19);
			this.btnAddSize.Name = "btnAddSize";
			this.btnAddSize.Size = new System.Drawing.Size(24, 23);
			this.btnAddSize.TabIndex = 14;
			this.btnAddSize.Text = "+";
			this.btnAddSize.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnAddSize.UseVisualStyleBackColor = true;
			this.btnAddSize.Click += new System.EventHandler(this.btnAddSize_Click);
			// 
			// grpSpin
			// 
			this.grpSpin.Controls.Add(this.mtsSpin);
			this.grpSpin.Controls.Add(this.numSpinVariance);
			this.grpSpin.Controls.Add(this.label8);
			this.grpSpin.Controls.Add(this.numSpinValue);
			this.grpSpin.Controls.Add(this.btnRemoveSpin);
			this.grpSpin.Controls.Add(this.btnAddSpin);
			this.grpSpin.Location = new System.Drawing.Point(6, 432);
			this.grpSpin.Name = "grpSpin";
			this.grpSpin.Size = new System.Drawing.Size(445, 75);
			this.grpSpin.TabIndex = 20;
			this.grpSpin.TabStop = false;
			this.grpSpin.Text = "Spin";
			// 
			// mtsSpin
			// 
			this.mtsSpin.BackColor = System.Drawing.Color.Transparent;
			this.mtsSpin.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mtsSpin.BackgroundImage")));
			this.mtsSpin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.mtsSpin.Location = new System.Drawing.Point(6, 19);
			this.mtsSpin.Name = "mtsSpin";
			this.mtsSpin.Size = new System.Drawing.Size(280, 43);
			this.mtsSpin.TabIndex = 20;
			this.mtsSpin.SelectionChanged += new MultiThumbSlider.MultiThumbSliderSelectionEventHandler(this.mtsSpin_SelectionChanged);
			this.mtsSpin.ThumbSlide += new MultiThumbSlider.MultiThumbSliderSlideEventHandler(this.mtsSpin_ThumbSlide);
			// 
			// numSpinVariance
			// 
			this.numSpinVariance.Location = new System.Drawing.Point(361, 46);
			this.numSpinVariance.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
			this.numSpinVariance.Name = "numSpinVariance";
			this.numSpinVariance.Size = new System.Drawing.Size(78, 20);
			this.numSpinVariance.TabIndex = 18;
			this.numSpinVariance.ValueChanged += new System.EventHandler(this.numSpinVariance_ValueChanged);
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(292, 49);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(63, 13);
			this.label8.TabIndex = 17;
			this.label8.Text = "Variance %:";
			// 
			// numSpinValue
			// 
			this.numSpinValue.Location = new System.Drawing.Point(361, 20);
			this.numSpinValue.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
			this.numSpinValue.Name = "numSpinValue";
			this.numSpinValue.Size = new System.Drawing.Size(78, 20);
			this.numSpinValue.TabIndex = 16;
			this.numSpinValue.ValueChanged += new System.EventHandler(this.numSpinValue_ValueChanged);
			// 
			// btnRemoveSpin
			// 
			this.btnRemoveSpin.AutoSize = true;
			this.btnRemoveSpin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnRemoveSpin.Location = new System.Drawing.Point(321, 19);
			this.btnRemoveSpin.Name = "btnRemoveSpin";
			this.btnRemoveSpin.Size = new System.Drawing.Size(23, 23);
			this.btnRemoveSpin.TabIndex = 15;
			this.btnRemoveSpin.Text = "-";
			this.btnRemoveSpin.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnRemoveSpin.UseVisualStyleBackColor = true;
			this.btnRemoveSpin.Click += new System.EventHandler(this.btnRemoveSpin_Click);
			// 
			// btnAddSpin
			// 
			this.btnAddSpin.AutoSize = true;
			this.btnAddSpin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnAddSpin.Location = new System.Drawing.Point(292, 19);
			this.btnAddSpin.Name = "btnAddSpin";
			this.btnAddSpin.Size = new System.Drawing.Size(24, 23);
			this.btnAddSpin.TabIndex = 14;
			this.btnAddSpin.Text = "+";
			this.btnAddSpin.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnAddSpin.UseVisualStyleBackColor = true;
			this.btnAddSpin.Click += new System.EventHandler(this.btnAddSpin_Click);
			// 
			// btnShowTexture
			// 
			this.btnShowTexture.Location = new System.Drawing.Point(387, 45);
			this.btnShowTexture.Name = "btnShowTexture";
			this.btnShowTexture.Size = new System.Drawing.Size(64, 22);
			this.btnShowTexture.TabIndex = 22;
			this.btnShowTexture.Text = "Show";
			this.btnShowTexture.UseVisualStyleBackColor = true;
			this.btnShowTexture.Click += new System.EventHandler(this.btnShowTexture_Click);
			// 
			// grpProperties
			// 
			this.grpProperties.Controls.Add(this.btnScaleKeyframeValues);
			this.grpProperties.Controls.Add(this.numParticleLifeVariance);
			this.grpProperties.Controls.Add(this.label12);
			this.grpProperties.Controls.Add(this.numParticleLife);
			this.grpProperties.Controls.Add(this.label11);
			this.grpProperties.Controls.Add(this.btnShowTexture);
			this.grpProperties.Controls.Add(this.picTexture);
			this.grpProperties.Controls.Add(this.grpSpin);
			this.grpProperties.Controls.Add(this.grpSize);
			this.grpProperties.Controls.Add(this.grpVelocity);
			this.grpProperties.Controls.Add(this.grpAlpha);
			this.grpProperties.Controls.Add(this.grpColor);
			this.grpProperties.Controls.Add(this.label7);
			this.grpProperties.Controls.Add(this.txtTexture);
			this.grpProperties.Controls.Add(this.txtParticleFilename);
			this.grpProperties.Controls.Add(this.label6);
			this.grpProperties.Location = new System.Drawing.Point(476, 28);
			this.grpProperties.Name = "grpProperties";
			this.grpProperties.Size = new System.Drawing.Size(457, 513);
			this.grpProperties.TabIndex = 0;
			this.grpProperties.TabStop = false;
			this.grpProperties.Text = "Particle Properties";
			// 
			// numParticleLifeVariance
			// 
			this.numParticleLifeVariance.Location = new System.Drawing.Point(100, 98);
			this.numParticleLifeVariance.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
			this.numParticleLifeVariance.Name = "numParticleLifeVariance";
			this.numParticleLifeVariance.Size = new System.Drawing.Size(81, 20);
			this.numParticleLifeVariance.TabIndex = 8;
			this.numParticleLifeVariance.ThousandsSeparator = true;
			this.numParticleLifeVariance.ValueChanged += new System.EventHandler(this.numParticleLifeVariance_ValueChanged);
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(6, 100);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(88, 13);
			this.label12.TabIndex = 8;
			this.label12.Text = "Life variance (%):";
			// 
			// numParticleLife
			// 
			this.numParticleLife.Location = new System.Drawing.Point(100, 72);
			this.numParticleLife.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.numParticleLife.Name = "numParticleLife";
			this.numParticleLife.Size = new System.Drawing.Size(81, 20);
			this.numParticleLife.TabIndex = 8;
			this.numParticleLife.ThousandsSeparator = true;
			this.numParticleLife.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numParticleLife.ValueChanged += new System.EventHandler(this.numParticleLife_ValueChanged);
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(6, 74);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(49, 13);
			this.label11.TabIndex = 8;
			this.label11.Text = "Life (ms):";
			// 
			// picTexture
			// 
			this.picTexture.BackColor = System.Drawing.Color.Black;
			this.picTexture.Location = new System.Drawing.Point(387, 72);
			this.picTexture.Name = "picTexture";
			this.picTexture.Size = new System.Drawing.Size(64, 65);
			this.picTexture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.picTexture.TabIndex = 21;
			this.picTexture.TabStop = false;
			// 
			// menuStrip1
			// 
			this.menuStrip1.AutoSize = false;
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(945, 10);
			this.menuStrip1.TabIndex = 4;
			this.menuStrip1.Text = "menuStrip1";
			this.menuStrip1.Visible = false;
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.toolStripMenuItem1,
            this.openToolStripMenuItem,
            this.toolStripMenuItem2,
            this.saveEmitterAsToolStripMenuItem,
            this.saveAllToolStripMenuItem,
            this.toolStripMenuItem3,
            this.quitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 6);
			this.fileToolStripMenuItem.Text = "&File";
			// 
			// newToolStripMenuItem
			// 
			this.newToolStripMenuItem.Name = "newToolStripMenuItem";
			this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
			this.newToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
			this.newToolStripMenuItem.Text = "&New";
			this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(233, 6);
			// 
			// openToolStripMenuItem
			// 
			this.openToolStripMenuItem.Name = "openToolStripMenuItem";
			this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
			this.openToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
			this.openToolStripMenuItem.Text = "&Open";
			this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(233, 6);
			// 
			// saveEmitterAsToolStripMenuItem
			// 
			this.saveEmitterAsToolStripMenuItem.Name = "saveEmitterAsToolStripMenuItem";
			this.saveEmitterAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
			this.saveEmitterAsToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
			this.saveEmitterAsToolStripMenuItem.Text = "Save &Emitter As...";
			this.saveEmitterAsToolStripMenuItem.Click += new System.EventHandler(this.saveEmitterAsToolStripMenuItem_Click);
			// 
			// saveAllToolStripMenuItem
			// 
			this.saveAllToolStripMenuItem.Name = "saveAllToolStripMenuItem";
			this.saveAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.saveAllToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
			this.saveAllToolStripMenuItem.Text = "Save &All";
			this.saveAllToolStripMenuItem.Click += new System.EventHandler(this.saveAllToolStripMenuItem_Click);
			// 
			// toolStripMenuItem3
			// 
			this.toolStripMenuItem3.Name = "toolStripMenuItem3";
			this.toolStripMenuItem3.Size = new System.Drawing.Size(233, 6);
			// 
			// quitToolStripMenuItem
			// 
			this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
			this.quitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
			this.quitToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
			this.quitToolStripMenuItem.Text = "&Quit";
			this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
			// 
			// btnScaleKeyframeValues
			// 
			this.btnScaleKeyframeValues.Location = new System.Drawing.Point(234, 72);
			this.btnScaleKeyframeValues.Name = "btnScaleKeyframeValues";
			this.btnScaleKeyframeValues.Size = new System.Drawing.Size(147, 23);
			this.btnScaleKeyframeValues.TabIndex = 23;
			this.btnScaleKeyframeValues.Text = "Scale Keyframe Values";
			this.btnScaleKeyframeValues.UseVisualStyleBackColor = true;
			this.btnScaleKeyframeValues.Click += new System.EventHandler(this.btnScaleKeyframeValues_Click);
			// 
			// FornaxEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.ClientSize = new System.Drawing.Size(945, 551);
			this.Controls.Add(this.grpProperties);
			this.Controls.Add(this.grpParticleGroupProperties);
			this.Controls.Add(this.grpParticlesToEmit);
			this.Controls.Add(this.toolStrip1);
			this.Controls.Add(this.grpEmitterProperties);
			this.Controls.Add(this.menuStrip1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuStrip1;
			this.MaximizeBox = false;
			this.Name = "FornaxEditor";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Fornax Particle Editor";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FornaxEditor_FormClosing);
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.grpParticlesToEmit.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numParticleCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numReEmitInterval)).EndInit();
			this.grpParticleGroupProperties.ResumeLayout(false);
			this.grpParticleGroupProperties.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numInitialDelay)).EndInit();
			this.grpChildEmitters.ResumeLayout(false);
			this.grpParticleLifespan.ResumeLayout(false);
			this.grpParticleLifespan.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numEmitterLife)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numEmitterRadius)).EndInit();
			this.grpEmitterProperties.ResumeLayout(false);
			this.grpEmitterProperties.PerformLayout();
			this.grpColor.ResumeLayout(false);
			this.grpColor.PerformLayout();
			this.grpAlpha.ResumeLayout(false);
			this.grpAlpha.PerformLayout();
			this.grpVelocity.ResumeLayout(false);
			this.grpVelocity.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numVelocityVariance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numVelocityValue)).EndInit();
			this.grpSize.ResumeLayout(false);
			this.grpSize.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numSizeVariance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numSizeValue)).EndInit();
			this.grpSpin.ResumeLayout(false);
			this.grpSpin.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numSpinVariance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numSpinValue)).EndInit();
			this.grpProperties.ResumeLayout(false);
			this.grpProperties.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numParticleLifeVariance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numParticleLife)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picTexture)).EndInit();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripButton btnNew;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripButton btnOpen;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripButton btnSaveAs;
		private System.Windows.Forms.ToolStripButton btnQuit;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.ToolStripButton btnAbout;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
		private System.Windows.Forms.ToolStripButton btnPreview;
		private System.Windows.Forms.ListBox lstParticlesToEmit;
		private System.Windows.Forms.Button btnAddParticle;
		private System.Windows.Forms.Button btnRemoveParticle;
		private System.Windows.Forms.GroupBox grpParticlesToEmit;
		private System.Windows.Forms.NumericUpDown numParticleCount;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.CheckBox chkParticleEnabled;
		private System.Windows.Forms.CheckBox chkParticleSolo;
		private System.Windows.Forms.NumericUpDown numReEmitInterval;
		private System.Windows.Forms.TextBox txtParticleToEmit;
		private System.Windows.Forms.GroupBox grpParticleGroupProperties;
		private System.Windows.Forms.GroupBox grpChildEmitters;
		private System.Windows.Forms.GroupBox grpParticleLifespan;
		private System.Windows.Forms.CheckBox chkAffectsLifespan;
		private System.Windows.Forms.CheckBox chkAffectsVelocity;
		private System.Windows.Forms.CheckBox chkAffectsSize;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown numEmitterLife;
		private System.Windows.Forms.NumericUpDown numEmitterRadius;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.GroupBox grpEmitterProperties;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtParticleFilename;
		private System.Windows.Forms.TextBox txtTexture;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.GroupBox grpColor;
		private MultiThumbSlider.MultiThumbSlider mtsColor;
		private System.Windows.Forms.Button btnColor;
		private System.Windows.Forms.Button btnRemoveColor;
		private System.Windows.Forms.Button btnAddColor;
		private System.Windows.Forms.GroupBox grpAlpha;
		private MultiThumbSlider.MultiThumbSlider mtsAlpha;
		private System.Windows.Forms.Button btnAlpha;
		private System.Windows.Forms.Button btnRemoveAlpha;
		private System.Windows.Forms.Button btnAddAlpha;
		private System.Windows.Forms.GroupBox grpVelocity;
		private MultiThumbSlider.MultiThumbSlider mtsVelocity;
		private System.Windows.Forms.NumericUpDown numVelocityVariance;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.NumericUpDown numVelocityValue;
		private System.Windows.Forms.Button btnRemoveVelocity;
		private System.Windows.Forms.Button btnAddVelocity;
		private System.Windows.Forms.GroupBox grpSize;
		private MultiThumbSlider.MultiThumbSlider mtsSize;
		private System.Windows.Forms.NumericUpDown numSizeVariance;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.NumericUpDown numSizeValue;
		private System.Windows.Forms.Button btnRemoveSize;
		private System.Windows.Forms.Button btnAddSize;
		private System.Windows.Forms.GroupBox grpSpin;
		private MultiThumbSlider.MultiThumbSlider mtsSpin;
		private System.Windows.Forms.NumericUpDown numSpinVariance;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.NumericUpDown numSpinValue;
		private System.Windows.Forms.Button btnRemoveSpin;
		private System.Windows.Forms.Button btnAddSpin;
		private System.Windows.Forms.PictureBox picTexture;
		private System.Windows.Forms.Button btnShowTexture;
		private System.Windows.Forms.GroupBox grpProperties;
		private System.Windows.Forms.ToolStripButton btnSaveAll;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem saveEmitterAsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveAllToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
		private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
		private System.Windows.Forms.NumericUpDown numParticleLifeVariance;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.NumericUpDown numParticleLife;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Button btnDuplicate;
		private System.Windows.Forms.CheckBox chkSpawnCenter;
		private System.Windows.Forms.CheckBox chkAdditiveBlending;
		private System.Windows.Forms.CheckBox chkReEmit;
		private System.Windows.Forms.NumericUpDown numInitialDelay;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnRemoveChild;
		private System.Windows.Forms.Button btnAddChild;
		private System.Windows.Forms.ListBox lstChildEmitters;
		private System.Windows.Forms.ToolStripButton btnExport;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
		private System.Windows.Forms.Button btnScaleKeyframeValues;
	}
}

