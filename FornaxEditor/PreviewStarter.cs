﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace FornaxEditor
{
	class PreviewStarter
	{
		public static void Export(string longFileName, int frameRate, int resolution, string outDir, string prefix)
		{
			Process.Start("previewer.exe", "\"" +
						  longFileName + "\" " +
						  frameRate + " " +
						  resolution + " " +
						  outDir + " " +
						  prefix);
		}

		public static void Preview(string longFileName)
		{
			Process.Start("previewer.exe", "\"" + longFileName + "\"");
		}
	}
}
