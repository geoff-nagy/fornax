﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FornaxEditor
{
	public partial class ScaleKeyframeValuesForm : Form
	{
		private double AlphaScale;
		private double VelocityScale;
		private double SizeScale;
		private double SpinScale;

		public ScaleKeyframeValuesForm()
		{
			InitializeComponent();

			AlphaScale = 1.0;
			VelocityScale = 1.0;
			SizeScale = 1.0;
			SpinScale = 1.0;

			errAlpha.SetError(txtAlphaScale, null);
			errVelocity.SetError(txtVelocityScale, null);
			errSize.SetError(txtSizeScale, null);
			errSpin.SetError(txtSpinScale, null);
		}

		// retrieve desired keyframe scale values
		public double GetAlphaScale() { return AlphaScale; }
		public double GetVelocityScale() { return VelocityScale; }
		public double GetSizeScale() { return SizeScale; }
		public double GetSpinScale() { return SpinScale; }

		// see if any textbox validation errors are present
		private bool AreTextboxErrorsPresent()
		{
			return !String.IsNullOrEmpty(errAlpha.GetError(txtAlphaScale)) ||
				   !String.IsNullOrEmpty(errVelocity.GetError(txtVelocityScale)) ||
				   !String.IsNullOrEmpty(errSize.GetError(txtSizeScale)) ||
				   !String.IsNullOrEmpty(errSpin.GetError(txtSpinScale));
		}

		private void txtAlphaScale_Validating(object sender, CancelEventArgs e)
		{
			// try to parse the value and see if we succeeded
			bool valid = Double.TryParse(txtAlphaScale.Text, out AlphaScale);
			if(!valid)
			{
				e.Cancel = true;
				btnAccept.Enabled = false;
				errAlpha.SetError(txtAlphaScale, "Not a valid floating-point number");
			}
		}

		private void txtAlphaScale_Validated(object sender, EventArgs e)
		{
			errAlpha.SetError(txtAlphaScale, null);
			btnAccept.Enabled = !AreTextboxErrorsPresent();
		}

		private void txtVelocityScale_Validating(object sender, CancelEventArgs e)
		{
			// try to parse the value and see if we succeeded
			bool valid = Double.TryParse(txtVelocityScale.Text, out VelocityScale);
			if(!valid)
			{
				e.Cancel = true;
				btnAccept.Enabled = false;
				errVelocity.SetError(txtVelocityScale, "Not a valid floating-point number");
			}
		}

		private void txtVelocityScale_Validated(object sender, EventArgs e)
		{
			errVelocity.SetError(txtVelocityScale, null);
			btnAccept.Enabled = !AreTextboxErrorsPresent();
		}

		private void txtSizeScale_Validating(object sender, CancelEventArgs e)
		{
			// try to parse the value and see if we succeeded
			bool valid = Double.TryParse(txtSizeScale.Text, out SizeScale);
			if(!valid)
			{
				e.Cancel = true;
				btnAccept.Enabled = false;
				errSize.SetError(txtSizeScale, "Not a valid floating-point number");
			}
		}

		private void txtSizeScale_Validated(object sender, EventArgs e)
		{
			errSize.SetError(txtSizeScale, null);
			btnAccept.Enabled = !AreTextboxErrorsPresent();
		}

		private void txtSpinScale_Validating(object sender, CancelEventArgs e)
		{
			// try to parse the value and see if we succeeded
			bool valid = Double.TryParse(txtSpinScale.Text, out SpinScale);
			if(!valid)
			{
				e.Cancel = true;
				btnAccept.Enabled = false;
				errSpin.SetError(txtSpinScale, "Not a valid floating-point number");
			}
		}

		private void txtSpinScale_Validated(object sender, EventArgs e)
		{
			errSpin.SetError(txtSpinScale, null);
			btnAccept.Enabled = !AreTextboxErrorsPresent();
		}
	}
}
