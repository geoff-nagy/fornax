﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace FornaxEditor
{
	class FileReader
	{
		public static Emitter ReadEmitter(String filename)
		{
			Emitter result = new Emitter();						// emitter to return
			Tree tree = Tree.CreateFromFile(filename);			// tree container of dash structure
			ListNode root = tree.GetRoot() as ListNode;			// root of dash structure (should be a list node)
			ParticleGroup group;								// a new group to load

			ListNode part;										// a list node (probably one of several) describing one particle in this emitter
			StringNode child;										// a list node describing any trail (if any) that the particle emits
			int particleIndex = 0;								// denotes the (particleIndex)th list node we're loading (starting at 0)
			int childEmitterIndex;

			String particleFileName;
			String particleShortName;
			ParticleDescriptor particleDescriptor;

			// set emitter location
			result.AbsoluteFileName = filename;

			// read basic emitter properties
			result.Life = root.GetNumber("life", 0, 0);
			result.Radius = root.GetNumber("radius", 0, 0);

			// now read as many particle groups as we can
			part = root.GetListNodeByName("particle", particleIndex++);
			while (part != null)
			{
				// read particle descriptor
				particleShortName = part.GetString("name", "");
				particleFileName = result.GetPathToParticle(particleShortName);
				particleDescriptor = ParticleDescriptor.FromFile(particleFileName);

				// read properties from dash file (look how easy that is!!)
				group = new ParticleGroup(particleShortName, particleDescriptor);
				group.EmissionCount = part.GetNumber("emission-count", 0, 0);
				group.InitialDelay = part.GetNumber("initial-delay", 0, 0);
				group.ReEmit = (part.GetNumber("re-emit", 0, 0) == 1);
				group.ReEmitInterval = part.GetNumber("re-emit-interval", 0, 0);
				group.Enabled = part.GetNumber("enabled", 0, 0) == 1;
				group.Solo = part.GetNumber("solo", 0, 0) == 1;
				group.SpawnCenter = part.GetNumber("spawn-center", 0, 0) == 1;
				group.AdditiveBlending = part.GetNumber("additive-blending", 0, 0) == 1;

				// read life inheritance properties
				group.EmitterAffectsSize = (part.GetNumber("scale-size", 0, 0)) == 1;
				group.EmitterAffectsVelocity = (part.GetNumber("scale-velocity", 0, 0)) == 1;
				group.EmitterAffectsLifespan = (part.GetNumber("scale-life", 0, 0)) == 1;

				// read in children emitters, if any
				childEmitterIndex = 0;
				child = part.GetStringNodeByName("child-emitter", childEmitterIndex++);
				while (child != null)
				{
					group.ChildEmitters.Add(child.GetValue());
					child = part.GetStringNodeByName("child-emitter", childEmitterIndex++);
				}

				// add the particle group to the emitter
				result.AddParticleGroup(group);

				// and now load the next one
				part = root.GetListNodeByName("particle", particleIndex++);
			}

			return result;
		}
	}
}
