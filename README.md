# Fornax Particle Editor

This is a small particle engine and editor. The main goal was to provide an editor that had a fast preview function, supported multiple keyframes per property, and could build effects that did not require billboarding or animation exports. It was originally developed for Gateway.